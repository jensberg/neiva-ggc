﻿using UnityEngine;
using System.Collections;

public class CameraZone : CameraComponent
{
	// types
	[System.Serializable]
	public struct AffectAxes
	{
		public bool xPosition;
		public bool yPosition;
		public bool zPosition;
		public bool rotation;
		public bool fov;
	}

	// variables
	public Transform actor = null;
	[Space]
	public Vector3 size = Vector3.one * 5f + Vector3.right * 6f + Vector3.up * 2f;
	public float transitionRadius = 2f;
	[Range(0f, 1f)]
	public float maxOpacity = 1f;
	public AffectAxes affectAxes = new AffectAxes() { xPosition = true, yPosition = true, zPosition = true, rotation = true, fov = false };
	public CameraAxes cameraSettings = new CameraAxes();

	private Vector3 innerHalfSize;
	private Vector3 outerHalfSize;

	// accessors
	public float opacity { get; private set; }

	// methods
	void Awake()
	{
		innerHalfSize = size / 2f;
		outerHalfSize = size / 2f + Vector3.one * transitionRadius;

		// init CameraComponent variables
		cameraPosition = cameraSettings.position;
		cameraRotation = cameraSettings.rotation;
		cameraFOV = cameraSettings.fov;
		xPositionOpacity = 0f;
		yPositionOpacity = 0f;
		zPositionOpacity = 0f;
		rotationOpacity = 0f;
		fovOpacity = 0f;
	}

	void Start()
	{
		// NEIVA Specific:
		actor = FindObjectOfType<NeivaController>().transform;
	}

	void Update()
	{
		Vector3 outerMin = transform.position - outerHalfSize;
		Vector3 outerMax = transform.position + outerHalfSize;

		if (inBox(actor.position, outerMin, outerMax))
		{
			Vector3 innerMin = transform.position - innerHalfSize;
			Vector3 innerMax = transform.position + innerHalfSize;

			if (inBox(actor.position, innerMin, innerMax))
				opacity = maxOpacity;
			else {
				float x = Mathf.Abs(actor.position.x - transform.position.x) - innerHalfSize.x;
				float y = Mathf.Abs(actor.position.y - transform.position.y) - innerHalfSize.y;
				float z = Mathf.Abs(actor.position.z - transform.position.z) - innerHalfSize.z;
				float tX = x / transitionRadius;
				float tY = y / transitionRadius;
				float tZ = z / transitionRadius;
				float tMin = tX;
				if (tY > tMin) tMin = tY;
				if (tZ > tMin) tMin = tZ;
				opacity = maxOpacity * (1f - tMin);
			}
		}
		else
			opacity = 0f;

		// set opacity for each individual axis
		xPositionOpacity = affectAxes.xPosition ? opacity : 0f;
		yPositionOpacity = affectAxes.yPosition ? opacity : 0f;
		zPositionOpacity = affectAxes.zPosition ? opacity : 0f;
		rotationOpacity = affectAxes.rotation ? opacity : 0f;
		fovOpacity = affectAxes.fov ? opacity : 0f;
	}

	// functions
	static bool inRange(float a, float min, float max) { return a >= min && a <= max; }
	static bool inBox(Vector3 point, Vector3 min, Vector3 max)
	{
		return
			inRange(point.x, min.x, max.x) &&
			inRange(point.y, min.y, max.y) &&
			inRange(point.z, min.z, max.z);
	}

#if UNITY_EDITOR
	[Header("Editor")]
	public bool saveCameraSettings = false;
	Camera editorCamera = null;
	void OnValidate()
	{
		if (saveCameraSettings && editorCamera != null)
		{
			saveCameraSettings = false;
			cameraSettings = new CameraAxes(editorCamera);
		}

		// clamp values
		size.x = size.x < 0f ? 0f : size.x;
		size.y = size.y < 0f ? 0f : size.y;
		size.z = size.z < 0f ? 0f : size.z;
		transitionRadius = transitionRadius < 0f ? 0f : transitionRadius;

		Awake();
	}
	void OnDrawGizmosSelected()
	{
		if (editorCamera == null)
			editorCamera = Camera.current;

		Vector3 outerMin = transform.position - outerHalfSize;
		Vector3 outerMax = transform.position + outerHalfSize;

		// draw zone in editor
		Gizmos.color = new Color(0f, 0f, 0f, 0.5f);
		Gizmos.DrawCube(transform.position, size);
		Gizmos.DrawCube(transform.position, size + Vector3.one * transitionRadius * 2f);
	}
#endif
}
