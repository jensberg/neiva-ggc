﻿using UnityEngine;
using System.Collections;
using System;

public class CameraFollow : CameraComponent
{
	public Transform target = null;
	public Vector3 offset = Vector3.up;
	public float tilt = 5f;
	public float fov = 17f;

	void Awake()
	{
		cameraPosition = target.position + offset;
		cameraRotation = Quaternion.Euler(tilt, 0f, 0f);
		cameraFOV = fov;
	}

	void Update()
	{
		cameraPosition = target.position + offset;
	}

	public override CameraAxesFlags UsingCameraAxes()
	{
		return 
			CameraAxesFlags.PositionX |
			CameraAxesFlags.PositionY |
			CameraAxesFlags.PositionZ |
			CameraAxesFlags.Rotation |
			CameraAxesFlags.FOV;
	}

#if UNITY_EDITOR
	void OnValidate()
	{
	
	}
#endif
}
