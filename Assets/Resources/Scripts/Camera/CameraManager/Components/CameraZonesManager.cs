﻿using UnityEngine;
using System.Collections;
using System;

public class CameraZonesManager : CameraComponent
{
	// variables
	public bool autoFindZones = true;
	public CameraZone[] zones = null;

	AlphaLayer accPositionX = new AlphaLayer(0f, 0f);
	AlphaLayer accPositionY = new AlphaLayer(0f, 0f);
	AlphaLayer accPositionZ = new AlphaLayer(0f, 0f);
	Quaternion accRotation = Quaternion.Euler(0f, 0f, 0f);
	AlphaLayer accFOV = new AlphaLayer(0f, 0f);

	// methods
	void Awake()
	{
		xPositionOpacity = 0f;
		yPositionOpacity = 0f;
		zPositionOpacity = 0f;
		rotationOpacity = 0f;
		fovOpacity = 0f;
	}

	void Start()
	{
		if (autoFindZones)
			zones = FindObjectsOfType<CameraZone>();
        if (zones.Length == 0)
            this.enabled = false;
	}
	
	public void Update()
	{
		float maxXPosOpacity = 0f;
		float maxYPosOpacity = 0f;
		float maxZPosOpacity = 0f;
		float maxRotOpacity = 0f;
		float maxFovOpacity = 0f;

		int num = 0;

		foreach (CameraZone zone in zones)
			if (zone.opacity > 0f)
			{
				num++;

				maxXPosOpacity = zone.xPositionOpacity > maxXPosOpacity ? zone.xPositionOpacity : maxXPosOpacity;
				maxYPosOpacity = zone.yPositionOpacity > maxYPosOpacity ? zone.yPositionOpacity : maxYPosOpacity;
				maxZPosOpacity = zone.zPositionOpacity > maxZPosOpacity ? zone.zPositionOpacity : maxZPosOpacity;
				maxRotOpacity = zone.rotationOpacity > maxRotOpacity ? zone.rotationOpacity : maxRotOpacity;
				maxFovOpacity = zone.fovOpacity > maxFovOpacity ? zone.fovOpacity : maxFovOpacity;

				accPositionX = num == 1 ? new AlphaLayer(zone.cameraPosition.x, zone.xPositionOpacity) : blend(new AlphaLayer(zone.cameraPosition.x, zone.xPositionOpacity), accPositionX);
				accPositionY = num == 1 ? new AlphaLayer(zone.cameraPosition.y, zone.yPositionOpacity) : blend(new AlphaLayer(zone.cameraPosition.y, zone.yPositionOpacity), accPositionY);
				accPositionZ = num == 1 ? new AlphaLayer(zone.cameraPosition.z, zone.zPositionOpacity) : blend(new AlphaLayer(zone.cameraPosition.z, zone.zPositionOpacity), accPositionZ);
				accRotation = num == 1 ? Quaternion.Lerp(accRotation, zone.cameraRotation, zone.rotationOpacity) : Quaternion.Lerp(accRotation, zone.cameraRotation, zone.rotationOpacity);
				accFOV = num == 1 ? new AlphaLayer(zone.cameraFOV, zone.fovOpacity) : blend(new AlphaLayer(zone.cameraFOV, zone.fovOpacity), accFOV);
			}

		this.xPositionOpacity = maxXPosOpacity;
		this.yPositionOpacity = maxYPosOpacity;
		this.zPositionOpacity = maxZPosOpacity;
		this.rotationOpacity = maxRotOpacity;
		this.fovOpacity = maxFovOpacity;

		this.cameraPosition = new Vector3(accPositionX.component, accPositionY.component, accPositionZ.component);
		this.cameraFOV = accFOV.component;
		this.cameraRotation = accRotation;
	}

	// helpers
	public struct AlphaLayer
	{
		public AlphaLayer(float component, float alpha)
		{ this.component = component; this.alpha = alpha; }
		[Range(0f, 1f)]
		public float component;
		public float alpha;
	}
	static AlphaLayer blend(AlphaLayer src, AlphaLayer dst)
	{
		// http://stackoverflow.com/questions/10781953/determine-rgba-colour-received-by-combining-two-colours
		// αr = αa + αb (1 - αa)
		// Cr = (Ca αa + Cb αb (1 - αa)) / αr
		if (src.alpha == 0f)
			return dst;
		else
		{
			float alpha = src.alpha + dst.alpha * (1f - src.alpha);
			float component = (src.component * src.alpha + dst.component * dst.alpha * (1f - src.alpha)) / alpha;
			return new AlphaLayer(component, alpha);
		}
	}
}
