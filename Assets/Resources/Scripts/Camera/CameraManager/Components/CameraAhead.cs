﻿using UnityEngine;
using System.Collections;
using System;

public class CameraAhead : CameraComponent
{
	public Rigidbody target = null;
	public Vector2 aheadMultiplier = Vector2.one;

	void Update()
	{
		cameraPosition = target.position + Vector3.Scale(target.velocity, aheadMultiplier);
	}

	public override CameraAxesFlags UsingCameraAxes()
	{
		return CameraAxesFlags.PositionX | CameraAxesFlags.PositionY;
	}
}
