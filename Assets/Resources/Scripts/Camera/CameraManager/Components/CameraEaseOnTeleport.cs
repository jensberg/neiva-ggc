﻿using UnityEngine;
using System.Collections;
using System;

public class CameraEaseOnTeleport : CameraComponent
{
	[Space]
	public Teleportable teleportableObject = null;
	public float duration = 1f;

	Vector3 teleportedFrom = Vector3.zero;
	bool isHolding = false;
	float timer = 0f;

	void OnEnable()
	{
		teleportableObject.onTeleport += OnTeleport;
	}

	void OnDisable()
	{
		teleportableObject.onTeleport -= OnTeleport;
	}

	void Update()
	{
		if (isHolding)
		{
			timer += Time.deltaTime;
			cameraPosition = Vector3.Lerp(teleportedFrom, teleportableObject.transform.position, timer / duration);
			if (timer >= duration)
				isHolding = false;
		}
	}

	void OnTeleport(Vector3 from, Vector3 to)
	{
		teleportedFrom = Camera.main.transform.position;
		isHolding = true;
		timer = 0;
	}

	public override CameraAxesFlags UsingCameraAxes()
	{
		return isHolding ? CameraAxesFlags.PositionX | CameraAxesFlags.PositionY : 0;
	}

#if UNITY_EDITOR
	void OnValidate()
	{
		duration = duration < 0f ? 0f : duration;
	}
#endif
}
