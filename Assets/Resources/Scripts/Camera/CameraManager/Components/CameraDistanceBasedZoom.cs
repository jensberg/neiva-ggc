﻿using UnityEngine;
using System.Collections;
using System;

public class CameraDistanceBasedZoom : CameraComponent
{
	public float minZ = -8f;
	public float multiplier = 1f;
	public Transform[] targets = new Transform[0];

	void Update()
	{
		Vector3 min = targets[0].position;
		Vector3 max = targets[0].position;
		for (int i = 1; i < targets.Length; ++i)
			if (targets[i].gameObject.activeInHierarchy)
			{
				Vector3 target = targets[i].position;
				min = new Vector3(
					target.x < min.x ? target.x : min.x,
					target.y < min.y ? target.y : min.y,
					target.z < min.z ? target.z : min.z
					);
				max = new Vector3(
					target.x > max.x ? target.x : max.x,
					target.y > max.y ? target.y : max.y,
					target.z > max.z ? target.z : max.z
					);
			}
		cameraPosition.z = minZ - Vector3.Distance(min, max) * multiplier;
	}

	public override CameraAxesFlags UsingCameraAxes()
	{
		return CameraAxesFlags.PositionZ;
	}
}
