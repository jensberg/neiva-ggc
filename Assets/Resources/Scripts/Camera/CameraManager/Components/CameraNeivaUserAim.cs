﻿using UnityEngine;
using System.Collections;
using System;

public class CameraNeivaUserAim : CameraComponent
{
	public NeivaUserControl neivaUserControl = null;
	public float lookDistanceMultiplier = 2f;
	public float speed = 3f;
    public Vector2 offset = Vector2.up;

	void Update()
	{
		cameraPosition = 
			Vector2.Lerp(cameraPosition, (Vector2)neivaUserControl.transform.position + neivaUserControl.AimInput * lookDistanceMultiplier + offset, Time.deltaTime * speed);
	}

	public override CameraAxesFlags UsingCameraAxes()
	{
		return neivaUserControl.AimInput.magnitude > 0f ? CameraAxesFlags.PositionX | CameraAxesFlags.PositionY : 0;
	}
}
