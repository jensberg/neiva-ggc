﻿using UnityEngine;
using System;
using System.Collections.Generic;

[System.Serializable]
public struct CameraAxes
{
	public CameraAxes(Camera camera)
	{ this.position = camera.transform.position; this.rotation = camera.transform.rotation; this.fov = camera.fieldOfView; }
	public CameraAxes(Vector3 position, Quaternion rotation, float fov)
	{ this.position = position; this.rotation = rotation; this.fov = fov; }
	public Vector3 position;
	public Quaternion rotation;
	public float fov;
}

public enum CameraAxesFlags
{
	PositionX = 1,
	PositionY = 2,
	PositionZ = 4,
	Rotation = 8,
	FOV = 16,
}

public abstract class CameraComponent : MonoBehaviour
{
	[HideInInspector] public Vector3 cameraPosition = Vector3.zero;
	[HideInInspector] public Quaternion cameraRotation = Quaternion.identity;
	[HideInInspector] public float cameraFOV = 0f;
	[HideInInspector] public float xPositionOpacity = 1f;
	[HideInInspector] public float yPositionOpacity = 1f;
	[HideInInspector] public float zPositionOpacity = 1f;
	[HideInInspector] public float rotationOpacity = 1f;
	[HideInInspector] public float fovOpacity = 1f;
	public virtual CameraAxesFlags UsingCameraAxes() { return 0; }
}

public class CameraManager : MonoBehaviour
{
	// types
	[System.Serializable]
	public class CameraLayer
	{
		[Range(0f, 1f)]
		public float opacity = 0.5f;
		public CameraComponent component;
	}
	public struct Layer
	{
		public Layer(float alpha, float component)
		{ this.alpha = alpha; this.component = component; }
		[Range(0f, 1f)]
		public float alpha;
		public float component;
	}
	struct RotationLayer
	{
		public float opacity;
		public Quaternion rotation;
	}

	// variables
	public new Camera camera = null;
	[Range(0f, 3f)]
	public float masterSmoothness = 1f;
	[Space]
	public List<CameraLayer> layers = null;

	Layer xPosResult = new Layer(0f, 0f);
	Layer yPosResult = new Layer(0f, 0f);
	Layer zPosResult = new Layer(0f, 0f);
	Quaternion rotResult = Quaternion.identity;
	Layer fovResult = new Layer(0f, 0f);

	// methods
	void Awake()
	{
		xPosResult = new Layer(0f, camera.transform.position.x);
		yPosResult = new Layer(0f, camera.transform.position.y);
		zPosResult = new Layer(0f, camera.transform.position.z);
		rotResult = camera.transform.rotation;
		fovResult = new Layer(0f, camera.fieldOfView);
	}

	void Start()
	{
		if (GameManager.Singleton != null)
			GameManager.Singleton.stateTree.onChangeState += OnGameStateChange;
	}

	void OnDestroy()
	{
		if (GameManager.Singleton != null)
			GameManager.Singleton.stateTree.onChangeState -= OnGameStateChange;
	}

	void LateUpdate()
	{
		if (Time.deltaTime > 0f)
		{
			xPosResult.alpha = 0f;
			yPosResult.alpha = 0f;
			zPosResult.alpha = 0f;
			fovResult.alpha = 0f;

			// merge layers
			foreach (CameraLayer layer in layers)
			{
				xPosResult = blend(new Layer(Mathf.Clamp01(layer.component.xPositionOpacity) * layer.opacity, layer.component.cameraPosition.x), xPosResult);
				yPosResult = blend(new Layer(Mathf.Clamp01(layer.component.yPositionOpacity) * layer.opacity, layer.component.cameraPosition.y), yPosResult);
				zPosResult = blend(new Layer(Mathf.Clamp01(layer.component.zPositionOpacity) * layer.opacity, layer.component.cameraPosition.z), zPosResult);
				rotResult = Quaternion.Slerp(rotResult, layer.component.cameraRotation, Mathf.Clamp01(layer.component.rotationOpacity * layer.opacity));
				fovResult = blend(new Layer(Mathf.Clamp01(layer.component.fovOpacity) * layer.opacity, layer.component.cameraFOV), fovResult);
			}

			Vector3 posResult = new Vector3(
				xPosResult.component,
				yPosResult.component,
				zPosResult.component
				);
			camera.transform.position = Vector3.Lerp(camera.transform.position, posResult, Time.deltaTime / masterSmoothness);
			camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, rotResult, Time.deltaTime / masterSmoothness);
			camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, fovResult.component, Time.deltaTime / masterSmoothness);
		}
    }

    void OnGameStateChange(State newState)
	{
		if (newState.Tag() == "Active" && GameManager.Singleton.stateTree.previousState.Tag() == "Death")
			ResetToNeiva();
	}

	public void ResetToNeiva()
	{
		NeivaUserCamera userCamera = GetComponent<NeivaUserCamera>();
		camera.transform.position = userCamera.neiva.rigidbody.position + userCamera.offset;
		camera.transform.rotation = Quaternion.Euler(userCamera.tilt, 0f, 0f);
		camera.fieldOfView = userCamera.fov;
	}

	// helpers
	static Layer blend(Layer src, Layer dst)
	{
		// http://stackoverflow.com/questions/10781953/determine-rgba-colour-received-by-combining-two-colours
		// αr = αa + αb (1 - αa)
		// Cr = (Ca αa + Cb αb (1 - αa)) / αr
		if (src.alpha == 0f)
			return dst;
		else {
			float alpha = src.alpha + dst.alpha * (1f - src.alpha);
			float component = (src.component * src.alpha + dst.component * dst.alpha * (1f - src.alpha)) / alpha;
			return new Layer(alpha, component);
		}
	}
	static RotationLayer blendRotations(RotationLayer src, RotationLayer dst)
	{
		float opacity = src.opacity + dst.opacity * (1f - src.opacity);
		return new RotationLayer();
	}
}
