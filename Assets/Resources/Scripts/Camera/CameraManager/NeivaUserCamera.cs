﻿using UnityEngine;
using System.Collections;
using System;

public class NeivaUserCamera : CameraComponent
{
	// variables
	public Vector3 offset = Vector3.up;
	public float tilt = 10f;
	public float fov = 15f;
	public Vector2 soulFocus = Vector2.right;
	public float easeOnTeleportSmoothness = 3f;
	public float easeOnTeleportTime = 1f;
	public Vector2 aimDistance = Vector2.one;
	public Vector2 ahead = Vector2.one;
	public Vector2 throwZoomOut = Vector2.one;

	public NeivaUserControl neivaUser { get; private set; }
	public NeivaController neiva { get; private set; }

	float accSmoothness;
	float lastMoveInput;

	float lookDirection;

	// methods
	public void Awake()
	{
		neivaUser = FindObjectOfType<NeivaUserControl>();
		neiva = FindObjectOfType<NeivaController>();
	}

	void Start()
	{
		cameraPosition = neiva.rigidbody.position + offset;
		cameraRotation = Quaternion.Euler(tilt, 0f, 0f);
		cameraFOV = fov;
		accSmoothness = 0f;

		neiva.teleportable.onTeleport += OnTeleport;
	}

	void OnDestroy()
	{
		neiva.teleportable.onTeleport -= OnTeleport;
	}

	public void Update()
	{
        if (Time.deltaTime > 0f)
        {
		    lookDirection =
			    neiva.aimInput.x < 0f || neiva.moveInput < 0f ? Mathf.MoveTowards(lookDirection, -1f, Time.deltaTime * 2f) :
			    neiva.aimInput.x > 0f || neiva.moveInput > 0f ? Mathf.MoveTowards(lookDirection, 1f, Time.deltaTime * 10f) :
			    lookDirection;

		    Vector3 targetPosition = neiva.transform.position;
		    targetPosition += Vector3.Scale(neiva.rigidbody.velocity, ahead);
		    targetPosition += Vector3.Scale(Vector3.ClampMagnitude(neivaUser.AimInput, 1f), aimDistance);
		    if (neiva.soulThrowerController.activeSoul != null) targetPosition += (Vector3)Vector2.Scale(soulFocus, (neiva.soulThrowerController.activeSoul.transform.position - neiva.transform.position));
		    targetPosition += Vector3.right * lookDirection * 1f;
		    targetPosition += offset;
		    if (neiva.soulThrowerController.activeSoul != null) targetPosition += Vector3.back * Vector3.Scale(throwZoomOut, (neiva.soulThrowerController.activeSoul.transform.position - neiva.transform.position)).magnitude;

		    accSmoothness = accSmoothness > 0f ? accSmoothness - Time.deltaTime / easeOnTeleportTime : 0f;

		    cameraPosition = Vector3.Lerp(cameraPosition, targetPosition, Time.deltaTime / accSmoothness);
		    cameraRotation = Quaternion.Euler(tilt, 0f, 0f);
		    cameraFOV = fov;
        }
    }

	void OnTeleport(Vector3 from, Vector3 to)
	{
		accSmoothness = easeOnTeleportSmoothness;
	}

#if UNITY_EDITOR
	void OnValidate()
	{
		soulFocus.x = Mathf.Clamp01(soulFocus.x);
		soulFocus.y = Mathf.Clamp01(soulFocus.y);
	}
#endif
}
