﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DebugScreenShake
{
    public KeyCode m_inputkeyToTest;
    public KeyCode m_inputKeyToStop;
    [Space(10)]
    public float m_offsetRange = 1f;
    public float m_shakeTime = 1f;
    public float m_shakeSpeed = 50f;
    public float m_timeScale = 1;
    [Space(10)]
    public bool m_smoothSpeed = true;
    public bool m_smoothRange = true;
}

public class ScreenShake : MonoBehaviour {

    private bool m_shake = false;
    private float m_timer = 1f;
    private float m_shakeTime = 1f;
    private Vector3 m_shakeRange = new Vector3(1,1,1);
    private float m_shakeSpeed = 50f;
    private bool m_smoothSpeed = true;
    private bool m_smoothRange = true;
    bool m_continousShake = false;

    Vector3 m_shakeOffset;

    public DebugScreenShake m_debugger;
    [Space(10)]
    [Header("Turn off shaking on a specifik axis.")]
    public bool m_freezeZAxis = false;
    public bool m_freezeYAxis = false;
    public bool m_freezeXAxis = false;
	
	void LateUpdate ()
    {
        if (Input.GetKeyDown(m_debugger.m_inputkeyToTest))
            TestScreenShake();
        if (Input.GetKeyDown(m_debugger.m_inputKeyToStop))
            Stop();

        if (m_shake)
        {
            // stop shaking if time is up
            if(m_timer > m_shakeTime * Time.timeScale && !m_continousShake)
            {
                // resetting variables
                m_timer = 0;
                m_shake = false;
                Time.timeScale = 1;
            }
            
            // shaking enabled
            else
            {
                m_timer += Time.deltaTime;

                // smooth values towards 0
                if (m_smoothSpeed)
                    m_shakeSpeed -= m_shakeSpeed * Mathf.Sin(Time.deltaTime / m_shakeTime); 
                if(m_smoothRange)
                    m_shakeRange -= Vector3.Scale(m_shakeRange, m_shakeRange * Mathf.Sin(Time.deltaTime / m_shakeTime));
                
                // set shake positions
                Vector3 originalPos = transform.position - m_shakeOffset; // the current position if we hadn't shook it.

                m_shakeOffset = Vector3.Scale(SmoothRandom.GetVector3(m_shakeSpeed), m_shakeRange);
                if (m_freezeXAxis)
                    m_shakeOffset.x = 0;
                if (m_freezeYAxis)
                    m_shakeOffset.y = 0;
                if (m_freezeZAxis)
                    m_shakeOffset.z = 0;

                Camera.main.transform.position = originalPos + m_shakeOffset;
                m_continousShake = false;           
            }
        }
	}
    /// <summary>
    /// Trigger a standard shake for time as seconds.
    /// </summary>
    /// <param name="time">Length of shake.</param>
    public void ShakeTrigger(float time)
    {
        m_shake = true;
        m_smoothRange = true;
        m_smoothSpeed = true;

        m_shakeSpeed = 50f;
        float range = 0.8f;
        m_shakeRange = new Vector3(1, 1, 1) * range;
        m_shakeTime = time;
        m_timer = 0;
    }
    /// <summary>
    /// Triggers a camera shake for a period of time & smooths the speed and offset range towards 0.
    /// </summary>
    /// <param name="speed">How fast the camera finds a random position. Smooths to 0 by default.</param>
    /// <param name="range">Max range of the camera offset on all axes from original position.</param>
    /// <param name="time">Length of the screenshake in seconds.</param>
    public void ShakeTrigger(float speed, float range, float time)
    {
        m_shake = true;
        m_smoothRange = true;
        m_smoothSpeed = true;

        m_shakeSpeed = speed;
        m_shakeRange = new Vector3(1, 1, 1) * range;
        m_shakeTime = time;
        m_timer = 0;
    }
    /// <summary>
    /// Triggers screen shake of the main camera for a period of time. Optional smoothing.
    /// </summary>
    /// <param name="speed">How fast the camera finds a random position. Smooths to 0 by default.</param>
    /// <param name="range">Max range of the camera offset on all axes from original position.</param>
    /// <param name="time">Length of the screenshake in seconds.</param>
    /// <param name="smoothRange">Should the range be smoothed towards 0?</param>
    /// <param name="smoothSpeed">Should the speed be smoothed towards 0?</param>
    public void ShakeTrigger(float speed, float range, float time, bool smoothRange, bool smoothSpeed)
    {
        m_shake = true;
        m_smoothRange = smoothRange;
        m_smoothSpeed = smoothSpeed;

        m_shakeSpeed = speed;
        m_shakeRange = new Vector3(1, 1, 1) * range;
        m_shakeTime = time;
        m_timer = 0;
    }
    /// <summary>
    /// Triggers screen shake of the main camera for a period of time. Optional smoothing.
    /// </summary>
    /// <param name="speed">How fast the camera finds a random position. Smooths to 0 by default.</param>
    /// <param name="range">Max range of the camera offset on all axes from original position.</param>
    /// <param name="time">Length of the screenshake in seconds.</param>
    /// <param name="smoothRange">Should the range be smoothed towards 0?</param>
    /// <param name="smoothSpeed">Should the speed be smoothed towards 0?</param>
    /// <param name="timeScale">Change time during the shake. 1 = normal time. Must be != 0.</param>
    public void ShakeTrigger(float speed, float range, float time, bool smoothRange, bool smoothSpeed, float timeScale)
    {
        m_shake = true;
        m_smoothRange = smoothRange;
        m_smoothSpeed = smoothSpeed;

        m_shakeSpeed = speed;
        m_shakeRange = new Vector3(1, 1, 1) * range;
        m_shakeTime = time;
        m_timer = 0;
        Time.timeScale = timeScale;
    }
    /// <summary>
    /// Shakes screen on a frame-by-frame basis. 
    /// </summary>
    public void ShakeContinuously(float speed, float range)
    {
        m_shakeSpeed = speed;
        m_shakeRange = new Vector3(1, 1, 1) * range;
        m_shake = true;
        m_continousShake = true;
        m_smoothRange = false;
        m_smoothSpeed = false;
    }
    /// <summary>
    /// Immediately stop the active screen shake.
    /// </summary>
    public void Stop()
    {
        // resetting variables
        m_timer = 0;
        m_shake = false;
        Time.timeScale = 1;
        m_shake = false;
        m_continousShake = false;
    }
    // Manual test of the screen shake using the public variables in the component.
    void TestScreenShake()
    {
        ShakeTrigger(
            m_debugger.m_shakeSpeed,
            m_debugger.m_offsetRange,
            m_debugger.m_shakeTime,
            m_debugger.m_smoothRange,
            m_debugger.m_smoothSpeed,
            m_debugger.m_timeScale);
    }
}
