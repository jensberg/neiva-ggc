﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class GameCamera : MonoBehaviour
{
    // variables
    public AudioListener audioListener;
	public float speed = 3f;
	public Vector3 offset = new Vector3(0f, 1f, -6f);
	public Vector2 ahead = new Vector2(0.15f, 0f);
	[Range(0f, 1f)]
	public float projectileFocus = 0.5f;
	public float zoomingOutOnThrow = 1f;

	private float defaultCameraZoom;

	// GameManager (todo)
	NeivaController neiva;

	public Camera Camera { get; private set; }

	void Start()
	{
		neiva = FindObjectOfType<NeivaController>();
	}

	void LateUpdate()
	{
			GameState();

        if(audioListener != null)
            audioListener.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
	}

	void GameState()
	{
		if (neiva.selectedSoul.currentState == Soul.State.Throw)
		{
			Vector3 dv = neiva.selectedSoul.transform.position - neiva.transform.position;
			transform.position =
				Vector3.Lerp(
					transform.position,
					neiva.transform.position + dv * projectileFocus + offset + Vector3.back * dv.magnitude * zoomingOutOnThrow,
					Time.deltaTime * speed
					);
		}
		else
		{
			transform.position =
				Vector3.Lerp(
					transform.position,
					neiva.transform.position + offset + (Vector3)Vector2.Scale(neiva.rigidbody.velocity, ahead),
					Time.deltaTime * speed
					);
		}
	}

	void DeadState()
	{
		transform.position =
			Vector3.Lerp(
				transform.position,
				neiva.transform.position + offset + (Vector3)Vector2.Scale(neiva.rigidbody.velocity, ahead),
				Time.deltaTime * speed
				);
	}

}
