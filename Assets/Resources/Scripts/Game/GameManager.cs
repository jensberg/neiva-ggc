﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Collections;

/*
	Klass:	GameManager (singleton)
	Syfte:	........ 
			Använd GameManager.Singleton.[metod] för att komma åt singleton instansen från andra scripts.

*/

public class GameManager : MonoBehaviour
{
    // singleton

    static GameManager m_instance;
    public static GameManager Singleton { get { return m_instance; } }

    // variables

    NeivaController m_Neiva = null;

    // private variables

    private Checkpoint m_lastCheckpoint;
    private Vector3 m_checkpointPosition;
    [HideInInspector]
    public UI_Parent UIParent { get; private set; }
    private float DeathTime = 3f;
    private float DeathTimer = 0f;
    private string PauseType;
    private bool OnCheckpoint = false;
    private bool IsCouroutineRunning = false;
    private bool inMenu;
    public DevTeleport DevTeleport { get; private set; }
    public Noclip noclip { get; private set; }
	// accessors
	public NeivaController Neiva { get { return m_Neiva; } }
    public StateTree stateTree { get; private set; }

    // unity methods

    void Awake()
	{
		// preserve singleton instance
		if (m_instance == null)
			m_instance = this;
		else {
			Destroy(this);
			return;
		}

		// find NeivaController
		m_Neiva = FindObjectOfType<NeivaController>();
        UIParent = GameObject.FindGameObjectWithTag("UI_Parent").GetComponent<UI_Parent>();
        DevTeleport = GameObject.Find("DevTeleport").GetComponent<DevTeleport>();
        noclip = GameObject.Find("Noclip").GetComponent<Noclip>(); ;
        InitStateTree();
    }
    void InitStateTree()
    {
        stateTree = new StateTree(new State[]
            {
                new StateLeaf("Menu", InitMenu, UpdateMenu),
                new StateLeaf("Active", InitActive, UpdateActive),
                new StateLeaf("Pause", InitPause, UpdatePause),
                new StateLeaf("Death", null, UpdateDeath),
            }
            );
    }

    void FixedUpdate()
    {
        // update state tree
    }

    void Update()
    {
        stateTree.Update(Time.deltaTime);
    }

    void InitMenu()
    {
        inMenu = true;

        UIParent.gameObject.SetActive(false);
    }

    string UpdateMenu(float dt)
    {
        //Run menu
        if(!inMenu)
        return "Active";
        return "Menu";
	}

    void InitActive()
    {
        if(stateTree.previousState.Tag() == "Menu")
        {
            if(m_lastCheckpoint != null)
                m_lastCheckpoint.GetComponentInChildren<ParticleSystem>().Play(true);
            UIParent.gameObject.SetActive(true);
		}
	}

	private string UpdateActive(float dt)
    {
        if (Neiva.transform.position.y < -35.0f)
            Restart();
        if (Time.timeScale == 0)
            return "Pause";
        
        if(m_Neiva.health.GetHealth() <= 0 && m_Neiva.health.canPlayerDie)
        {
            return "Death";
        }
        return "Active";
    }

    void InitPause()
    {
        if(DevTeleport.gameObject != null)
        if(PauseType == "DevPause")
        {
            UIParent.DevPauseCanvas.SetActive(true);                        
        }
        else if (PauseType == "Pause")
        {
            UIParent.PauseCanvas.SetActive(true);
        }
        else
        {
                
        }
    }

    private string UpdatePause(float dt)
    {
        if (Time.timeScale == 0)
        {
            
            return "Pause";
        }
        else
        {
            UIParent.PauseCanvas.SetActive(false);
            UIParent.DevPauseCanvas.SetActive(false);
            return "Active";
        }
    }
    private string UpdateDeath(float dt)
    {
        DeathTimer += Time.deltaTime;
        DisablePlayer(DeathTime);
        if (DeathTimer > DeathTime)
        {
            DeathTimer = 0;
            Restart();
            return "Active";
		}
        return "Death";
    }
    public void SetCheckPoint(Checkpoint checkpoint)
    {
        OnCheckpoint = true;
        m_checkpointPosition = new Vector3(checkpoint.transform.position.x, checkpoint.transform.position.y + 1.5f, checkpoint.transform.position.z);
        m_lastCheckpoint = checkpoint;
    }

    public void ExitCheckPoint()
    {
        OnCheckpoint = false;
    }

    public void SetPause(string type)
    {
        PauseType = type;
    }

    public void Restart()
    {
        if (m_checkpointPosition != Vector3.zero)
        {
            m_Neiva.transform.position = new Vector3(m_checkpointPosition.x, m_checkpointPosition.y, m_Neiva.transform.position.z);
            m_Neiva.health.Restore();
			m_Neiva.soulThrowerController.activeSoul.CancelThrow();
			m_Neiva.soulThrowerController.activeSoul.transform.position = m_Neiva.transform.position;
        }
        else
            RestartScene();
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex -1);
    }
    public void RestartSceneAfter(float time)
    {
        StartCoroutine(RestartAfter(time));
    }

    private IEnumerator RestartAfter(float time)
    {
        yield return new WaitForSeconds(time);
            RestartScene();
    }

    public void DisablePlayer(float disableTime)
    {
        if(IsCouroutineRunning == false)
        {
            Neiva.rigidbody.velocity = Vector3.zero;
            Neiva.GetComponent<NeivaUserControl>().enabled = false;
            Neiva.GetComponent<NeivaController>().enabled = false;
            Neiva.moveController.Move(Vector3.zero);
            Neiva.jumpController.enabled = false;
            StartCoroutine(Disable(disableTime));
        }
    }

    private IEnumerator Disable(float disableTime)
    {
        IsCouroutineRunning = true;
        yield return new WaitForSeconds(disableTime);
        Neiva.GetComponent<NeivaUserControl>().enabled = true;
        Neiva.GetComponent<NeivaController>().enabled = true;
        Neiva.jumpController.enabled = true;
        IsCouroutineRunning = false;
    }

    public void InMenu(bool state)
    {
        inMenu = state;
    }

}
