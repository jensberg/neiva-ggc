﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

/*
Klass:	GameAudio (singleton)
Syfte:	........ 
		Använd GameAudio.Singleton.[metod] för att komma åt singleton instansen från andra scripts.
*/
[System.Serializable]
public class VolumeControls
{
    [Range(-80, 20)]
    public float m_masterVolume;
    [Range(-80, 20)]
    public float m_SfxVolume;
    [Range(-80, 20)]
    public float m_musicVolume;
    [Range(-80, 20)]
    public float m_ambienceVolume;
}
[RequireComponent(typeof (MusicTriggerHandler))]

public class GameAudio : MonoBehaviour {

    static GameAudio m_instance;
    public static GameAudio Singleton { get { return m_instance; } }

    //Private variables
    private AudioMixer m_masterMixer;

    //Public variables
    public VolumeControls m_volumeControl;
    public bool m_MuteAll;

    public bool stopControl { get; set; }
    //Unity Methods
    void Awake ()
    {
        // preserve singleton instance
        if (m_instance == null)
            m_instance = this;
        else {
            Destroy(this);
            return;
        }
        DontDestroyOnLoad(this);
        //Load mixer
        m_masterMixer = Resources.Load("Audio/Mixer/MasterMixer") as AudioMixer;
        if (m_masterMixer == null)
            Debug.LogError("Could not find master mixer at Resources/Audio/Mixer/");

        stopControl = false;
    }
	
    void Update()
    {
        if(!stopControl)
        {
            m_masterMixer.SetFloat("masterVol", m_volumeControl.m_masterVolume);
            m_masterMixer.SetFloat("sfxVol", m_volumeControl.m_SfxVolume);
            m_masterMixer.SetFloat("ambienceVol", m_volumeControl.m_ambienceVolume);
            m_masterMixer.SetFloat("musicVol", m_volumeControl.m_musicVolume);
        }

        if(m_MuteAll)
        {
            m_masterMixer.SetFloat("masterVol", -80);
            m_masterMixer.SetFloat("sfxVol", -80);
            m_masterMixer.SetFloat("ambienceVol", -80);
            m_masterMixer.SetFloat("musicVol", -80);
        }
    }
}
