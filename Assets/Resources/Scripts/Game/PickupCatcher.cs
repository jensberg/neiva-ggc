﻿using UnityEngine;
using System.Collections;

public class PickupCatcher : MonoBehaviour {
    private GameObject target;
    public Vector3 offset;
    public float speed = 1f;
    private Transform m_transform;
	// Use this for initialization
	void Start () {
        target = GameManager.Singleton.Neiva.gameObject;
        m_transform = GetComponent<Transform>();
        m_transform.position = target.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if(target)
        m_transform.position = Vector3.MoveTowards(m_transform.position, new Vector3(target.transform.position.x + offset.x, target.transform.position.y + offset.y, target.transform.position.z + offset.z), Time.deltaTime * speed);
	}
}
