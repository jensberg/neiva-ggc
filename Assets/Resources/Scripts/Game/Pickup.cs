﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Pickup : MonoBehaviour {

    public AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
    private GameObject player;
    private GameObject pickupCatcher;
    private Vector3 targetPosition;

    public float distance = 5f;
    public float speed = 2f;
    private float curveTimer = 0;
    private bool isTargetFound;

    [Space]
    public GameObject deathParticles;
    public GameObject idleParticles;

    public UnityEvent OnPickup;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        pickupCatcher = GameObject.FindGameObjectWithTag("PickupCatcher");
        if (pickupCatcher == null)
            this.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        //Distance move to player
        if (Vector3.Distance(transform.position, player.transform.position) < distance || Vector3.Distance(transform.position, pickupCatcher.transform.position) < distance)
        {
            isTargetFound = true;
        }
        if(isTargetFound)
        {
            curveTimer += Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * curve.Evaluate(curveTimer) * Time.deltaTime);
        }
	}

    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Player")
        {
            GameManager.Singleton.Neiva.AddSouls(1);

            //Disable
            Destroy(this.gameObject, 5f);

            deathParticles.SetActive(true);
            GetComponent<Collider>().enabled = false;
            idleParticles.SetActive(false);
            GetComponent<MeshRenderer>().enabled = false;

            //if(OnPickup)
        }
    }
#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, distance);
    }
#endif
}
