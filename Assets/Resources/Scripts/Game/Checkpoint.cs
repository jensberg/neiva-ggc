﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour {
    public float TriggerRange = 15f;

    private bool isTriggered = false;
    private GameObject Player;
    //Emissive
    private Material mat;
    private float emissionMultiplier = 1f;
    private float emissionSpeed = 2.5f;
    private Color basecolor;
    //Idle particles
    private GameObject idleParticle;
    private GameObject checkpointPlane;
    // Use this for initialization
    void Start () {
        Player = GameObject.FindGameObjectWithTag("Player");

        //Emissive
        mat = GetComponent<MeshRenderer>().material;
        basecolor = mat.GetColor("_EmissionColor");
        emissionMultiplier = 0;
        mat.SetColor("_EmissionColor", basecolor * emissionMultiplier);

        //Idle particle
        idleParticle = transform.GetChild(1).gameObject;
        idleParticle.SetActive(false);

        checkpointPlane = transform.GetChild(2).gameObject;
        checkpointPlane.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        if (transform.position.x < Player.transform.position.x)
        {        
            if (isTriggered)
                LerpEmissive();
        }
	}

    void LerpEmissive()
    {
        emissionMultiplier = Mathf.MoveTowards(emissionMultiplier, 1, Time.deltaTime * emissionSpeed);
        mat.SetColor("_EmissionColor", basecolor * emissionMultiplier);
        if (emissionMultiplier == 1)
            this.enabled = false;
    }
    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Player")
        {
            //Idle particles
            idleParticle.SetActive(true);
            checkpointPlane.SetActive(true);

            //Unityevent for sound

            //Tell position to manager
            GameManager.Singleton.SetCheckPoint(this);

            //Heal player
            GameManager.Singleton.Neiva.health.Restore();

            //Turn off script handling this checkpoint
            isTriggered = true;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.tag == "Player")
        {
            GameManager.Singleton.ExitCheckPoint();
        }
    }
}
