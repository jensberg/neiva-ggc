﻿using UnityEngine;
using System.Collections;

public class SoulCheckpoint : MonoBehaviour {
    public TutorialManager tutorialCanvas = null;
    public bool isShrine = false;

    //Emissive
    private Material[] mat = new Material[3];
    private Color[] basecolor = new Color[3];
    private float emissionMultiplier = 1f;

    //
    private bool active;
    private Animator animator;
    private EventTrigger eventTrigger;

    //Move towards player
    private GameObject player;
    private bool isTargetFound = false;
    private float curveTimer;
    [Space]
    public float distance = 5f;
    public float speed = 2f;
    public AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

    //Remove on contact
    private GameObject child;
    private float lifeTimer = -1;

    //AltarProjectiles
    private NeivaParticleManager particleManager;

    //Sound
    private AudioSource audioSource;
    public AudioClip soundEffect;

    //

    public enum Soul
    {
        Bounce,
        Swap,
    } public Soul soul;
    // Use this for initialization
    void Start ()
    {

        audioSource = GetComponent<AudioSource>();
        if(audioSource != null)
        audioSource.enabled = false;

        particleManager = FindObjectOfType<NeivaParticleManager>();

        player = GameManager.Singleton.Neiva.transform.GetChild(0).GetChild(0).gameObject;
        child = transform.GetChild(0).gameObject;
        animator = GameManager.Singleton.Neiva.GetComponent<Animator>();

        if(!isShrine)
        eventTrigger = GetComponent<EventTrigger>();
        if(isShrine)
        {
            for(int i = 0; i < mat.Length; i++)
            {
                mat[i] = transform.GetChild(i).GetComponent<MeshRenderer>().material;
                basecolor[i] = mat[i].GetColor("_EmissionColor");
                emissionMultiplier = 0;
                mat[i].SetColor("_EmissionColor", basecolor[i] * emissionMultiplier);
            }
            eventTrigger = transform.GetChild(mat.Length).GetComponent<EventTrigger>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (active && isShrine)
        {
            if (lifeTimer >= 0)
            {
                lifeTimer += Time.deltaTime;

                if(lifeTimer >= 0.7f)
                {
                    animator.SetFloat("VelocityX", 1);

                    animator.SetTrigger("Altar");
                }

                if (lifeTimer >= 2.05f)
                {
                    particleManager.AltarParticles.SetActive(true);
                    if (audioSource != null)
                        audioSource.enabled = true;
                }
                if(lifeTimer >= 6.6f)
                {
                    animator.SetBool("Altar", false);

                    particleManager.AltarParticles.SetActive(false);
                }

                if (lifeTimer >= 7f)
                {
                    GameManager.Singleton.Neiva.moveController.Move(Vector3.right);
                }

                emissionMultiplier = Mathf.MoveTowards(emissionMultiplier, 1, Time.deltaTime * 0.3f);
                for (int i = 0; i < mat.Length; i++)
                {
                    mat[i].SetColor("_EmissionColor", basecolor[i] * emissionMultiplier);
                }
            }
        }

            if (!isShrine)
            {
                if (Vector3.Distance(transform.position, player.transform.position) < distance)
                {
                    isTargetFound = true;
                }
                if (isTargetFound)
                {
                    curveTimer += Time.deltaTime;
                    transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * curve.Evaluate(curveTimer) * Time.deltaTime);
                }

                if (Vector3.Distance(transform.position, player.transform.position) < 0.1f)
                {
                    child.SetActive(false);
                }

                if (lifeTimer >= 0)
                {
                    lifeTimer += Time.deltaTime;
                    if (lifeTimer >= 10f)
                        Destroy(this.gameObject);
                }
            }
        
    }


    public void Ontrigger()
    {
        lifeTimer = 0;
        active = true;
        GameManager.Singleton.Neiva.selectedSoul.SetHiding(true);
        eventTrigger.DisableTrigger(true);
        if(!isShrine)
        {
            animator.SetTrigger("Shrine");
            GameManager.Singleton.DisablePlayer(4f);

            if (soul == Soul.Bounce)
                tutorialCanvas.ActivateBounce();
            if (soul == Soul.Swap)
                tutorialCanvas.ActivateSwap();

            if (!audioSource.isPlaying)
            {
                audioSource.enabled = true;
            }
        }

        if(isShrine)
        {

            animator.SetFloat("VelocityX", 0);

            GameManager.Singleton.Neiva.moveController.Move(Vector3.zero);
        }
    }

    public void OnMovementTrigger()
    {
        GameManager.Singleton.DisablePlayer(100f);
        GameManager.Singleton.Neiva.moveController.Move(Vector3.right);
        GameManager.Singleton.Neiva.GetComponent<NeivaAnimation>().enabled = false;
        Animator animator = GameManager.Singleton.Neiva.GetComponent<Animator>();
        animator.SetFloat("AimX", 0);
        animator.SetFloat("AimY", 0);
        animator.SetFloat("VelocityY", 0);
        animator.SetBool("Grounded", true);
        GameManager.Singleton.Neiva.selectedSoul.SetHiding(true);
        particleManager.ChargeParticles.SetActive(false);

    }
#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, distance);
    }
#endif
}
