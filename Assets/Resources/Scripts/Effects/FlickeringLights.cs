﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class FlickeringLights : MonoBehaviour {

    public float timeBetweenFlashes = 6;
    public float flashLenght = 1.2f;
    public float flickeringWaitTime = 0.04f;

    public float maxRange = 20f;
    public float minRange = 15f;

    NeivaController neiva;
    Light pointLight;
    bool start;

    public bool flash { get; set; }
    float timer;

    void Start()
    {
        pointLight = GetComponent<Light>();
        neiva = FindObjectOfType<NeivaController>();
        if (neiva == null)
            Debug.Log("No neiva found");   
    }
	// Update is called once per frame
	void Update ()
    {
        if(start)
        {
            flash = true;
            StartCoroutine(PlayLights());
            start = false;
        }
    }
    
    public void TriggerLights()
    {
        start = true;
    }

    IEnumerator PlayLights()
    {
        while(flash)
        {
            pointLight.range = Random.Range(minRange, maxRange);
            pointLight.enabled = !(pointLight.enabled);
            yield return new WaitForSeconds(flickeringWaitTime);
        }
        pointLight.enabled = false;
    }
}
