﻿using UnityEngine;
using System.Collections;

public class DestroyGameObject : MonoBehaviour {

    public float timeBeforeDestroy;
    private float timer;
    
	void Update ()
    {
        timer += Time.deltaTime;

        if (timer > timeBeforeDestroy)
            Destroy(this.gameObject);
	}
}
