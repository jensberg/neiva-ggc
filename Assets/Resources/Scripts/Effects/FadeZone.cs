﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityStandardAssets.ImageEffects;

public class FadeZone : MonoBehaviour
{

    [System.Serializable]
    public class Fog
    {
        public bool enabled;
        [Header("Target Values:")]
        public Color fogColor;
        public float startDistance;
        public float endDistance;

        [HideInInspector]
        public Color originalFogColor;
        [HideInInspector]
        public float originalStart;
        [HideInInspector]
        public float originalEnd;

    }
    [System.Serializable]
    public class LightColorSettings
    {
        public bool enabled;
        [Header("Target Values:")]
        public Color skyColor;
        public Color equatorColor;
        public Color groundColor;
        public float ambientIntensity;

        [HideInInspector]
        public Color originalSkyColor;
        [HideInInspector]
        public Color originalEquatorColor;
        [HideInInspector]
        public Color originalGroundColor;
        [HideInInspector]
        public float originalAmbientIntensity;
    }
    [System.Serializable]
    public class DirectionalLightSettings
    {
        public bool enabled;
        public Light light;

        [HideInInspector]
        public Quaternion rotation;
        [HideInInspector]
        public float shadowStrength;
        [HideInInspector]
        public Color color;
        [HideInInspector]
        public float intensity;
        [HideInInspector]
        public float bounceIntensity;

        [HideInInspector]
        public Quaternion originalRotation;
        [HideInInspector]
        public float originalShadowStrength;
        [HideInInspector]
        public Color originalColor;
        [HideInInspector]
        public float originalIntensity;
        [HideInInspector]
        public float originalBounceIntensity;

    }
    [System.Serializable]
    public class WindZoneSettings
    {
        public bool enabled;
        public WindZone windZone;

        [Header("Target Values:")]
        public float main;
        public float turbulence;
        public float pulseMagnitude;
        public float pulseFrequency;

        [HideInInspector]
        public float originalMain;
        [HideInInspector]
        public float originalTurbulence;
        [HideInInspector]
        public float originalPulseMagnitude;
        [HideInInspector]
        public float originalPulseFrequency;


    }
    [Header("Zone Settings:")]
    public Vector2 size;
    public float transitionRadius;
    public FadeZone linkedZone;

    [Header("Image Effect Settings:")]
    public Fog fogSettings;
    public LightColorSettings lightColorSettings;
    public DirectionalLightSettings directionalLightSettings;
    [Header("Wind Zone:")]
    public WindZoneSettings windZoneSettings;

    // public variables
    [Space]
    public float fadeSpeed = 3f;
    public float forceTransitionOffset = 3f;

    // private variables
    NeivaController neiva;
    Rect transitionRect;
    Rect sizeRect;
    float percent;
    bool hasEntered;
    bool isFirstZone;
    bool forceStop;

    float targetFogStart;
    float targetFogEnd;
    Color targetFogColor;

    float targetAmbientIntensity;
    Color targetEquatorColor;
    Color targetGroundColor;
    Color targetSkyColor;

    Quaternion targetRotation;
    float targetShadowStrength;
    Color targetDirLightColor;
    float targetIntensity;
    float targetBounceIntensity;

    float targetMain;
    float targetTurbulence;
    float targetPulseMagnitude;
    float targetPulseFrequency;

    void Awake()
    {
        transitionRect = new Rect(transform.position, new Vector2(size.x + transitionRadius, size.y + transitionRadius));
        transitionRect.center = transform.position;

        sizeRect = new Rect(transform.position, new Vector2(size.x, size.y));
        sizeRect.center = transform.position;

        neiva = FindObjectOfType<NeivaController>();

        if (directionalLightSettings.light == null)
            directionalLightSettings.enabled = false;
        if (windZoneSettings.windZone == null)
            windZoneSettings.enabled = false;

        if (linkedZone != null)
            linkedZone.isFirstZone = true;
    }

    void Update()
    {
        // first encounter
        if (!hasEntered)
        {
            CacheOriginalValues();
        }
        // in main area
        if (sizeRect.Contains(neiva.transform.position))
        {
            percent = 1f;
            
            if (linkedZone != null) 
                linkedZone.hasEntered = false; //Not a great solution... Stop previous zone from updating
        }

        // in transition area
        else if (transitionRect.Contains(neiva.transform.position))
        {
            //if (linkedZone != null)
            //    linkedZone.forceStop = true;
            hasEntered = true;
            if (neiva.transform.position.x < sizeRect.xMin)
            {
                float _offset = neiva.transform.position.x - transitionRect.xMin;
                percent = _offset / (sizeRect.xMin - transitionRect.xMin);
            }
            else if (neiva.transform.position.x > sizeRect.xMax)
            {
                float _offset = neiva.transform.position.x - transitionRect.xMax;
                percent = _offset / (sizeRect.xMax - transitionRect.xMax);
            }
        }
        else
            percent = 0;
        
        // Set all effects
        if(hasEntered)
        {
            if (fogSettings.enabled)
                SetFog();
            if (lightColorSettings.enabled)
                SetAmbientColors();
            if (directionalLightSettings.enabled)
                SetDirectionalLight();
            if (windZoneSettings.enabled)
                SetWindZone();
            
            //else if(!forceStop)
            //{
            //    if (fogSettings.enabled)
            //        SetFog();
            //    if (lightColorSettings.enabled)
            //        SetAmbientColors();
            //    if (directionalLightSettings.enabled)
            //        SetDirectionalLight();
            //    if (windZoneSettings.enabled)
            //        SetWindZone();
            //}
        }

        // finsish transition if player left area
        if (hasEntered && neiva.transform.position.x < transitionRect.xMin - forceTransitionOffset || neiva.transform.position.x > transitionRect.xMax + forceTransitionOffset)
        {
            if(!isFirstZone)
            {
                hasEntered = false;
                ResetValues();
            }
        }
    }

    void SetAmbientColors()
    {
        targetAmbientIntensity = lightColorSettings.originalAmbientIntensity + (lightColorSettings.ambientIntensity - lightColorSettings.originalAmbientIntensity) * percent;
        RenderSettings.ambientIntensity = Mathf.Lerp(RenderSettings.ambientIntensity, targetAmbientIntensity, Time.deltaTime * fadeSpeed);

        targetEquatorColor = lightColorSettings.originalEquatorColor + (lightColorSettings.equatorColor - lightColorSettings.originalEquatorColor) * percent;
        RenderSettings.ambientEquatorColor = Color.Lerp(RenderSettings.ambientEquatorColor, targetEquatorColor, Time.deltaTime * fadeSpeed);
        
        targetGroundColor = lightColorSettings.originalGroundColor + (lightColorSettings.groundColor - lightColorSettings.originalGroundColor) * percent;
        RenderSettings.ambientGroundColor = Color.Lerp(RenderSettings.ambientGroundColor, targetGroundColor, Time.deltaTime * fadeSpeed);

        targetSkyColor = lightColorSettings.originalSkyColor + (lightColorSettings.skyColor - lightColorSettings.originalSkyColor) * percent;
        RenderSettings.ambientSkyColor = Color.Lerp(RenderSettings.ambientSkyColor, targetSkyColor, Time.deltaTime * fadeSpeed);
    }

    void SetFog()
    {
       targetFogStart = fogSettings.originalStart + (fogSettings.startDistance - fogSettings.originalStart) * percent;
       RenderSettings.fogStartDistance = Mathf.Lerp(RenderSettings.fogStartDistance, targetFogStart, Time.deltaTime * fadeSpeed);

       targetFogEnd = fogSettings.originalEnd + (fogSettings.endDistance - fogSettings.originalEnd) * percent;
       RenderSettings.fogEndDistance = Mathf.Lerp(RenderSettings.fogEndDistance, targetFogEnd, Time.deltaTime * fadeSpeed);

       targetFogColor = fogSettings.originalFogColor + (fogSettings.fogColor - fogSettings.originalFogColor) * percent;
       RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor, targetFogColor, Time.deltaTime * fadeSpeed);
    }

    void SetDirectionalLight()
    {
        targetDirLightColor = directionalLightSettings.originalColor + (directionalLightSettings.color - directionalLightSettings.originalColor) * percent;
        directionalLightSettings.light.color = Color.Lerp(directionalLightSettings.light.color, targetDirLightColor, Time.deltaTime * fadeSpeed);

        targetBounceIntensity = directionalLightSettings.originalBounceIntensity + (directionalLightSettings.bounceIntensity - directionalLightSettings.originalBounceIntensity) * percent;
        directionalLightSettings.light.bounceIntensity = Mathf.Lerp(directionalLightSettings.light.bounceIntensity, targetBounceIntensity, Time.deltaTime);

        targetIntensity = directionalLightSettings.originalIntensity + (directionalLightSettings.intensity - directionalLightSettings.originalIntensity) * percent;
        directionalLightSettings.light.intensity = Mathf.Lerp(directionalLightSettings.light.intensity, targetIntensity, Time.deltaTime);

        targetRotation = Quaternion.Lerp(directionalLightSettings.originalRotation, directionalLightSettings.rotation, percent);
        directionalLightSettings.light.transform.rotation = Quaternion.Lerp(directionalLightSettings.light.transform.rotation, targetRotation, Time.deltaTime * fadeSpeed);
    }

    void SetWindZone()
    {
        targetMain = windZoneSettings.originalMain + (windZoneSettings.main - windZoneSettings.originalMain) * percent;
        windZoneSettings.windZone.windMain = Mathf.Lerp(windZoneSettings.windZone.windMain, targetMain, Time.deltaTime * fadeSpeed);

        targetPulseFrequency = windZoneSettings.originalPulseFrequency + (windZoneSettings.pulseFrequency - windZoneSettings.originalPulseFrequency) * percent;
        windZoneSettings.windZone.windPulseFrequency = Mathf.Lerp(windZoneSettings.windZone.windPulseFrequency, targetPulseFrequency, Time.deltaTime * fadeSpeed);

        targetPulseMagnitude = windZoneSettings.originalPulseMagnitude + (windZoneSettings.pulseMagnitude - windZoneSettings.originalPulseMagnitude) * percent;
        windZoneSettings.windZone.windPulseMagnitude = Mathf.Lerp(windZoneSettings.windZone.windPulseMagnitude, targetPulseMagnitude, Time.deltaTime * fadeSpeed);

        targetTurbulence = windZoneSettings.originalTurbulence + (windZoneSettings.turbulence - windZoneSettings.originalTurbulence) * percent;
        windZoneSettings.windZone.windTurbulence = Mathf.Lerp(windZoneSettings.windZone.windTurbulence, targetTurbulence, Time.deltaTime * fadeSpeed);
    }

    void ResetValues()
    {
        if(fogSettings.enabled)
        {
            RenderSettings.fogStartDistance = fogSettings.originalStart;
            RenderSettings.fogEndDistance = fogSettings.originalEnd;
            RenderSettings.fogColor = fogSettings.originalFogColor;
        }
        
        if(lightColorSettings.enabled)
        {
            RenderSettings.ambientIntensity = lightColorSettings.originalAmbientIntensity;
            RenderSettings.ambientEquatorColor = lightColorSettings.originalEquatorColor;
            RenderSettings.ambientGroundColor = lightColorSettings.originalGroundColor;
            RenderSettings.ambientSkyColor = lightColorSettings.originalSkyColor;
        }
        
        if(directionalLightSettings.enabled)
        {
            directionalLightSettings.light.color = directionalLightSettings.originalColor;
            directionalLightSettings.light.intensity = directionalLightSettings.originalIntensity;
            directionalLightSettings.light.bounceIntensity = directionalLightSettings.originalBounceIntensity;
            directionalLightSettings.light.transform.rotation = directionalLightSettings.originalRotation;
            directionalLightSettings.light.shadowStrength = directionalLightSettings.originalShadowStrength;
            
        }

        if(windZoneSettings.enabled)
        {
            windZoneSettings.windZone.windMain = windZoneSettings.originalMain;
            windZoneSettings.windZone.windPulseFrequency = windZoneSettings.originalPulseFrequency;
            windZoneSettings.windZone.windPulseMagnitude = windZoneSettings.originalPulseMagnitude;
            windZoneSettings.windZone.windTurbulence = windZoneSettings.originalTurbulence;
        }
    }

    void CacheOriginalValues()
    {
        if (fogSettings.enabled)
        {
            fogSettings.originalStart = linkedZone == null ? RenderSettings.fogStartDistance : linkedZone.fogSettings.startDistance;
            fogSettings.originalEnd = linkedZone == null ? RenderSettings.fogEndDistance : linkedZone.fogSettings.endDistance;
            fogSettings.originalFogColor = linkedZone == null ? RenderSettings.fogColor : linkedZone.fogSettings.fogColor;
        }
        if (lightColorSettings.enabled)
        {
            lightColorSettings.originalAmbientIntensity = linkedZone == null ? RenderSettings.ambientIntensity : linkedZone.lightColorSettings.ambientIntensity;
            lightColorSettings.originalEquatorColor = linkedZone == null ? RenderSettings.ambientEquatorColor : linkedZone.lightColorSettings.equatorColor;
            lightColorSettings.originalGroundColor = linkedZone == null ? RenderSettings.ambientGroundColor : linkedZone.lightColorSettings.groundColor;
            lightColorSettings.originalSkyColor = linkedZone == null ? RenderSettings.ambientSkyColor : linkedZone.lightColorSettings.skyColor;
        }
        if (directionalLightSettings.enabled)
        {
            directionalLightSettings.originalBounceIntensity = linkedZone == null ? directionalLightSettings.light.bounceIntensity : linkedZone.directionalLightSettings.bounceIntensity;
            directionalLightSettings.originalColor =           linkedZone == null ? directionalLightSettings.light.color : linkedZone.directionalLightSettings.color;
            directionalLightSettings.originalIntensity =       linkedZone == null ? directionalLightSettings.light.intensity : linkedZone.directionalLightSettings.intensity;
            directionalLightSettings.originalShadowStrength =  linkedZone == null ? directionalLightSettings.light.shadowStrength : linkedZone.directionalLightSettings.shadowStrength;
            directionalLightSettings.originalRotation =        linkedZone == null ? directionalLightSettings.light.transform.rotation : linkedZone.directionalLightSettings.rotation;

        }
        if (windZoneSettings.enabled)
        {
            windZoneSettings.originalMain =             linkedZone == null ? windZoneSettings.windZone.windMain : linkedZone.windZoneSettings.main;
            windZoneSettings.originalPulseFrequency =   linkedZone == null ? windZoneSettings.windZone.windPulseFrequency : linkedZone.windZoneSettings.pulseFrequency;
            windZoneSettings.originalPulseMagnitude =   linkedZone == null ? windZoneSettings.windZone.windPulseMagnitude : linkedZone.windZoneSettings.pulseMagnitude;
            windZoneSettings.originalTurbulence =       linkedZone == null ? windZoneSettings.windZone.windTurbulence : linkedZone.windZoneSettings.turbulence;
        }
    }

#if UNITY_EDITOR
    [Header("Editor")]
    public Light saveLightSettings = null;

    void OnValidate()
    {
        if (saveLightSettings != null)
        {
            directionalLightSettings.bounceIntensity = saveLightSettings.bounceIntensity;
            directionalLightSettings.color           = saveLightSettings.color;
            directionalLightSettings.intensity       = saveLightSettings.intensity;
            directionalLightSettings.shadowStrength  = saveLightSettings.shadowStrength;
            directionalLightSettings.rotation        = saveLightSettings.transform.rotation;
            
            saveLightSettings = null;
        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 1, 1, 0.45f);
        Gizmos.DrawCube(transform.position, new Vector3(size.x + transitionRadius, size.y + transitionRadius, 0));
        Gizmos.color = new Color(1, 1, 1, 0.7f);
        Gizmos.DrawCube(transform.position, new Vector3(size.x, size.y, 0));
    }
#endif
}
