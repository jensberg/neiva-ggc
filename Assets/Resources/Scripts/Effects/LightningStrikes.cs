﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class LightningStrikes : MonoBehaviour {

    AudioSource audioSource;
    ParticleSystem flashParticles;
    float timer;
    bool playerIsNear;

    public float delayBetweenStrikes;
    public FlickeringLights flickeringLights;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        flashParticles = GetComponent<ParticleSystem>();
    }

	void Update ()
    {
        if(playerIsNear)
        {
            timer += Time.deltaTime;
            if (timer > delayBetweenStrikes)
            {
                audioSource.Play();
                timer = 0;
                flickeringLights.TriggerLights();
                flashParticles.Play();
            }
            if (timer > 0.3)
            {
                flickeringLights.flash = false;
            }
        }
    }

    public void PlayerTriggered(bool trigger)
    {
        playerIsNear = trigger;
    }
}
