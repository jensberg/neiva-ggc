﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class LedgeClimber : MonoBehaviour
{
	// variables
	public LayerMask layerMask = 1;
	[Space]
	public Vector3 wallCheckOrigin = Vector3.zero;
	public float wallCheckRadius = 1f;
	[Space]
	public Vector3 ledgeCheckOrigin = Vector3.right;
	public float ledgeCheckHeight = 1f;
	[Space]
	public float climbSpeed = 5f;
	public float climbMaxDuration = 0.6f;
	[Space]
	public float onExitVelocity = 5f;
	[Range(90f, 0f)]
	public float onExitVelocityAngle = 45f;

	public UnityAction onClimbEnter = null;
	public UnityAction onClimbRunning = null;
	public UnityAction onClimbExit = null;

	// accessors
	public new Rigidbody rigidbody { get; private set; }
	public bool isClimbing { get; private set; }
	public TStateMachine<LedgeClimber> stateMachine { get; private set; }
	public RaycastHit ledgeHit { get; private set; }
	public bool climbInput { get; private set; }

	// methods
	void Awake()
	{
		rigidbody = GetComponent<Rigidbody>();
		stateMachine = new TStateMachine<LedgeClimber>(this, new IdleState());
	}

	void FixedUpdate()
	{
		stateMachine.update(Time.fixedDeltaTime);
	}

	public void Control(bool climb)
	{
		climbInput = climb;
	}

	public bool CheckLedge()
	{
		Ray ray = new Ray(transform.position + Vector3.Scale(Vector3.up + Vector3.right * transform.right.x, ledgeCheckOrigin), Vector3.up);
		RaycastHit ceilingHit = new RaycastHit();
		bool anyCeiling = Physics.Raycast(ray, out ceilingHit, ledgeCheckHeight, layerMask);

		// reverse ray to check any ground hit
		ray.origin = ray.GetPoint(ledgeCheckHeight);
		ray.direction = Vector3.down;

		RaycastHit groundHit = new RaycastHit();
		bool anyGround = Physics.Raycast(ray, out groundHit, ledgeCheckHeight - 0.1f, layerMask);

		if ((anyCeiling && anyGround) ? (groundHit.collider == ceilingHit.collider) : (anyGround && !anyCeiling))
		{
			ledgeHit = groundHit;
			return true;
		}
		else
			return false;
	}

	public bool CheckWall()
	{
		return Physics.OverlapSphere(transform.position + wallCheckOrigin, wallCheckRadius, layerMask).Length > 0;
	}

	class IdleState : TState<LedgeClimber>
	{
		public override TState<LedgeClimber> run()
		{
			return obj.climbInput && obj.CheckWall() && obj.CheckLedge() ? new ClimbState() : this as TState<LedgeClimber>;
		}
	}
	class ClimbState : TState<LedgeClimber>
	{
		bool wasKinematic;
		Vector3 startPosition;

		public override void init()
		{
			obj.isClimbing = true;

			if (obj.onClimbEnter != null)
				obj.onClimbEnter();
		}
		public override TState<LedgeClimber> run()
		{
			Vector3 vel = obj.rigidbody.velocity;
			vel.y = obj.climbSpeed;
			obj.rigidbody.velocity = vel;

			if (obj.onClimbRunning != null)
				obj.onClimbRunning();

			return !obj.climbInput || obj.stateMachine.stateTime >= obj.climbMaxDuration || !obj.CheckLedge() ? new IdleState() : this as TState<LedgeClimber>;
		}
		public override void exit()
		{
			obj.isClimbing = false;
			obj.rigidbody.velocity = Quaternion.Euler(0f, 0f, obj.transform.right.x < 0f ? -obj.onExitVelocityAngle : obj.onExitVelocityAngle) * obj.transform.right * obj.onExitVelocity;

			if (obj.onClimbExit != null)
				obj.onClimbExit();
		}
	}


#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		Gizmos.color = isClimbing ? Color.green : Color.red;
		Gizmos.DrawLine(
			transform.position + Vector3.Scale(Vector3.up + Vector3.right * transform.right.x, ledgeCheckOrigin),
			transform.position + Vector3.Scale(Vector3.up + Vector3.right * transform.right.x, ledgeCheckOrigin) + Vector3.up * ledgeCheckHeight);
		Gizmos.DrawWireSphere(transform.position + wallCheckOrigin, wallCheckRadius);
		Gizmos.DrawSphere(ledgeHit.point, 0.1f);
	}
#endif
}
