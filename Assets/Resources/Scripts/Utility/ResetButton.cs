﻿using UnityEngine;
using UnityEngine.Internal;
using System.Collections;

public class ResetButton : MonoBehaviour
{
	public Transform character = null; 
	public KeyCode key = KeyCode.Escape;

	Vector3 originalPosition = Vector3.zero;

	void Awake()
	{
		originalPosition = character.position;
	}

	void Update()
	{
		if (Input.GetKeyDown(key))
			character.position = originalPosition;
    }
}
