﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DevTeleport : MonoBehaviour {
    public GameObject[] Points;
    private int Selection = 0;
    private float Xon = 0;
    private float SelectionTimer;
    private float SelectionTime;

	// Use this for initialization
	void Start () {
        Points = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            Points[i] = transform.GetChild(i).gameObject;

        }
        this.enabled = false;
	}

    // Update is called once per frame
    void Update()
    {
        Xon = (Input.GetAxis("LS X"));
        SelectionTimer += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.D) || Xon > 0.5f)
        {
            if (SelectionTimer > 0.5f)
            {
                SelectionTimer = 0;
                
                if (Selection < Points.Length)
                {
                    Selection += 1;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.A) || Xon < -0.5f)
        {
            if (SelectionTimer > 0.5f)
            {
                SelectionTimer = 0;

                if (Selection > 0)
                {
                    Selection -= 1;
                }
            }
        }
    } 

    public void TeleportNext()
    {
        Selection += 1;
        GameObject.Find("Fade").GetComponent<Fading>().FadeOut();
        StartCoroutine(TeleportIn(1.0f));
    }

    private IEnumerator TeleportIn(float time)
    {
        
        yield return new WaitForSeconds(time);
        StartCoroutine(TeleportOut(time));
        GameObject.FindGameObjectWithTag("PickupCatcher").transform.position = Points[Selection].transform.position;
        GameManager.Singleton.Neiva.transform.position = Points[Selection].transform.position;
        CameraManager cameraManager = (CameraManager)FindObjectOfType(typeof(CameraManager));
        cameraManager.ResetToNeiva();
        if (Selection == Points.Length)
            Selection = 0;
    }
    private IEnumerator TeleportOut(float time)
    {
        yield return new WaitForSeconds(time);
        GameObject.Find("Fade").GetComponent<Fading>().FadeIn();

    }
}
