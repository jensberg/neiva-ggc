﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class FloatingRigidbody : MonoBehaviour
{
	// variables
	public Vector3 forceAmount = Vector3.one * 50f;
	public Vector3 dragAmount = Vector3.one * 10f;
	[HideInInspector]
	public Vector3 targetPosition = Vector3.zero;

	// accessors
	public Rigidbody Rigidbody { get; private set; }

	// methods
	void Awake()
	{
		Rigidbody = GetComponent<Rigidbody>();
		targetPosition = transform.localPosition;
	}

	void FixedUpdate()
	{
		Vector3 deltaVector = targetPosition - transform.localPosition;
		Vector3 force = Vector3.Scale(deltaVector, forceAmount) - Vector3.Scale(Rigidbody.velocity, dragAmount);
		Rigidbody.AddForce(force, ForceMode.Force);
	}
}
