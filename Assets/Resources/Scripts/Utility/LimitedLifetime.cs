﻿using UnityEngine;
using System.Collections;

public class LimitedLifetime : MonoBehaviour
{
	public float lifetime = 3f;
	public bool destroy = false;
	[HideInInspector]
	public float timer = 0f;

	void OnEnable()
	{
		timer = lifetime;
	}
	
	void Update()
	{
		if (timer <= 0f)
		{
			timer = 0f;

			if (destroy)
				Destroy(this.gameObject);
			else
				gameObject.SetActive(false);
		}

		timer -= Time.deltaTime;
	}
}
