﻿using UnityEngine;
using System.Collections;

public class Ahead : MonoBehaviour
{
	[SerializeField] bool m_fixed = true;
	[SerializeField] Transform m_target = null;
	[SerializeField] float m_distance = 1;

	Vector3 m_oldPosition;

	void Update()
	{
		if (!m_fixed)
		{
			Vector3 velocity = m_target.position - m_oldPosition;
			Vector3 aheadPosition = m_target.position + velocity * m_distance;

			transform.position = aheadPosition;
			m_oldPosition = m_target.position;
		}
	}

	void FixedUpdate()
	{
		if (m_fixed)
		{
			Vector3 velocity = m_target.position - m_oldPosition;
			Vector3 aheadPosition = m_target.position + velocity * m_distance;

			transform.position = aheadPosition;
			m_oldPosition = m_target.position;
		}
	}
}