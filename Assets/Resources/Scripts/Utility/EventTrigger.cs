﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[RequireComponent(typeof (Collider))]
public class EventTrigger : MonoBehaviour {

    public bool isTriggered {set; get; }

    public bool m_cantBeUntriggered;
    
    [System.Serializable]
    public class Timer { public bool enableTimer; public float time;  public UnityEvent onTimeIsUp; }

    [System.Serializable]
    public class TriggerOnEnter {public UnityEvent m_onEnter; }

    [System.Serializable]
    public class TriggerOnStay {public UnityEvent m_onStay; }

    [System.Serializable]
    public class TriggerOnExit { public UnityEvent m_onExit; }

    [System.Serializable]
    public class CollisionOnEnter { public UnityEvent m_onEnter; }

    [System.Serializable]
    public class CollisionOnStay { public UnityEvent m_onStay; }

    [System.Serializable]
    public class CollisionOnExit { public UnityEvent m_onExit; }

    public Timer m_triggerTimer;
    [Space]
    public LayerMask m_checkLayers;

    [Header("Events:")]   
    public TriggerOnEnter m_triggerOnEnter;
    public TriggerOnStay m_triggerOnStay;
    public TriggerOnExit m_triggerOnExit;

    public CollisionOnEnter m_collisionOnEnter;
    public CollisionOnStay m_collisionOnStay;
    public CollisionOnExit m_collisionOnExit;

    private float timer;
    private bool isDisabled;
    public float trigTimer { get; private set; }

    void OnTriggerEnter(Collider col)
    {
        if (m_checkLayers == (m_checkLayers | (1 << col.gameObject.layer)) && !isDisabled)
        {
            if (m_triggerOnEnter.m_onEnter != null)
                m_triggerOnEnter.m_onEnter.Invoke();
            isTriggered = true;
            timer = 0; 
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (m_checkLayers == (m_checkLayers | (1 << col.gameObject.layer)) && !isDisabled)
        {
            if (m_triggerOnStay.m_onStay != null)
                m_triggerOnStay.m_onStay.Invoke();
            isTriggered = true;
            timer = 0;
            trigTimer = 0;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (m_checkLayers == (m_checkLayers | (1 << col.gameObject.layer)) && !m_cantBeUntriggered && !isDisabled)
        {
                if (m_triggerOnExit.m_onExit != null)
                    m_triggerOnExit.m_onExit.Invoke();
            isTriggered = false;
        }
     }

    void OnCollisionEnter(Collision col)
    {
        if (m_checkLayers == (m_checkLayers | (1 << col.gameObject.layer)) && !isDisabled)
        {
            if (m_collisionOnEnter.m_onEnter != null)
                m_collisionOnEnter.m_onEnter.Invoke();
        }
    }

    void OnCollisionStay(Collision col)
    {
        if (m_checkLayers == (m_checkLayers | (1 << col.gameObject.layer)) && !isDisabled)
        {
            if (m_collisionOnStay.m_onStay != null)
                m_collisionOnStay.m_onStay.Invoke();
            
        }
    }

    void OnCollisionExit(Collision col)
    {
        if (m_checkLayers == (m_checkLayers | (1 << col.gameObject.layer)) && !isDisabled)
        {
            if (m_collisionOnExit.m_onExit != null)
                m_collisionOnExit.m_onExit.Invoke();
        }
    }

    public void DisableTrigger(bool disable)
    {
        isDisabled = disable;
    }

    void Update()
    {
        //Check if timer is enabled
        if (m_triggerTimer.enableTimer)
        {
            m_cantBeUntriggered = true;
            if (isTriggered)
                trigTimer += Time.deltaTime;
            if (trigTimer > m_triggerTimer.time)
            {
                isTriggered = false;
                trigTimer = 0;
                m_triggerTimer.onTimeIsUp.Invoke();
            }
        }
    }
}
