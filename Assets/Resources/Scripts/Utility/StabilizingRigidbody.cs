﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class StabilizingRigidbody : MonoBehaviour
{
	// variables
	public Vector3 torqueAmount = Vector3.one * 10f;
	public Vector3 dragAmount = Vector3.one * 50f;

	// accessors
	public Rigidbody Rigidbody { get; private set; }
	public Vector3 targetDirection { get; set; }

	// methods
	void Awake()
	{
		Rigidbody = GetComponent<Rigidbody>();
		targetDirection = transform.up;
	}

	void FixedUpdate()
	{
		Vector3 deltaR = Quaternion.FromToRotation(transform.up, targetDirection).eulerAngles;
		deltaR.x = deltaR.x > 180f ? deltaR.x - 360f : deltaR.x;
		deltaR.y = deltaR.y > 180f ? deltaR.y - 360f : deltaR.y;
		deltaR.z = deltaR.z > 180f ? deltaR.z - 360f : deltaR.z;
		Vector3 torque = Vector3.Scale(deltaR, torqueAmount) - Vector3.Scale(Rigidbody.angularVelocity, dragAmount);
		Rigidbody.AddTorque(torque * 0.01f, ForceMode.Force);
	}
}
