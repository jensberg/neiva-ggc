﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeInTextColor : MonoBehaviour {
    public float TransitionSpeed = 1f;
    private float alpha = 0f;
    private float maxAlpha = 1f;
    //
    private Text text;
    private Image image;
    //

    private Color startColor;

    public enum ColorType
    {
        Text, 
        Image,
        //Sprite,
    } public ColorType colorType;
	// Use this for initialization
	void Start () {
        if(colorType == ColorType.Text)
        {
            text = GetComponent<Text>();
            if (text == null)
                return;
            startColor = text.color;
            text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
        }
        if(colorType == ColorType.Image)
        {
            image = GetComponent<Image>();
            if (image == null)
                return;
            startColor = image.color;
            image.color = new Color(image.color.r, image.color.g, image.color.b, alpha); 
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        alpha = Mathf.MoveTowards(alpha, maxAlpha, Time.deltaTime * TransitionSpeed);
        if (colorType == ColorType.Text)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
        }
        if(colorType == ColorType.Image)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
        }

        if (alpha == startColor.a)
            this.enabled = false;
	}
}
