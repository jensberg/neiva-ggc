﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody), typeof(GroundChecker))]
public class MoveController : MonoBehaviour
{
	// variables
	public float maxSpeed = 5f;
	public float minSpeed = 0.5f;
	public float acceleration = 10f;
	[Range(0f, 1f)]
	public float airbourneSpeed = 0.5f;
	[Range(0f, 1f)]
	public float airbourneAcceleration = 0.5f;

	private float onGroundEnterTimer = 0f;
	private float onGroundEnterXVel = 0f;

	// accessors
	public Vector3 moveInput { get; private set; }

	public Rigidbody Rigidbody { get; private set; }
	public GroundChecker GroundChecker { get; private set; }

	//methods
	void Awake()
	{
		Rigidbody = GetComponent<Rigidbody>();
		GroundChecker = GetComponent<GroundChecker>();
		GroundChecker.onGroundEnter += OnGroundEnter;
	}

	void OnDestroy()
	{
		GroundChecker.onGroundEnter -= OnGroundEnter;
	}

	void FixedUpdate()
	{
		if (GroundChecker.isGrounded)
		{
			Vector3 moveDirection =
				Quaternion.FromToRotation(Vector3.up, GroundChecker.normal) * moveInput.normalized;
			Rigidbody.velocity =
				Vector3.Lerp(Rigidbody.velocity, moveDirection * minSpeed + moveDirection * moveInput.magnitude * (maxSpeed - minSpeed) + (GroundChecker.ground.attachedRigidbody != null ? GroundChecker.ground.attachedRigidbody.velocity : Vector3.zero), acceleration * Time.fixedDeltaTime);

			if (onGroundEnterTimer < 0.1f)
			{
				onGroundEnterTimer += Time.fixedDeltaTime;
				Rigidbody.velocity = new Vector3(onGroundEnterXVel, Rigidbody.velocity.y, Rigidbody.velocity.z);
			}
		}
		else
		{
			Vector3 vel = Vector3.Lerp(
				Rigidbody.velocity,
				moveInput * maxSpeed * airbourneSpeed,
				acceleration * airbourneAcceleration * Time.fixedDeltaTime
				);
			Rigidbody.velocity = new Vector3(vel.x, Rigidbody.velocity.y, vel.z);
		}
	}

	public void Move(Vector3 direction)
	{
		direction.x = Mathf.Clamp(direction.x, -1f, 1f);
		direction.y = 0f;
		direction.z = Mathf.Clamp(direction.z, -1f, 1f);
		moveInput =
			direction.magnitude > 1f ? direction.normalized : direction;
	}

	void OnGroundEnter()
	{
		onGroundEnterTimer = 0f;
		onGroundEnterXVel = Rigidbody.velocity.x;
	}

#if UNITY_EDITOR
	void OnValidate()
	{
		maxSpeed = Mathf.Clamp(maxSpeed, 0f, float.MaxValue);
		minSpeed = Mathf.Clamp(minSpeed, 0f, maxSpeed);
		acceleration = Mathf.Clamp(acceleration, 0f, float.MaxValue);
	}
#endif
}