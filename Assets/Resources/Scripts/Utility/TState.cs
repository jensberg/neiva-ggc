﻿// TState.cs

public abstract class TState<T>
{
	public T obj;
	public virtual void init() { }
	public virtual TState<T> run() { return this; }
	public virtual void exit() { }
}

public class TStateMachine<T>
{
	// types
	public delegate void StateChange();

	// variables
	public TState<T> currentState;
	//public System.Type[] history;
	public float stateTime; // (placeholder)
	public StateChange onChangeState;

	// constructors
	public TStateMachine(T obj, TState<T> initialState)
	{
		currentState = initialState;
		currentState.obj = obj;
		currentState.init();
		stateTime = 0f; // (placeholder)
	}

	// methods
	public void update(float dt) // (placeholder)
	{
		// update state time
		stateTime += dt;

		// update current state
		TState<T> nextState = currentState.run();

		// check if new state
		if (nextState != currentState)
			setState(nextState);
	}

	public void setState(TState<T> newState)
	{
		currentState.exit();
		newState.obj = currentState.obj;
		currentState = newState;
		currentState.init();
		if (onChangeState != null)
			onChangeState();
		stateTime = 0f;
	}
}