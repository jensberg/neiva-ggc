﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

[RequireComponent(typeof(Collider))]
public class Destructible : MonoBehaviour {

    public new GameObject particleSystem;

    void OnCollisionStay(Collision col)
    {
       if(col.transform.tag == "TestForSwap")
        {
            if (particleSystem != null)
                Instantiate(particleSystem, transform.position, Quaternion.Euler(0, 0, 0));
            else
                Debug.Log("The variable particleSystem could not be found");
            gameObject.SetActive(false);
        }
    }
}
