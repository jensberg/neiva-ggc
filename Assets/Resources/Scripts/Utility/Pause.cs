﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
public class Pause : MonoBehaviour {

    public static bool isPaused { get; private set; }
    private float originalTimescale;
	// Use this for initialization
	void Start () {
        isPaused = false;
        originalTimescale = Time.timeScale;
    }

    // Update is called once per frame
    void Update () {
        if(GameManager.Singleton.UIParent.gameObject.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.JoystickButton7) || Input.GetKeyDown(KeyCode.Escape)) //(todo): Get Start button from Controller
            {
                GameManager.Singleton.SetPause("Pause");
                if (!isPaused)
                    originalTimescale = Time.timeScale;
                Unpause();
            }
            if(Input.GetKeyDown(KeyCode.JoystickButton6))
            {
                GameManager.Singleton.SetPause("DevPause");
                if (!isPaused)
                    originalTimescale = Time.timeScale;
                Unpause();
            }
            if (isPaused)
            {
                Time.timeScale = 0f;
            }
            else
            {
                Time.timeScale = originalTimescale;
            }
        }
    }

    public static bool IsPaused()
    {
        return isPaused;
    }

    public static void Unpause()
    {
        isPaused = !isPaused;
    }
}
