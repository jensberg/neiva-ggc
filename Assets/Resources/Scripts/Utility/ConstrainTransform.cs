﻿using UnityEngine;
using System.Collections;

public class ConstrainTransform : MonoBehaviour {

    [System.Serializable]
    public struct Positions { public bool x; public bool y; public bool z; }
    [System.Serializable]
    public struct Rotations { public bool x; public bool y; public bool z; }

    [Header("Constraints")]
    public Positions position;
    public Rotations rotation;

    Vector3 originalPos;
    Vector3 originalRot;
        

    void Awake ()
    {
        originalPos = transform.position;
        originalRot = transform.rotation.eulerAngles;
        originalPos.z = -6f;
    }

	void Update ()
    {
        transform.position = new Vector3
            (
            position.x ? originalPos.x : transform.position.x,
            position.y ? originalPos.y : transform.position.y,
            position.z ? originalPos.z : transform.position.z
            );

        transform.rotation = Quaternion.Euler
            (
             rotation.x ? originalRot.x : transform.rotation.x,
             rotation.y ? originalRot.y : transform.rotation.y,
             rotation.z ? originalRot.z : transform.rotation.z
            );
	}
}
