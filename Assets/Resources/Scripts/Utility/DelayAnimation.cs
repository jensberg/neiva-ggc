﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class DelayAnimation : MonoBehaviour {
    private Animator m_animator;
    private float delayAnim;
    private float timer;
    public bool addRandomSpeed = true;
    private float randomSpeed;
	// Use this for initialization
	void Start () {
        m_animator = GetComponent<Animator>();
        delayAnim = Random.Range(0f, 1f);
        if(addRandomSpeed)
        randomSpeed = Random.Range(0.8f, 1.2f);
    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        if (timer > delayAnim)
        {
            m_animator.SetTrigger("Start");
            if(addRandomSpeed)
            m_animator.SetFloat("Speed", randomSpeed);
            this.enabled = false;
        }
    }
}
