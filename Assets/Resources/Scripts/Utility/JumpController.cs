﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof(Rigidbody), typeof(GroundChecker))]
public class JumpController : MonoBehaviour
{
	// types
	public enum State { Jumping, Falling, Grounded }

	// variables
	public float height = 3.3f;
	public float duration = 0.5f;
	public AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
	public float minJumpDuration = 0.2f;
	[Range(0f, 1f)]
	public float preserveVelocity = 0.5f;
	[Range(0f, 90f)]
	public float ceilingAngle = 45f;
	public float timeTolerance = 0.2f;

	private float heightJumpedFrom = 0f;
	private float disturbance = 0f;
	private float toleranceTimer = float.MaxValue;
	private bool jumpRequested = false;

	public State currentState = State.Falling;
	State previousState = State.Falling;

	// events
	public UnityAction onJump = null;
	public UnityAction<State> onChangeState = null;

	// accessors
	public float stateTime { get; private set; }
	public bool isJumping { get { return currentState == State.Jumping; } }
	public Rigidbody Rigidbody { get; private set; }
	public GroundChecker GroundChecker { get; private set; }

	// methods
	void Awake()
	{
		this.Rigidbody = GetComponent<Rigidbody>();
		this.GroundChecker = GetComponent<GroundChecker>();
		onChangeState += OnChangeState;
	}

	void OnDestroy()
	{
		onChangeState -= OnChangeState;
	}

	void FixedUpdate()
	{
		// update state time
		stateTime += Time.fixedDeltaTime;

		// update current state
		State newState =
			currentState == State.Jumping ? UpdateJumping() :
			currentState == State.Falling ? UpdateFalling() :
			currentState == State.Grounded ? UpdateGrounded() :
			currentState;

		// check state change
		if (newState != currentState)
		{
			previousState = currentState;
			currentState = newState;
			onChangeState.Invoke(newState);
			stateTime = 0f;
		}
	}

	State UpdateJumping()
	{
		float curveX = stateTime / duration;
		float curveY = curve.Evaluate(curveX);
		float yPos = heightJumpedFrom + curveY * height;

		Rigidbody.MovePosition(new Vector3(Rigidbody.position.x, yPos - disturbance, Rigidbody.position.z));
		Rigidbody.velocity = new Vector3(Rigidbody.velocity.x, 0f, Rigidbody.velocity.z);

		if (Mathf.Abs(Rigidbody.position.y - (yPos - disturbance)) > 0.05f)
			disturbance = yPos - Rigidbody.position.y;

		return
			stateTime < minJumpDuration ? State.Jumping :
			stateTime >= duration ? State.Falling :
			jumpRequested ? State.Jumping :
			State.Falling;
	}

	State UpdateFalling()
	{
		toleranceTimer += Time.fixedDeltaTime;
		return
			GroundChecker.isGrounded ? State.Grounded :
			previousState == State.Grounded && jumpRequested && stateTime < timeTolerance ? State.Jumping :
			State.Falling;
	}

	State UpdateGrounded()
	{
		return
			jumpRequested ? State.Jumping :
			toleranceTimer < timeTolerance ? State.Jumping :
			GroundChecker.isGrounded ? State.Grounded :
			State.Falling;
	}

	void OnChangeState(State newState)
	{
		if (newState == State.Jumping)
		{
			disturbance = 0f;
			heightJumpedFrom = Rigidbody.position.y;
			toleranceTimer = timeTolerance;
					
			if (onJump != null)
				onJump.Invoke();
		}
		else if (newState == State.Falling && previousState == State.Jumping)
		{
			toleranceTimer = timeTolerance;
			float cx = stateTime / duration;
			float cy = curve.Evaluate(cx) - curve.Evaluate(cx - Time.fixedDeltaTime);
			Rigidbody.velocity = new Vector3(Rigidbody.velocity.x, (((cy * height) / duration) / Time.fixedDeltaTime) * preserveVelocity, Rigidbody.velocity.z);
		}
	}

	public void JumpOneShot()
	{
		jumpRequested = true;
		toleranceTimer = 0f;
	}

	public void StopJump()
	{
		jumpRequested = false;
		if (currentState != State.Falling)
		{
			previousState = currentState;
			currentState = State.Falling;
			onChangeState.Invoke(State.Falling);
			stateTime = 0f;
		}
	}

	void OnCollisionEnter(Collision coll)
	{
		if (isJumping)
			foreach (ContactPoint c in coll.contacts)
				if (c.normal.y < -(90f - ceilingAngle) / 90f)
					StopJump();
	}

#if UNITY_EDITOR
	void OnValidate()
	{
		height = Mathf.Clamp(height, 0f, float.MaxValue);
		duration = Mathf.Clamp(duration, 0f, float.MaxValue);
		minJumpDuration = Mathf.Clamp(minJumpDuration, 0f, duration);
	}

	void OnDrawGizmosSelected()
	{
		float yMaxAcc = 0f;
		for (int i = 0; i <= 100; i++)
		{
			float y = curve.Evaluate((float)i / 100f);
			if (y > yMaxAcc)
				yMaxAcc = y;
		}
		Gizmos.color = Color.green;
		Gizmos.DrawLine(transform.position, transform.position + Vector3.up * yMaxAcc * height);
	}
#endif
}
