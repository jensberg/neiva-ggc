﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour
{
	public Transform target = null;
	public bool x = true;
	public bool y = true;
	public bool z = false;
    public float Offset = 0.0f;
	public float speed = 5f;

	void LateUpdate()
	{
		Vector3 targetPosition = new Vector3(
			(x ? target.position.x : transform.position.x),
			(y ? target.position.y - Offset: transform.position.y),
			(z ? target.position.z: transform.position.z)
			);
		transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * speed);

        transform.LookAt(target);
    }
}