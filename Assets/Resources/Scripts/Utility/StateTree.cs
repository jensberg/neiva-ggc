﻿
public abstract class State
{
	public delegate void InitMethod();
	public delegate string UpdateMethod(float dt);

	public abstract string Tag();
	public abstract void Init();
	public abstract string Update(float dt);
}

public class StateLeaf : State
{
	// variables
	string m_tag;
	InitMethod m_init;
	UpdateMethod m_update;

	// constructor
	public StateLeaf(string tag, InitMethod initMethod, UpdateMethod updateMethod)
	{
		m_tag = tag;
		m_init = initMethod;
		m_update = updateMethod;
	}

	// methods
	public override string Tag() { return m_tag; }
	public override void Init() { if (m_init != null) m_init(); }
	public override string Update(float dt) { return m_update(dt); }
}

public class StateTree : State
{
	// variables
	string m_tag;
	InitMethod m_initMethod;
	UpdateMethod m_updateMethod;
	State[] m_states;
	State m_currentState;
	State m_previousState;
	float m_stateTime;

	// accessors
	public State[] states { get { return m_states; } }
	public State currentState { get { return m_currentState; } }
	public State previousState { get { return m_previousState; } }
	public float stateTime { get { return m_stateTime; } }
	public delegate void StateChange(State newState);
	public StateChange onChangeState { get; set; }

	// constructors
	public StateTree(string tag, InitMethod initMethod, UpdateMethod updateMethod, State[] states)
	{
		m_tag = tag;
		m_initMethod = initMethod;
		m_updateMethod = updateMethod;
		m_states = states;
		Init();
	}
	public StateTree(InitMethod initMethod, UpdateMethod updateMethod, State[] states) : this("", initMethod, updateMethod, states) { }
	public StateTree(UpdateMethod updateMethod, State[] states) : this(null, updateMethod, states) { }
	public StateTree(State[] states) : this(null, states) { }

	// methods
	public override string Tag() { return m_tag; }
	public override void Init()
	{
		if (m_initMethod != null)
			m_initMethod();
		m_currentState = states[0];
		m_previousState = null;
		m_stateTime = 0f;
		m_currentState.Init();
	}
	public override string Update(float dt)
	{
		// update state time
		m_stateTime += dt;

		if (m_updateMethod == null)
			return UpdateCurrentState(dt);
		else {
			string nextState = m_updateMethod(dt);
			return nextState != m_tag ? nextState : UpdateCurrentState(dt);
		}
	}

	string UpdateCurrentState(float dt)
	{
		// update current state
		string nextState = m_currentState.Update(dt);

		// check if new state
		if (nextState != m_currentState.Tag())
		{
			// find next state in list of states
			foreach (State s in m_states)
				if (s.Tag() == nextState)
				{
					SetState(s);
					return m_tag;
				}
			return nextState;
		}
		else return m_tag;
	}

	void SetState(State newState)
	{
		m_previousState = m_currentState;
		m_currentState = newState;
		m_stateTime = 0f;
		m_currentState.Init();
		if (onChangeState != null)
			onChangeState(newState);
	}

	public State FindState(string tag)
	{
		foreach (State s in m_states)
			if (s.Tag() == tag)
				return s;
		return null;
	}

	public StateTree FindStateTree(string tag)
	{
		foreach (State s in m_states)
			if (s.Tag() == tag)
				return s as StateTree;
		return null;
	}
}