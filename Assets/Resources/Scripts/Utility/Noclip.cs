﻿using UnityEngine;
using System.Collections;

public class Noclip : MonoBehaviour {
    static public bool active = false;
    private Vector2 AimInput;
    private GameObject Player;
    private Rigidbody m_rigidbody;
    public float speed = 40f;

    private Vector3 targetPos;
    private float zLock = 0;

    //Placeholder trailer invisible
    public bool ScenarioView = true;
    private SkinnedMeshRenderer m_meshRenderer;
    private GameObject[] projectiles = new GameObject[3];
	// Use this for initialization
	void Start () {
        Player = GameManager.Singleton.Neiva.gameObject;
        targetPos = Player.transform.position;
        m_rigidbody = Player.GetComponent<Rigidbody>();
        if(ScenarioView)
        {
            m_meshRenderer = Player.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>();
            projectiles[0] = GameManager.Singleton.Neiva.stickySoul.gameObject;
            projectiles[1] = GameManager.Singleton.Neiva.bouncySoul.gameObject;
            projectiles[2] = GameManager.Singleton.Neiva.swappySoul.gameObject;
        }

        

    }
	
	// Update is called once per frame
	void Update () {
        //if (Input.GetKeyDown(KeyCode.JoystickButton6) || Input.GetKeyDown(KeyCode.N))
        //{
        //    m_rigidbody.isKinematic = false;
        //    if (ScenarioView)
        //    {
        //        m_meshRenderer.enabled = true;
        //        for (int i = 0; i < projectiles.Length; i++)
        //        {
        //            projectiles[i].SetActive(true);
        //        }
        //    }

        //    active = !active;
        //    Player.GetComponent<Health>().SetNoclip(active);

        //}
        //if (Input.GetKeyUp(KeyCode.JoystickButton6) || Input.GetKeyUp(KeyCode.N))
        //{
        //    if(active)
        //    {
        //        m_rigidbody.isKinematic = true;
        //        if (ScenarioView)
        //        {
        //            m_meshRenderer.enabled = false;
        //            for (int i = 0; i < projectiles.Length; i++)
        //            {
        //                projectiles[i].SetActive(false);
        //            }
        //        }
        //    }

        //}

        if (active)
        {
            m_rigidbody.isKinematic = true;
            AimInput = new Vector2(-Input.GetAxis("LS X"), Input.GetAxis("LS Y"));
            if (AimInput.normalized.x < -0.1f)
                AimInput.x = -AimInput.x;
            Player.transform.Translate(AimInput.normalized * speed * Time.deltaTime);
            Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, zLock);
            targetPos = Player.transform.position;
            if(Input.GetKey(KeyCode.W))
            {
                targetPos.y += speed /100;
            }
            if (Input.GetKey(KeyCode.D))
            {
                targetPos.x += speed /100;
            }
            if (Input.GetKey(KeyCode.A))
            {
                targetPos.x -= speed /100;
            }
            if (Input.GetKey(KeyCode.S))
            {
                targetPos.y -= speed /100;
            }
            if(targetPos != Player.transform.position)
            {
                Player.transform.position = targetPos;
            }

        }
        else
        {
            m_rigidbody.isKinematic = false;
        }
        
    }

    public void Toggle()
    {
        active = !active;
    }
}
