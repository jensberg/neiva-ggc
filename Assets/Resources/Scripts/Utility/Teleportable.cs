﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Teleportable : MonoBehaviour
{
	// variables
	public float hangtime = 0.2f;
	public UnityAction<Vector3, Vector3> onTeleport = null;

	float timer = 0f;
	Collider maybeCollider = null;

	// accessors
	public Rigidbody Rigidbody { get; private set; }
	public bool isTeleporting { get; private set; }

	// methods
	void Awake()
	{
		Rigidbody = GetComponent<Rigidbody>();
		maybeCollider = GetComponent<Collider>();
	}

	void FixedUpdate()
	{
		if (isTeleporting) {
			timer += Time.fixedDeltaTime;
			Rigidbody.velocity = Vector3.zero;
			if (timer >= hangtime)
				isTeleporting = false;
		}
	}

	public void TeleportTo(Vector3 toPosition)
	{
		if (maybeCollider != null)
		{
			if (onTeleport != null)
				onTeleport(maybeCollider.bounds.center, toPosition);
			transform.position = toPosition + maybeCollider.bounds.center - Rigidbody.position;
		}
		else
		{
			if (onTeleport != null)
				onTeleport(transform.position, toPosition);
			transform.position = toPosition;
		}
		isTeleporting = true;
		timer = 0f;
	}

	void OnCollisionExit(Collision coll)
	{
		if (isTeleporting)
		{
			Rigidbody.velocity = Vector3.zero;
			if (coll.collider.attachedRigidbody != null)
				coll.collider.attachedRigidbody.velocity = Vector3.zero;
		}
	}

#if UNITY_EDITOR
	void OnValidate()
	{
		hangtime = Mathf.Clamp(hangtime, 0f, float.MaxValue);
	}
#endif
}
