﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody), typeof(GroundChecker))]
public class MoveWithGround : MonoBehaviour
{
	// variables
	//public bool preserveVelocityOnExit = true;

	private Vector3 groundLastPosition;
	//private int timestepCount;

	// accessors
	public Vector3 groundMovement { get; private set; }

	public Rigidbody Rigidbody { get; private set; }
	public GroundChecker GroundChecker { get; private set; }

	// methods
	void Awake()
	{
		Rigidbody = GetComponent<Rigidbody>();
		GroundChecker = GetComponent<GroundChecker>();
	}

	void OnEnable()
	{
		GroundChecker.onGroundEnter += OnGroundEnter;
		GroundChecker.onGroundExit += OnGroundExit;
		GroundChecker.onNewGround += OnNewGround;
		//timestepCount = 0;
	}

	void OnDisable()
	{
		GroundChecker.onGroundEnter -= OnGroundEnter;
		GroundChecker.onGroundExit -= OnGroundExit;
		GroundChecker.onNewGround -= OnNewGround;
	}

	void OnGroundEnter()
	{
		groundLastPosition = GroundChecker.ground.transform.position;
	}

	void OnNewGround()
	{
		groundLastPosition = GroundChecker.ground.transform.position;
	}

	void OnGroundExit()
	{
		//if (preserveVelocityOnExit)
		//	Rigidbody.velocity += (groundMovement / timestepCount) / Time.fixedDeltaTime;
	}

	void Update()
	{
		//Debug.Log(timestepCount);
		//timestepCount = 1;
	}

	void FixedUpdate()
	{
		if (GroundChecker.isGrounded)
		{
			Vector3 groundPos = GroundChecker.ground.transform.position;

			if (groundPos != groundLastPosition)
			{
				groundMovement = groundPos - groundLastPosition;
                Rigidbody.MovePosition(Rigidbody.position + groundMovement);
			}

			groundLastPosition = groundPos;
			//timestepCount += 1;
		}
	}
}
