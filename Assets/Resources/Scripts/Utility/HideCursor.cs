﻿using UnityEngine;

public class HideCursor : MonoBehaviour
{
	void Awake()
	{
		Cursor.visible = false;
	}
}
