﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class GroundChecker : MonoBehaviour
{
	// variables
	public LayerMask whatsGround = 1;
	[Space]
	public float slopeLimit = 45f;
	[Space]
	public Vector3 size = Vector3.one / 2f;
	public Vector3 offset = Vector3.zero;

	// accessors
	public bool isGrounded { get; private set; }
	public float slope { get; private set; }
	public Collider ground { get; private set; }
	public Vector3 normal { get; private set; }

	// delegates
	public UnityAction onGroundEnter = null;
	public UnityAction onGroundStay = null;
	public UnityAction onGroundExit = null;
	public UnityAction onNewGround = null;

	// methods
	void FixedUpdate()
	{
		RaycastHit[] hits = Physics.BoxCastAll(
			transform.position + offset + Vector3.up * size.y / 2f,
			new Vector3(size.x / 2f, 0f, size.z / 2f),
			-transform.up,
			transform.rotation,
			size.y,
			whatsGround
			);
		foreach (RaycastHit hit in hits)
		{
			if (hit.transform != this.transform)
				if (!hit.collider.isTrigger)
				{
					float _slope = Vector3.Angle(-Physics.gravity.normalized, hit.normal);
					bool _isGround =
						_slope < slopeLimit && _slope > -slopeLimit ? true : false;

					if (_isGround)
					{
						slope = _slope;
						if (isGrounded && ground != hit.collider && onNewGround != null)
						{
							ground = hit.collider;
							onNewGround.Invoke();
						}
						ground = hit.collider;
						normal = hit.normal;
						if (isGrounded && onGroundStay != null)
							onGroundStay.Invoke();
						else if (!isGrounded && onGroundEnter != null)
							onGroundEnter.Invoke();
						isGrounded = true;
						return;
					}
				}
		}
		slope = 0f;
		ground = null;
		normal = Vector3.zero;
		if (isGrounded && onGroundExit != null)
			onGroundExit.Invoke();
		isGrounded = false;
	}

#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		Gizmos.color = isGrounded ? Color.green : Color.red;
		Gizmos.DrawWireCube(transform.position + offset, size);
	}

	void OnValidate()
	{
		slopeLimit = Mathf.Clamp(slopeLimit, 0f, 90f);
		size = new Vector3(
			Mathf.Clamp(size.x, 0f, float.MaxValue),
			Mathf.Clamp(size.y, 0f, float.MaxValue),
			Mathf.Clamp(size.z, 0f, float.MaxValue)
			);
	}
#endif
}
