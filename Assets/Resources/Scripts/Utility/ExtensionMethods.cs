﻿using UnityEngine;
using System.Collections;

public static class ExtensionMethods
{
	public static RaycastHit2D Raycast(this CircleCollider2D collider, LayerMask layers)
	{
		return Physics2D.CircleCast(
			collider.bounds.center,
			collider.radius,
			Vector2.zero,
			0f,
			layers
			);
	}

	public static RaycastHit2D Raycast(this CircleCollider2D collider, LayerMask layers, float skinWidth)
	{
		return Physics2D.CircleCast(
			collider.bounds.center,
			collider.radius + skinWidth,
			Vector2.zero,
			0f,
			layers
			);
	}

	public static RaycastHit2D[] RaycastAll(this CircleCollider2D collider, LayerMask layers)
	{
		return Physics2D.CircleCastAll(
			collider.bounds.center,
			collider.radius,
			Vector2.zero,
			0f,
			layers
			);
	}

	public static RaycastHit2D[] RaycastAll(this CircleCollider2D collider, LayerMask layers, float skinWidth)
	{
		return Physics2D.CircleCastAll(
			collider.bounds.center,
			collider.radius + skinWidth,
			Vector2.zero,
			0f,
			layers
			);
	}

	public static float angle(this Vector2 _, Vector2 v1, Vector2 v2)
	{
		float angle = Vector2.Angle(v1, v2);
		Vector3 cross = Vector3.Cross(v1, v2);
		return cross.z < 0f ? -angle : angle;
	}
}
