﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class ClampVelocity : MonoBehaviour
{
	public float magnitude = 10f;

	public new Rigidbody rigidbody { get; private set; }

	void Awake()
	{
		rigidbody = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate()
	{
		rigidbody.velocity = Vector3.ClampMagnitude(rigidbody.velocity, magnitude);
	}

#if UNITY_EDITOR
	void OnValidate()
	{
		magnitude = magnitude < 0f ? 0f : magnitude;
	}
#endif
}
