﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class FadeMixers : MonoBehaviour {

    public AudioMixer masterMixer;
    public float fadeSpeed = 1f;
    
    public bool fadeMusic;
    void Start()
    {
        masterMixer = Resources.Load("Audio/Mixer/MasterMixer") as AudioMixer;
        
        if (masterMixer == null)
            Debug.LogError("Could not find master mixer at Resources/Audio/Mixer/");
    }

	public void StartFadeOut()
    {
        StartCoroutine(FadeOut());
        FindObjectOfType<GameAudio>().stopControl = true;
    }

    IEnumerator FadeOut()
    {
        string channel;
        channel = fadeMusic ? "musicVol" : "ambienceVol";
        float vol;
        
        masterMixer.GetFloat(channel, out vol);
       
        while (vol > -80)
        {
            float nextVol;
            masterMixer.GetFloat(channel, out nextVol);
            masterMixer.SetFloat(channel, Mathf.Lerp(nextVol, -80f, Time.deltaTime * fadeSpeed / 10));
            yield return null;
        }
    }

    public void StartFadeIn()
    {
        StartCoroutine(FadeIn());
    }

    IEnumerator FadeIn()
    {
        float vol;
        masterMixer.GetFloat("musicVol", out vol);

        while (vol < 0)
        {
            masterMixer.SetFloat("musicVol", Mathf.Lerp(vol, 0, Time.deltaTime * fadeSpeed));
            yield return null;
        }
    }
}
