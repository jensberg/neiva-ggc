﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

//Class for handling all triggers in the scene. 

public class MusicTriggerHandler : MonoBehaviour {

    private MusicTriggers[] m_musicTriggers;
    
    void OnEnable()
    {
        MusicTriggers.OnZoneTriggered += ResetTriggers;
        MusicTriggers.OnThemeLoaded += FindTriggersInScene;
    }

    void OnDisable()
    {
        MusicTriggers.OnZoneTriggered -= ResetTriggers;
        MusicTriggers.OnThemeLoaded -= FindTriggersInScene;
    }

    void FindTriggersInScene(ref Elias.Theme theme)
    {
        m_musicTriggers = FindObjectsOfType<MusicTriggers>();
    }

    void ResetTriggers(uint zone_ID, MusicTriggers.TrackType trackType)
    {
        foreach (MusicTriggers trigger in m_musicTriggers)
        {
            if (trigger.zone_id != zone_ID)
            {
                trigger.ResetColor();
            }
                

        }
    }
}
