﻿using UnityEngine;
using System.Collections;

public class MusicTriggers : MonoBehaviour {

    public enum TrackType { Music, Stinger };
    public TrackType trackType;
    
    public uint zone_id = 0;

    public delegate void ZoneTriggered(uint zone_id, TrackType trackType);
    public delegate void ThemeLoaded(ref Elias.Theme theme);

    // e.g MusicSwitcher.Switch(uint zone_id)
    public static event ZoneTriggered OnZoneTriggered;
    public static event ThemeLoaded OnThemeLoaded;

    public enum ELIASTriggerType { TriggerEnter, TriggerExit, CollisionEnter, CollisionExit };
    
    //enum ELIASAction {SwitchLevel, PlayStinger};
    public ELIASTriggerType TriggerOn = ELIASTriggerType.TriggerEnter;

    private bool hasTriggered;
    NeivaController m_Player;

    void Awake () {
        m_Player = FindObjectOfType<NeivaController>();
    }
	
	// Update is called once per frame
	void Update () {
        //if (!hasTriggered)
        //    CheckPlayerPosition();
    }

    public static void ThemeWasLoaded(ref Elias.Theme theme)
    {
        if (OnThemeLoaded != null)
        {
            OnThemeLoaded(ref theme);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        // Send trigger event. Let event manager on ELIAS_Component be a registered event listener.
        if (TriggerOn == ELIASTriggerType.TriggerEnter && collider.tag == "Player")
        {
            // Call all registered methods for this event!
            if (OnZoneTriggered != null)
            {
                OnZoneTriggered(zone_id, trackType);
                hasTriggered = true;
            }
        }
    }

    void CheckPlayerPosition()
    {
        if (transform.position.x - m_Player.transform.position.x < 0)
        {
            if (OnZoneTriggered != null)
            {
                OnZoneTriggered(zone_id, trackType);
                hasTriggered = true;
            }
        }
    }

    public void ResetColor()
    {
        hasTriggered = false;
    }

#if UNITY_EDITOR
    [ExecuteInEditMode]
    void OnDrawGizmos()
    {
        Color color = new Color();

        if (hasTriggered)
            color = Color.red;
        else if (trackType == TrackType.Music && !hasTriggered)
            color = Color.blue;
        else if (trackType == TrackType.Stinger && !hasTriggered)
            color = Color.cyan;

        BoxCollider collider = GetComponent<BoxCollider>(); 
        Gizmos.color = color;
        Gizmos.DrawWireCube(collider.bounds.center, new Vector3 (collider.bounds.size.x, collider.bounds.size.y, 0));
        
        GUIStyle style = new GUIStyle();
        style.fontSize = 12;
        style.alignment = TextAnchor.MiddleCenter;
        style.fontStyle = FontStyle.Bold;
        style.padding = new RectOffset(-100, 1, 0, 0);
        style.normal.textColor = Color.yellow;
        UnityEditor.Handles.Label(collider.bounds.center, trackType.ToString() +" Zone ID: " + zone_id.ToString(), style);
    }
#endif //UNITY_EDITOR
}