﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class ImpactAudio : MonoBehaviour {

    AudioSource m_audiosource;
    public AudioClip[] impactClips;
    public LayerMask checkLayers;

    void Awake () {
        m_audiosource = GetComponent<AudioSource>();
	}
	
	void OnCollisionEnter(Collision col)
    {
        if (checkLayers == (checkLayers | (1 << col.gameObject.layer)) && m_audiosource != null)
        {
            m_audiosource.pitch = Random.Range(0.8f, 1.1f);
            m_audiosource.PlayOneShot(impactClips[Random.Range(0, impactClips.Length)], Random.Range(0.9f, 1f));
        }
    }
}
