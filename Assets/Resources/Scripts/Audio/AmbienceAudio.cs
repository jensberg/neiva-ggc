﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AmbienceAudio : MonoBehaviour
{
    [System.Serializable]
    public struct AmbienceClips
    {
        public string name;
        public AudioClip clip;
    }

    AudioSource audioSource;
    bool hasFaded;

    public AmbienceClips[] ambienceClips;
    [Space]
    public float fadeSpeed = 1f;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        if(audioSource != null)
        {
            audioSource.loop = true;
            audioSource.playOnAwake = true;
        }
    }

    public void ChangeTrack(string name)
    {
        foreach(AmbienceClips c in ambienceClips)
        {
            if (c.name == name && audioSource.clip != c.clip)
                StartCoroutine(FadeTrack(c.clip));
        }
    }

    IEnumerator FadeTrack(AudioClip clip)
    {
        // fade out
        while (audioSource.volume > .1f && !hasFaded)
        {
            audioSource.volume = Mathf.Lerp(audioSource.volume, 0f, Time.deltaTime * fadeSpeed);
            yield return null;
        }
        if (clip != null)
        {
            audioSource.clip = clip;
            audioSource.Play();
        }
        else
            audioSource.Stop();  
        hasFaded = true;

        // fade in
        while (audioSource.volume < 1f)
        {
            audioSource.volume = Mathf.Lerp(audioSource.volume, 1.1f, Time.deltaTime * fadeSpeed);
            yield return null;
        }
        audioSource.volume = 1f;
        hasFaded = false;
    }
    
}
