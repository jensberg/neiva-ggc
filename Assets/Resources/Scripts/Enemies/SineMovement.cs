﻿using UnityEngine;
using System.Collections;

public class SineMovement : MonoBehaviour {
    private Vector2 destPos;
    private Vector3 m_startPosition;
    public Vector2 idleMoveRange = new Vector2(2f, 3f);
    private float moveTime;
    public Vector2 m_idleMovementSpeed = new Vector2(0.5f, 0.5f);
    private Vector2 dir;

    //debug 
    Vector2 rand;
    // Use this for initialization
    void Start () {
        m_startPosition = transform.position;
        rand = new Vector2(Random.Range(0f, 0.2f*m_idleMovementSpeed.x), Random.Range(0f, 0.2f*m_idleMovementSpeed.y));
        m_idleMovementSpeed = m_idleMovementSpeed + rand;
    }

    // Update is called once per frame
    void Update () {
        
        moveTime += Time.deltaTime;
        destPos.y = m_startPosition.y + idleMoveRange.y * Mathf.Sin(moveTime * m_idleMovementSpeed.y * Mathf.PI);
        destPos.x = m_startPosition.x + idleMoveRange.x * Mathf.Cos(moveTime * m_idleMovementSpeed.x);
        dir = destPos - new Vector2(transform.position.x, transform.position.y);
        transform.Translate(dir);
    }
}
