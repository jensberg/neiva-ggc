﻿using UnityEngine;
using System.Collections;

public class FlyingAbsorber : MonoBehaviour {
    //Private variables
    private bool m_isProjectileActive;
    private AudioSource m_audioSource;
    private GameObject m_target;
    private GameObject m_player;
    private Rigidbody m_rigidbody;
    private Soul m_projectile;
    private SphereCollider m_collider;
    private LayerMask m_projectileLayer;
    private LayerMask m_playerLayer;
    private LayerMask m_groundLayer;
    private float curveTimer = 0.25f;

    //Serialized variables
    [SerializeField] private float m_targetRange = 5f;
    [SerializeField] private float m_activeMovementSpeed = 5f;
    [SerializeField] private AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

    public bool CanBeDestroyed = true;
    public float Armor = 10f;
    public AudioClip aggroSound;
    private enum State
    {
        Active,
        Destroyed,
    } private State m_state = State.Active;

    //In progress: Animation
    private Animator m_animator;
    private bool openMouth;

    //Return to start position
    Vector3 startPos;
    bool hasProjectileBeenActive = false;

    // Use this for initialization
    void Start () {
        m_projectileLayer = (1 << LayerMask.NameToLayer("Projectile"));
        m_playerLayer = (1 << LayerMask.NameToLayer("Player"));
        m_groundLayer = (1 << LayerMask.NameToLayer("Ground"));
        m_collider = GetComponent<SphereCollider>();
        m_rigidbody = GetComponent<Rigidbody>();
        m_animator = GetComponent<Animator>();
        m_player = GameObject.FindGameObjectWithTag("Player");
        startPos = transform.position;
        m_audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
        switch(m_state)
        {
            case State.Active:
                RunActive();
                break;

            case State.Destroyed:
                RunDestroyed();
                break;

            default:
                break;
        }

    }

    void RunActive()
    {
        FindTarget();
        if (m_isProjectileActive)
            GoToTarget();
        else
        {
            FlipTowardsPlayer();
        }
        if (CheckProjectileCollision())
        {
            m_animator.SetTrigger("isSoulEaten");
            if (GameManager.Singleton.Neiva.selectedSoulType != NeivaController.SoulType.Swappy)
			{
                m_projectile.stateMachine.setState(new Soul.CooldownState());
                m_audioSource.PlayOneShot(aggroSound, 0.8f);
            }
			//Set launch cooldown
		}

        if (CheckPlayerCollision())
        {
            GameManager.Singleton.Neiva.health.TakeDamage(Health.DamageType.FlyingEnemy);
        }
        //Clamp velocity
        m_rigidbody.velocity = Vector3.ClampMagnitude(m_rigidbody.velocity, 10f);

        //Animator set
        m_animator.SetBool("isProjectileInRange", m_isProjectileActive);

        if (hasProjectileBeenActive == true && m_isProjectileActive == false)
        {
            MoveToStart();
            curveTimer = 0.25f;
        }
    }

    void RunDestroyed()
    {
        transform.parent.GetComponent<SineMovement>().enabled = false;
    }

    void FindTarget()
    {
        m_target = GameManager.Singleton.Neiva.selectedSoul.gameObject;
        if (m_target)
        {
            m_projectile = m_target.GetComponent<Soul>();

            if (Vector3.Distance(transform.position, m_target.transform.position) <= m_targetRange)
            {
                if (!CheckLineofSight())
                {
                    if (m_projectile.currentState == Soul.State.Throw)
                        m_isProjectileActive = true;
                    hasProjectileBeenActive = true;
                }
                else
                    m_isProjectileActive = false;
            }
            else
                m_isProjectileActive = false;
        }
        else
        {
            m_isProjectileActive = false;

        }
    }

    bool CheckLineofSight()
    {
        return Physics.Linecast(transform.position, m_target.transform.position,m_groundLayer);
    }

    void GoToTarget()
    {
        FlipTowardsProjectile();

        if (m_projectile.currentState == Soul.State.Throw)
        {
            curveTimer += Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, m_target.transform.position, m_activeMovementSpeed * Time.deltaTime * curve.Evaluate(curveTimer));
        }
        else
        {
            m_isProjectileActive = false;
        }

    }

    bool CheckProjectileCollision()
    {
        return Physics.CheckSphere(transform.position, m_collider.radius *1.3f, m_projectileLayer, QueryTriggerInteraction.Ignore);
    }

    bool CheckPlayerCollision()
    {
        //return Physics.Raycast(transform.position, Vector3.up, (m_collider.radius *= 1.5f), m_playerLayer, QueryTriggerInteraction.Ignore);

        return Physics.BoxCast(transform.position, new Vector3((m_collider.radius / 2), (m_collider.radius / 2), 100f), Vector3.up, Quaternion.identity, (m_collider.radius * 1.5f), m_playerLayer, QueryTriggerInteraction.Ignore);
    }

    void MoveToStart()
    {
        transform.position = Vector3.MoveTowards(transform.position, startPos, Time.deltaTime);
        if(Vector3.Distance(transform.position, startPos) < 4f)
        {
            hasProjectileBeenActive = false;

        }

    }

    private void FlipTowardsProjectile()
    {
        if(transform.position.x > m_target.transform.position.x)
        transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);
        else if (transform.position.x < m_target.transform.position.x)
        transform.rotation = Quaternion.Euler(transform.rotation.x, -90, transform.rotation.z);
    }
    private void FlipTowardsPlayer()
    {
        if (transform.position.x > m_player.transform.position.x)
            transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);
        else if (transform.position.x < m_player.transform.position.x)
            transform.rotation = Quaternion.Euler(transform.rotation.x, -90, transform.rotation.z);
    }

    void OnCollisionEnter(Collision coll)
    {
        if (CanBeDestroyed)
            if (coll.gameObject.tag == "TestForSwap")
            {
                if (coll.relativeVelocity.magnitude > Armor)
                {
                    m_state = State.Destroyed;
                }
            }
    }
#if UNITY_EDITOR
    //Draw TargetRange
    void OnDrawGizmos()
    {
        UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, m_targetRange);

    }

#endif
}
