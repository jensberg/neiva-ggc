﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class LoopLineRenderer : MonoBehaviour {
    
    // public variables
    public Transform startPoint;
    [Range(0, 30)]
    public float stretchModifier = 1;
    public float offsetSpeed;

    // private variables
    private LineRenderer m_lineRenderer;
    private float m_texWidth;
    private bool hidden;
    public Vector3 targetPoint { get; set; }
    public Transform targetTransform;

    void Awake ()
    {
        if (targetTransform != null)
            targetPoint = targetTransform.position;
        m_lineRenderer = GetComponent<LineRenderer>();
        m_lineRenderer.enabled = true;
        m_texWidth = m_lineRenderer.sharedMaterial.mainTexture.width; //Get the width of the attached texture
        hidden = true;
	}

    void Update()
    {
        if (!hidden)
        {
            float distance = Vector3.Distance(startPoint.position, targetPoint);
            float tilingX = distance / m_texWidth;

            //Increase tiling with distance
            m_lineRenderer.material.mainTextureScale = new Vector2(tilingX * (m_texWidth / stretchModifier), 1);
            //animate texture movement
            m_lineRenderer.material.mainTextureOffset = new Vector2(
                m_lineRenderer.material.mainTextureOffset.x + offsetSpeed * Time.deltaTime, //move along the x-axis over time * speed
                m_lineRenderer.material.mainTextureOffset.y);

            //update positions
            m_lineRenderer.SetPosition(1, startPoint.position);
            m_lineRenderer.SetPosition(0, targetPoint);
        }
        else
        {
            m_lineRenderer.SetPosition(1, Vector3.zero);
            m_lineRenderer.SetPosition(0, Vector3.zero);
        }

    }

    public void HideLine(bool hide)
    {
        hidden = hide;
    }
}
