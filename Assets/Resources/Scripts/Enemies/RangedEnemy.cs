﻿using UnityEngine;
using System.Collections;

public class RangedEnemy : MonoBehaviour {
    //public variables
    public Transform rotationTarget;
    [Space]
    public bool canShoot = true;
    public float range = 10.0f;
    public float Cooldown = 1.0f;
    public float rotationSpeed = 5f;
    [Space]
    public bool canBeDestroyed = false;
    public float armor = 10f;
    [Space]
    public bool movement;
    public float movementSpeed = 1f;

    //Private variables
    private LayerMask m_ground;
    private float m_colliderRadius;
    private Transform playerTarget;
    private Transform m_transform;
    private Transform m_projectile;
    private Vector3 LookDirection;
    private Vector2 launchDirection;
    private Animator m_animator;
    private Rigidbody m_rigidbody;
    private bool isFacingPlayer;
    private bool isProjectileActive;
    private bool canMove;
    private float zLock;
    private enum State 
    {
        Active,
        Destroyed,
    } private State m_state;

	// Use this for initialization
	void Start () {
        //Set private variables
        playerTarget = GameObject.FindGameObjectWithTag("Player").transform;
        m_ground = (1 << LayerMask.NameToLayer("Ground"));
        m_transform = transform;
        m_animator = GetComponent<Animator>();
        m_rigidbody = GetComponent<Rigidbody>();
        m_colliderRadius = GetComponent<SphereCollider>().radius; GetComponent<SphereCollider>().enabled = false;
        m_projectile = transform.GetChild(0);
        LookDirection = transform.position;
        launchDirection = Vector2.zero;
        m_state = State.Active;
        zLock = m_transform.position.z;
    }

    // Update is called once per frame
    void Update ()
    {
        switch (m_state)
        {
            case State.Active:
                RunActive();
                break;

            case State.Destroyed:
                RunDestroyed();
                break;

            default:
                break;
        }
	}

    void RunActive()
    {
        m_animator.SetBool("isWalking", movement);
        RunMovement(); 
        RotateTowardsPlayer();
        LaunchProjectile();
    }

    void RunMovement()
    {
        if (!movement)
            return;
        m_animator.SetFloat("Speed", movementSpeed);

        m_rigidbody.MovePosition(transform.position + m_animator.deltaPosition + transform.forward * Time.deltaTime * m_animator.speed);
            Flip(CheckWallCollision());
    }

    string CheckWallCollision()
    {
        if (Physics.Raycast(transform.position, Vector3.left, m_colliderRadius, m_ground, QueryTriggerInteraction.Ignore))
            return "Left";
        if (Physics.Raycast(transform.position, Vector3.right, m_colliderRadius, m_ground, QueryTriggerInteraction.Ignore))
            return "Right";
        else
            return null;
    }

    void Flip(string Dir)
    {
        if (Dir == null)
            return;
        else if (Dir == "Right")
            m_transform.rotation = Quaternion.Euler(m_transform.rotation.eulerAngles.x, 180, m_transform.rotation.eulerAngles.z);
        else if (Dir == "Left")
            m_transform.rotation = Quaternion.Euler(m_transform.rotation.eulerAngles.x, 0, m_transform.rotation.eulerAngles.z);
    }

    void RotateTowardsPlayer()
    {
        //Find direction
        if (playerTarget)
        {
            Vector2 directionV = playerTarget.transform.position - rotationTarget.position;
            launchDirection = directionV;
        }
        //Rotate object
        LookDirection = Vector2.MoveTowards(LookDirection, launchDirection, rotationSpeed * Time.deltaTime);
        rotationTarget.rotation = Quaternion.FromToRotation(Vector3.left, LookDirection);

        if (LookDirection == (Vector3)launchDirection)
        {
            isFacingPlayer = true;
        }
        else
        {
            isFacingPlayer = false;
        }
    }
    
    void LaunchProjectile()
    {
        isProjectileActive = m_projectile.gameObject.activeSelf;

        if (canShoot && IsPlayerInRange() && !isProjectileActive && isFacingPlayer)
        {
            m_projectile.gameObject.SetActive(true);
            m_projectile.GetComponent<Enemy_Ranged_Projectile>().OnProjectileLaunch(rotationTarget.position, launchDirection.normalized);
            StartCoroutine(ProjectileLaunched());
        }
    }
    private IEnumerator ProjectileLaunched()
    {
        canShoot = false;
        yield return new WaitForSeconds(Cooldown);
        canShoot = true;
    }
    private bool IsPlayerInRange()
    {
        if (Vector2.Distance(transform.position, playerTarget.transform.position) > range)
            return false;
        else
            return true;
    }

    void OnCollisionEnter(Collision coll)
    {
        if (canBeDestroyed)
            if (coll.gameObject.tag == "TestForSwap")
            {
                if (coll.relativeVelocity.magnitude > armor)
                {
                    m_state = State.Destroyed;
                }
            }
    }

    void RunDestroyed()
    {

    }

#if UNITY_EDITOR
    //Draw TargetRange
    void OnDrawGizmos()
    {
        UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, range);
    }
#endif
}
