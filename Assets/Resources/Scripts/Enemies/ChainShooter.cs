﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(GroundChecker), typeof(Animator), typeof(MoveController))]
public class ChainShooter : MonoBehaviour
{
    [System.Serializable]
    public struct SoundEffects
    {
        public AudioClip shoot;
        public AudioClip hit;
        public AudioClip retract;
    }

    // public variables
    [Tooltip("Prevent from moving")]
    public bool isStationary;

    public float moveSpeedMultiplier = 1f;
    public float animSpeedMultiplier = 1f; [Space]

    [Range(-40, 0)]
    public float leftPathNodePosition;
    [Range(0, 40)]
    public float rightPathNodePosition;
    
    public enum StartDir { Right, Left }; [Space]
    [Tooltip("If enabled the shooter will start off by walking left.")]
    public StartDir startDirection; [Space]
    [Range(0,30)]
    [Tooltip("How fast it rotates towards the player.")]
    public float rotationalSpeed;
    [Range(0, 2)]
    [Tooltip("Increase value to make it worse at aiming when shooting.")]
    public float inaccuracy;
    [Range(1, 15)]
    [Tooltip("The angle in which allowing to fire.")]
    public float firingFov;
    public float aggroradius;
    [Tooltip("The circleradius from the tip of the chain to register a hit.")]
    public float grabRadius = 0.3f;
    [Tooltip("How many seconds to wait inbetween shots.")]
    public float cooldown;
    [Tooltip("How fast the chain travels.")]
    public float chainSpeed;
    public float retractChainSpeed;
    [Range(0.5f, 3f)]
    [Tooltip("Speed the firing animation up or down.")]
    public float rapidFireMultiplier = 2f;
    [Tooltip("The time in seconds to wait after hitting the player, before dragging her in.")]
    public float pullWaitTime = 1f;
    [Range(1f, 6f)]
    [Tooltip("Increase to drop grappled player earlier")]
    public float releaseRange = 1f;
    [Space]
    public Transform ribCage;
    public LayerMask layers;
    public GameObject particles;

    public SoundEffects soundEffects;
    // private variables
    StateTree stateTree;
    Animator animator;
    Rigidbody rigidBody;
    GroundChecker groundChecker;
    LoopLineRenderer lineRenderer;
    NeivaController player;
    AudioSource audioSource;
    Rigidbody swapable;

    float walkDirection;
    float leftNode;
    float rightNode;
    float walkTimer;
    float idleTimer;
    float cooldownTimer;
    float firingTimer;
    float waitForPullTimer;
    bool playerInRange;
    bool triggerShot;
    bool sideCollisionL;
    bool sideCollisionR;
    bool hitPlayer;
    bool hitSwapable;
    bool releasePlayer;
    bool retractSoundPlayed;
    bool gotSwapRigidbody;
    int onCollisionDirection;
    Vector3 shootDirection;
    Vector3 connectingPoint;

    void Awake()
    {
        // Init StateTree & States
        stateTree = new StateTree(new State[]
        {
            new StateLeaf("Idle", IdleInit, Idle),
            new StateLeaf("Walk", WalkInit, Walk),
            new StateLeaf("PlayerInRange", null, PlayerInRange),
            new StateLeaf("Firing", FiringInit, Firing),
            new StateLeaf("HitTarget", HitTargetInit, HitTarget),

        });

        // Init member variables
        player =        FindObjectOfType<NeivaController>();
        animator =      GetComponent<Animator>();
        rigidBody =     GetComponent<Rigidbody>();
        groundChecker = GetComponent<GroundChecker>();
        lineRenderer =  GetComponent<LoopLineRenderer>();
        audioSource =   GetComponent<AudioSource>();
      
        // Init inspector values
        walkDirection = startDirection == StartDir.Right ? -1 : 1;
        leftNode =      transform.position.x + leftPathNodePosition;
        rightNode =     transform.position.x + rightPathNodePosition;
        
    }

    void Start()
    {
        player.teleportable.onTeleport += onNeivaTeleport;
    }

    void OnDestroy()
    {
        player.teleportable.onTeleport -= onNeivaTeleport;
    }
    void FixedUpdate()
    {
        stateTree.Update(Time.fixedDeltaTime);
    }
	void Update()
	{
		cooldownTimer += Time.deltaTime;
		UpdateAnimations();
	}

	// state updates
	string Idle(float dt)
    {
        idleTimer += dt;
        string _nextState = null;

        if (!IsPlayerNear())
            _nextState = IsPlayerNear() ? "Walk" : "Idle";

        if (CanFire())
            _nextState = "PlayerInRange";
        else if (idleTimer > 3 && !isStationary)
            _nextState = "Walk";
        
        return _nextState;
    }
    string Walk(float dt)
    {
        string _nextState;
        _nextState = IsPlayerNear() ? "Walk" : "Idle";
        if(!isStationary)
            Move(dt);
        if (CanFire())
            return "PlayerInRange";
        else if (walkTimer < 5)
            return "Walk";

        return "Idle";
    }
    string PlayerInRange(float dt)
    {
        // rotating towards the target by rotationalSpeed
        Vector3 _cross = Vector3.Cross(ribCage.right, player.transform.position - ribCage.position);
        float _dir = _cross.z < 0 ? -1 : 1;
        ribCage.Rotate(Vector3.back * _dir * rotationalSpeed * 10f * dt, Space.World);

        // checking if angle towards player is small enough to fire
        //float _angle = Vector3.Angle((Vector2)ribCage.right, player.transform.position - ribCage.position);
        if (/*_angle < firingFov &&*/ CanFire() && !player.isDead)
        {
            return "Firing";
        }

        else if (CanFire())
            return "PlayerInRange";
        else
            return "Idle";
    }
    string Firing(float dt)
    {
        if (triggerShot)
        {
            firingTimer += dt * chainSpeed;
            cooldownTimer = 0;
            connectingPoint = ribCage.position + shootDirection.normalized * firingTimer;
            connectingPoint.z = 0;
            lineRenderer.HideLine(false);
            lineRenderer.targetPoint = connectingPoint;
            particles.transform.position = connectingPoint;

            Collider[] hits;
            hits = Physics.OverlapSphere(connectingPoint, grabRadius, layers);
            
            foreach (Collider hit in hits)
            {
                if (hit.transform.tag == "Player" && player.health.GetHealth() > 0)
                {
                    triggerShot = false;
                    hitPlayer = true;
                    audioSource.PlayOneShot(soundEffects.hit);
                    player.isImmobilized = true;
                    return "HitTarget";
                }
                else if(hit.transform.tag == "TestForSwap")
                {
                    swapable = hit.transform.gameObject.GetComponent<Rigidbody>();
                    triggerShot = false;
                    audioSource.PlayOneShot(soundEffects.hit);
                    return "HitTarget";
                }
                else if(!hit.isTrigger)
                {
                    hitPlayer = false;
                    swapable = null;
                    triggerShot = false;
                    audioSource.clip = soundEffects.retract;
                    audioSource.Play();
                    return "HitTarget";
                }
            }
            if (Vector3.Distance(ribCage.position + shootDirection.normalized * firingTimer, ribCage.position) > aggroradius)
            {
                hitPlayer = false;
                triggerShot = false;
                audioSource.clip = soundEffects.retract;
                audioSource.Play();
                swapable = null;
                return "HitTarget";
            }
        }
        // rotate only when a chain hasn't been triggered
        if (CanFire() && !triggerShot) { 
            Vector3 _cross = Vector3.Cross(ribCage.right, player.transform.position - ribCage.position);
            float _dir = _cross.z < 0 ? -1 : 1;
            //ribCage.right = Quaternion.Euler(0, 0, _dir * rotationalSpeed * 10f * dt) * ribCage.right;
            ribCage.Rotate(Vector3.back * _dir * rotationalSpeed * 10f * dt, Space.World);
        }
        
        return "Firing";
    }
    string HitTarget(float dt)
    {
        waitForPullTimer += dt;
        connectingPoint = ribCage.position + shootDirection.normalized * firingTimer;
        connectingPoint.z = 0;
        if (hitPlayer)
        {
            player.rigidbody.isKinematic = true;
            player.rigidbody.useGravity = false;
            player.rigidbody.position = connectingPoint;
            lineRenderer.targetPoint = connectingPoint;
            particles.transform.position = connectingPoint;

            if (waitForPullTimer > pullWaitTime && firingTimer > releaseRange)
            {
                firingTimer -= dt * retractChainSpeed;
                if (!retractSoundPlayed)
                {
                    audioSource.clip = soundEffects.retract;
                    audioSource.Play();
                    retractSoundPlayed = true;
                }
            }

            else if (firingTimer <= releaseRange)
            {
                lineRenderer.HideLine(true);
                audioSource.Stop();
                retractSoundPlayed = false;
                particles.SetActive(false);
                player.isImmobilized = false;
                return "Idle";
            }
            return "HitTarget";
        }
        else if (swapable != null)
        {
            swapable.useGravity = false;
            swapable.isKinematic = true;
            swapable.position = new Vector3(connectingPoint.x, connectingPoint.y - 0.5f, 0); 
            lineRenderer.targetPoint = connectingPoint;
            particles.transform.position = connectingPoint;

            if (waitForPullTimer > pullWaitTime && firingTimer > releaseRange)
            {
                firingTimer -= dt * retractChainSpeed;
                if (!retractSoundPlayed)
                {
                    audioSource.clip = soundEffects.retract;
                    audioSource.Play();
                    retractSoundPlayed = true;
                }
            }
            else if (firingTimer <= releaseRange)
            {
                lineRenderer.HideLine(true);
                audioSource.Stop();
                retractSoundPlayed = false;
                swapable.useGravity = true;
                swapable.isKinematic = false;
                swapable = null;
                particles.SetActive(false);
                return "Idle";
            }
            return "HitTarget";
        }
        else
        {
            if (firingTimer > 0)
                firingTimer -= dt * retractChainSpeed;
            else
            {
                lineRenderer.HideLine(true);
                audioSource.Stop();
                retractSoundPlayed = false;
                particles.SetActive(false);
                return "Idle";
            }
            lineRenderer.targetPoint = connectingPoint;
            particles.transform.position = connectingPoint;
            return "HitTarget";
        }
    }

    // state inits
    void IdleInit()
    {
        idleTimer = 0;
    } 
    void WalkInit()
    {
        firingTimer = 0;
        walkTimer = 0;
    }
    void FiringInit()
    {
        
    }
    void HitTargetInit()
    {
        waitForPullTimer = 0;
    }

    // private methods
    void UpdateAnimations()
    {
        string _currentState = stateTree.currentState.Tag();

        if (_currentState == "Idle" || _currentState == "PlayerInRange" || _currentState == "HitTarget")
        {
            animator.SetBool("isWalking", false);
            animator.SetBool("Shoot", false);
            animator.speed = 1f;
        }
        else if (_currentState == "Walk")
        {
            animator.SetBool("isWalking", true);
            animator.SetBool("Shoot", false);
            animator.speed = animSpeedMultiplier;
        }
        else if(_currentState == "Firing")
        {
            if (cooldown < cooldownTimer)
            {
                animator.speed = rapidFireMultiplier;
                animator.SetBool("Shoot", true);
            }
            else
                animator.SetBool("Shoot", false);
        }
    }
    bool IsPlayerNear()
    {
        // is player near enough to start Walk state?
        float _distance = Vector3.Distance(transform.position, player.transform.position);
        bool _near = _distance < 40 ? true : false;
        return _near;
    }
    void Move(float dt)
    {
        walkTimer += dt;
        // node reached or path blocked
        float _previousDir = walkDirection;
        if (transform.position.x > rightNode || sideCollisionL)
        {
            walkDirection = 1;
            sideCollisionR = false;
        }
        else if (transform.position.x < leftNode || sideCollisionR)
        {
            walkDirection = -1;
            sideCollisionL = false;
        }
        if (_previousDir != walkDirection)
        {
            walkTimer = 6;
        }

        // move position if grounded
        if (groundChecker.isGrounded)
        {
            animator.SetFloat("Forward", walkDirection, 0.1f, dt);
            animator.applyRootMotion = true;
        }
        else
            animator.applyRootMotion = false;
    }   
    public void OnAnimatorMove()
    {
        // using OnAnimatorMove to override the default root motion.
        // this allows modification of the positional speed before it's applied.
        if(groundChecker.isGrounded && Time.deltaTime > 0)
        {
            Vector3 _v = (animator.deltaPosition * moveSpeedMultiplier) / Time.deltaTime;

            _v.y = rigidBody.velocity.y; //preserve y velocity
            rigidBody.velocity = _v;
        }
    }
    public void ReleaseProjectile()
    {
        //Is called as an event from the animation clip
        triggerShot = true;
        firingTimer = 0;
        shootDirection = Quaternion.Euler(0, 0, Random.Range(-inaccuracy, inaccuracy)) * -ribCage.right;
        audioSource.PlayOneShot(soundEffects.shoot);
        particles.SetActive(true);
    }
    bool CanFire()
    {
        // is anything blocking the path to the player?
        RaycastHit hit;
        if (Physics.Raycast(new Ray(ribCage.position, (player.transform.position - ribCage.position).normalized), out hit, aggroradius, layers))
        {
            if (hit.transform.tag == "Player")
            {
                playerInRange = true;
                return true;
            }
            else
                return false;
        }
        else
        {
            playerInRange = false;
            return false;
        }
    }
    void OnCollisionEnter(Collision col)
    {
        if((layers == (layers | (1 << col.gameObject.layer))))
        {
            foreach (ContactPoint p in col)
                if (p.normal.x < 0 && p.normal.y == 0)
                {
                    sideCollisionR = true;
                    return;
                }
            else if(p.normal.x > 0 && p.normal.y == 0)
                {
                    sideCollisionL = true;
                }
        }
    }
    void onNeivaTeleport(Vector3 from, Vector3 to)
    {
        hitPlayer = false;
        player.isImmobilized = false;
    }

#if UNITY_EDITOR
    Vector3 originalPos;
    void OnDrawGizmosSelected()
    {
        originalPos = transform.position;
        GUIStyle style = new GUIStyle();
        style.fontSize = 10;
        style.normal.textColor = Color.green;
        if(!Application.isPlaying)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(new Vector3(originalPos.x + leftNode, originalPos.y + 3), new Vector3(leftNode + originalPos.x, originalPos.y, 0));
            UnityEditor.Handles.Label(new Vector3(originalPos.x + leftNode, originalPos.y + 3), "Path_Node_L", style);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(new Vector3(originalPos.x + rightNode, originalPos.y + 3), new Vector3(rightNode + originalPos.x, originalPos.y, 0));
            UnityEditor.Handles.Label(new Vector3(originalPos.x + rightNode, originalPos.y + 3), "Path_Node_R", style);
        }
        Gizmos.color = Color.white;
        UnityEditor.Handles.color = new Color(0.5f, 0, 0, 0.1f);
        UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.back, -Vector3.right, 360f, aggroradius);

        Gizmos.DrawWireSphere(connectingPoint, grabRadius);
    }

    void OnValidate()
    {
        if(!Application.isPlaying)
        {
            leftNode = leftPathNodePosition;
            rightNode = rightPathNodePosition;
        }
    }
#endif
}