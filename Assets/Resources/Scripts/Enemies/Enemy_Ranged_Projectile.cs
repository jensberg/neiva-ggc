﻿using UnityEngine;
using System.Collections;

public class Enemy_Ranged_Projectile : MonoBehaviour {
    private Vector2 direction;
    private SphereCollider m_collider;
    private GameObject playerTarget;
    private LayerMask m_playerLayer;
    private LayerMask m_trapLayer;
    private LayerMask m_groundLayer;

    public float speed = 5.0f;

    //debug test scaling
    public Vector3 startScale = new Vector3(0.25f, 0.25f, 0.25f);
    Vector3 scaleDecrease;
    public Vector3 endScale = new Vector3(0.5f, 0.5f, 0.5f);
    public float scaleSpeed = 5.0f;
    private float lifeTimer;
    public float lifeTime = 1.0f;

    public float KnockbackForce = 20f;
    public int Damage = 20;
    // Use this for initialization
    void Start () {
        m_collider = GetComponent<SphereCollider>();
        m_groundLayer = (1 << LayerMask.NameToLayer("Ground"));
        m_playerLayer = (1 << LayerMask.NameToLayer("Player"));
        m_trapLayer = (1 << LayerMask.NameToLayer("Trap"));
    }

    public void OnProjectileLaunch(Vector3 startPos, Vector2 dir)
    {
        transform.position = startPos;
        direction = dir;
        transform.localScale = startScale;
        lifeTimer = 0;


    }

    // Update is called once per frame
    void Update () {
        lifeTimer += Time.deltaTime;

        if (lifeTimer > lifeTime)
            SetInactive();

        transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);
        transform.localScale = Vector3.MoveTowards(transform.localScale, endScale, Time.deltaTime * scaleSpeed);
        if (HandleGroundCollision())
            SetInactive();
        if (HandlePlayerCollision())
        {
            GameManager.Singleton.Neiva.health.TakeDamage(Health.DamageType.Projectile);
            SetInactive();
        }
        if(HandleTrapCollision())
        {
            SetInactive();
        }
    }
    
    bool HandleGroundCollision()
    {
        return Physics.CheckSphere(transform.position, m_collider.radius, m_groundLayer, QueryTriggerInteraction.Ignore);
    }
    bool HandlePlayerCollision()
    {
        return Physics.CheckSphere(transform.position, m_collider.radius, m_playerLayer, QueryTriggerInteraction.Ignore);
    }
    bool HandleTrapCollision()
    {
        return Physics.CheckSphere(transform.position, m_collider.radius, m_trapLayer, QueryTriggerInteraction.Collide);
    }
    
    void SetInactive()
    {
        this.gameObject.SetActive(false);
    }
}
