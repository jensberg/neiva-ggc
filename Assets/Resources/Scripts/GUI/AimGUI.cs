﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(RectTransform), typeof(Image))]
public class AimGUI : MonoBehaviour
{
	public Vector3 originOffset = Vector3.zero;
    public float distance = 210;
	NeivaUserControl m_neivaUser = null;
	Image m_image = null;
    public float speed = 10f;
	
	void Awake()
	{
		m_neivaUser = FindObjectOfType<NeivaUserControl>();
		m_image = GetComponent<Image>();
	}
	
	void Update()
	{
		if (m_neivaUser.m_inputType == NeivaUserControl.InputType.XboxController)
		{
			m_image.enabled = m_neivaUser.AimInput.magnitude > 0f;
			transform.position = Vector3.Lerp(transform.position,
				Camera.main.WorldToScreenPoint(m_neivaUser.NeivaController.throwFrom.position + Vector3.up * 0.5f) + (Vector3)m_neivaUser.AimInput * distance + originOffset, Time.deltaTime * speed);
		}
		else
		{
			m_image.enabled = true;
			transform.position = Input.mousePosition;
		}
	}
}
