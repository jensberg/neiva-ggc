﻿using UnityEngine;
using System.Collections;

public class UI_Parent : MonoBehaviour {
    public GameObject ButtonCanvas;
    public GameObject ParticleCanvas;
    public GameObject HealthCanvas;
    public GameObject PauseCanvas;
    public GameObject TutorialCanvas;
    public GameObject PickupCanvas;
    public GameObject ProjectileCanvas;
    public GameObject DevPauseCanvas;
    void Start()
    {
        ButtonCanvas = transform.GetChild(0).gameObject;
        ParticleCanvas = transform.GetChild(1).gameObject;
        HealthCanvas = transform.GetChild(2).gameObject;
        PauseCanvas = transform.GetChild(3).gameObject;
        TutorialCanvas = transform.GetChild(4).gameObject;
        PickupCanvas = transform.GetChild(5).gameObject;
        ProjectileCanvas = transform.GetChild(6).gameObject;
        DevPauseCanvas = transform.GetChild(7).gameObject;
    }
}
