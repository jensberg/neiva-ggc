﻿using UnityEngine;
using System.Collections;

public class TutorialManager : MonoBehaviour {

    public GameObject bounce = null;
    public GameObject swap = null;
    // Use this for initialization
    void Start () {
        bounce.SetActive(false);
        swap.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if(bounce.activeSelf == true)
	    if(Input.GetKeyDown(KeyCode.JoystickButton1) || Input.GetKeyDown(KeyCode.Alpha2))
        {
            bounce.SetActive(false);
        }
        if(swap.activeSelf == true)
        if(Input.GetKeyDown(KeyCode.JoystickButton3) || Input.GetKeyDown(KeyCode.Alpha3))
        {
            swap.SetActive(false);
        }
	}

    public void ActivateBounce()
    {
        StartCoroutine(SetObject(bounce, true, 2f));
    }
    public void ActivateSwap()
    {
        StartCoroutine(SetObject(swap, true, 2f));
    }

    IEnumerator SetObject(GameObject _gameObject, bool _state, float _time)
    {
        yield return new WaitForSeconds(_time);
        _gameObject.SetActive(_state);
    }
}
