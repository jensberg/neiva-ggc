﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;



public class UI_HealthManagement : MonoBehaviour
{
    [HideInInspector]
    public struct UiHealth
    {
        public GameObject gameObject;
        public Transform transform;
        public Animator animator;
        public Image image;
        public bool active;
    }
    // // 
    private float curveTimer;
    public AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
    private float curveSpeed;
    private bool onRestored = false;
    // //
    private int health;
    private UiHealth[] m_uiHealth = new UiHealth[3];
    private int lastObject;
    public float TransitionTimeUp = 8f;
    public float TransitionTimeDown = 10f;
    public float MaxAlpha = 1f;
    public float MinAlpha = 0.25f;
    private Vector3 startScale;
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            m_uiHealth[i] = new UiHealth();
            m_uiHealth[i].active = true;
            m_uiHealth[i].gameObject = transform.GetChild(i).gameObject;
            m_uiHealth[i].transform = m_uiHealth[i].gameObject.transform;
            m_uiHealth[i].image = m_uiHealth[i].gameObject.GetComponent<Image>();
            m_uiHealth[i].animator = m_uiHealth[i].gameObject.GetComponent<Animator>();
        }
        startScale = m_uiHealth[0].gameObject.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        health = GameManager.Singleton.Neiva.health.GetHealth();
        m_uiHealth[health - 1].animator.SetBool("Active", true);

        for (int i = 0; i < transform.childCount; i++)
        {
            if (!onRestored)
            {
                if (i >= health)
                {
                    m_uiHealth[i].image.color = Color.Lerp(m_uiHealth[i].image.color, new Color(1, 1, 1, MinAlpha), Time.deltaTime * TransitionTimeDown);
                }
                else
                {
                    m_uiHealth[i].image.color = Color.Lerp(m_uiHealth[i].image.color, new Color(1, 1, 1, MaxAlpha), Time.deltaTime * TransitionTimeUp);

                    if (m_uiHealth[i].transform.localScale != startScale)
                    {
                        m_uiHealth[i].transform.localScale = Vector3.MoveTowards(m_uiHealth[i].transform.localScale, startScale, Time.deltaTime * TransitionTimeDown / 2);
                    }

                }
            }
        }
        if (onRestored)
        {
            for (int i = 0; i < m_uiHealth.Length; i++)
            {
                if (m_uiHealth[i].active == false)
                    MakeItPop(i);
            }
        }
    }

    private void MakeItPop(int i)
    {
        m_uiHealth[i].active = true;
        onRestored = false;
        curveTimer = 0;
        return;
        curveTimer += Time.deltaTime;
        curveSpeed = curve.Evaluate(curveTimer);
        m_uiHealth[i].transform.localScale = Vector3.zero;
    }

    public void OnTakeDamage()
    {
        int i = 0;
        for (; i < m_uiHealth.Length; i++) if (m_uiHealth[i].active == false) break;
        m_uiHealth[i - 1].active = false;
        m_uiHealth[i - 1].animator.SetBool("Death", true);
        m_uiHealth[i - 1].animator.SetBool("Active", false);
        //Final fix? Move healthcheck from update?
        //Play animation / particle system or whatever
    }

    public void OnRestore()
    {
        if (Time.time > 0.2f)
        {
            for (int i = 0; i < m_uiHealth.Length; i++)
            {
                if (m_uiHealth[i].active == false)
                {
                    onRestored = true;
                    if(GameManager.Singleton.stateTree.currentState.Tag() != "Menu")
                    StartCoroutine(PopElement(i));
                }
                if(m_uiHealth[i].active == true)
                {
                    m_uiHealth[i].animator.SetBool("Active", false);

                }
            }
        }
    }

    IEnumerator PopElement(int i)
    {
        yield return new WaitForSeconds(i * 0.15f);
        m_uiHealth[i].animator.SetBool("Death", false);
    }
}
