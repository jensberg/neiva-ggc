﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI_GetPickups : MonoBehaviour {
    private Text m_text;
	// Use this for initialization
	void Start () {
        m_text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        m_text.text = GameManager.Singleton.Neiva.GetSouls().ToString();
	}
}
