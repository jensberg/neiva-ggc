﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour {
    //Needs trigger
    //Header -> Menu.OnTrigger
    //Gamemanager.InMenu = false
    Transform player;
    Image header;
    Camera m_camera;
    bool triggered;
    public float TransitionTime = 10f;
	// Use this for initialization
	void Start () {
        player = GameManager.Singleton.Neiva.gameObject.transform;
        header = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (triggered)
            RunActive();
	}

    void RunActive()
    {
        header.color = Color.Lerp(header.color, new Color(1, 1, 1, 0), Time.deltaTime * TransitionTime);
        if (header.color.a == 0)
            Destroy(gameObject);
    }

    public void OnTrigger()
    {
        triggered = true;
    }
}
