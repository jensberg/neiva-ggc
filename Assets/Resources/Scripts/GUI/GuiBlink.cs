﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GuiBlink : MonoBehaviour {
    private bool isTop = false;
    private Image image;
    private Color targetColor;
    private float alpha;
    private float targetAlpha;
    private float stayTopTimer;
    public float stayTopTime = 1f;
    public float transitionSpeed = 1f;
    public bool isJump = false;
    private bool isJumpActive;
    private Image image2;
	// Use this for initialization
	void Start () {
        image = transform.GetChild(0).GetComponent<Image>();
        if(isJump)
        image2 = transform.GetChild(1).GetComponent<Image>();
        if (image == null)
        {
            Debug.Log("Image in child could not be found.");
            this.enabled = false;
        }
        isJumpActive = isJump;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!isJumpActive)
            ScaleObjectOne();
        else if (isJump && isJumpActive)
            ScaleObjectTwo();
    }

    void ScaleObjectOne()
    {
        if (image.color.a == 1)
        {
            stayTopTimer += Time.deltaTime;
            if (stayTopTimer >= stayTopTime)
            {
                isTop = false;
                stayTopTimer = 0;
            }
        }
        else if (image.color.a == 0f)
        {
            isTop = true;
            if(isJump)
            isJumpActive = true;
        }
        if (isTop)
            targetAlpha = 1;
        else
            targetAlpha = 0f;

        alpha = Mathf.MoveTowards(alpha, targetAlpha, Time.deltaTime * transitionSpeed);
        targetColor = new Color(1, 1, 1, alpha);
        image.color = targetColor;
    }

    void ScaleObjectTwo()
    {
        if (image2.color.a == 1)
        {
            stayTopTimer += Time.deltaTime;
            if (stayTopTimer >= stayTopTime)
            {
                isTop = false;
                stayTopTimer = 0;
            }
        }
        else if (image2.color.a == 0f)
        {
            isTop = true;
            isJumpActive = false;
        }
        if (isTop)
            targetAlpha = 1;
        else
            targetAlpha = 0f;

        alpha = Mathf.MoveTowards(alpha, targetAlpha, Time.deltaTime * transitionSpeed);
        targetColor = new Color(1, 1, 1, alpha);
        image2.color = targetColor;
    }

    public void SetChild(int i)
    {
        image = transform.GetChild(i).GetComponent<Image>();
    }
}
