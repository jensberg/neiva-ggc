﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AimShootTeleportText : MonoBehaviour {
    public Sprite m_shoot;
    public float TransitionTime;
    [SerializeField][TextArea] private string Aim;
    [SerializeField][TextArea] private string Shoot;
    [SerializeField][TextArea] private string Teleport;
    private GuiBlink m_guiBlink;
    private GameObject[] m_textWindowChilds = new GameObject[3];
    private GameObject[] m_buttons = new GameObject[2];
    private Image[] m_buttonImage = new Image[2];
    public enum State
    {
        Aim,
        Shoot,
        Teleport,
    } public State m_state;
    // Use this for initialization
    void Start () {
        for (int i = 0; i < m_textWindowChilds.Length; i++)
        {
            m_textWindowChilds[i] = transform.parent.GetChild(0).GetChild(i).gameObject;
            m_textWindowChilds[i].SetActive(false);
        }
        for (int i = 0; i < transform.childCount; i++)
        {
            m_buttons[i] = transform.GetChild(i).gameObject;
            m_buttonImage[i] = m_buttons[i].GetComponent<Image>();
            if (i != 0)
                m_buttons[i].SetActive(false);
        }

        m_guiBlink = GetComponent<GuiBlink>();

    }

    // Update is called once per frame
    void Update () {
        InputTrigger();

        if(m_state == State.Aim)
        {
            m_textWindowChilds[0].SetActive(true);
        }
        if (m_state == State.Shoot)
        {
            m_textWindowChilds[0].SetActive(false);
            m_textWindowChilds[1].SetActive(true);

            m_buttons[0].SetActive(false);
            m_buttons[1].SetActive(true);
            m_guiBlink.SetChild(1);    
        }
        if (m_state == State.Teleport)
        {
            m_textWindowChilds[0].SetActive(false);
            m_textWindowChilds[1].SetActive(false);
            m_textWindowChilds[2].SetActive(true);
            m_buttons[0].SetActive(false);
            m_buttons[1].SetActive(true);

        }
    }

    void InputTrigger()
    {
        if(m_state == State.Aim)
        if(GameManager.Singleton.Neiva.aimInput != Vector2.zero)
        {
            m_state = State.Shoot;
        }
        if(Input.GetKeyDown(KeyCode.JoystickButton5))
        {
            m_state = State.Teleport;
        }
    }
}
