﻿using UnityEngine;
using System.Collections;

public class PauseManager : MonoBehaviour {
    private int Choices = 4;
    private int choice = 1;
    private enum InputType
    {
        A,
        B, 
        Up,
        Down,
        Invalid,
    }private InputType input;
    private Vector2 joyInput;
    private Vector2 dpadInput;
    private float cooldown;

    //
    GameObject MainMenu;
    GameObject Controls;
    Animator[] playChoice;
    public bool isSga = false;
    public float cooldownTime = 0.2f;
	// Use this for initialization
	void Start () {
        if(!isSga)
        {
            Choices = 3;
            playChoice = new Animator[3];
            transform.GetChild(0).GetChild(3).gameObject.SetActive(false);
        }
        if(isSga)
        {
            Choices = 4;
            playChoice = new Animator[4];
        }

        input = InputType.Invalid;

        MainMenu = transform.GetChild(0).gameObject;
        Controls = transform.GetChild(1).gameObject;

        for(int i = 0; i < playChoice.Length; i++)
        {
            playChoice[i] = transform.GetChild(0).GetChild(i).GetComponent<Animator>();
        }
	}

    void OnDisable()
    {
        if(MainMenu != null)
        MainMenu.SetActive(true);
        if(Controls != null)
        Controls.SetActive(false);
        cooldown = 0;
        input = InputType.Invalid;
        choice = 1;
    }
	
	// Update is called once per frame
	void Update () {
        cooldown += Time.fixedDeltaTime;

        choice = Mathf.Clamp(choice, 1, Choices);
        if (cooldown > cooldownTime)
            GetInput();
        HandleInput();
        
        for(int i = 0; i < playChoice.Length; i++)
        {
            if(i == choice -1)
            {
                playChoice[i].SetBool("Active", true);
            }
            else
            {
                playChoice[i].SetBool("Active", false);
            }
        }
    }

    void GetInput()
    {
        joyInput = NeivaUserControl.GetXboxAxis(NeivaUserControl.XboxAxis.LS);
        dpadInput = NeivaUserControl.GetXboxAxis(NeivaUserControl.XboxAxis.DPAD);
        if (Input.GetKeyDown(KeyCode.JoystickButton0) || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
        {
            input = InputType.A;
        }
        else if (Input.GetKeyDown(KeyCode.JoystickButton1) || Input.GetKeyDown(KeyCode.Escape))
        {
            input = InputType.B;
        }
        else if(joyInput.y > 0.75f || dpadInput.y > 0.75f || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            input = InputType.Up;
        }
        else if (joyInput.y < -0.75f ||dpadInput.y < -0.75f || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            input = InputType.Down;
        }

        if (input != InputType.Invalid)
        {
            cooldown = 0;
        }

    }

    void HandleInput()
    {
        if(input == InputType.Down)
        {
            if(choice < Choices)
            choice++;
            input = InputType.Invalid;
        }
        if(input == InputType.Up)
        {
            if(choice > 1)
            choice--;
            input = InputType.Invalid;
        }
        if(input == InputType.A)
        {
            if(MainMenu.activeSelf)
            Continue();

            input = InputType.Invalid;
        }
        if(input == InputType.B)
        {
            if(!MainMenu.activeSelf)
            {
                MainMenu.SetActive(true);
                Controls.SetActive(false);
            }
            else
            {
                Pause.Unpause();
            }
            input = InputType.Invalid;
        }


    }

    void Continue()
    {
        if(choice == 1)
        {
            Pause.Unpause();
        }
        else if (choice == 2)
        {
            if (MainMenu.activeSelf)
            {
                MainMenu.SetActive(false);
                Controls.SetActive(true);
            }
        }
        else if (choice == 3)
        {
            Time.timeScale = 1;
            GameManager.Singleton.RestartScene();
        }
        else if(choice == 4)
        {
            Application.Quit();
        }
    }

}
