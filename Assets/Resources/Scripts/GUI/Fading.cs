﻿using UnityEngine;
using System.Collections;

public class Fading : MonoBehaviour
{

    public Texture2D fadeOutTexture;
    public float fadeSpeed = 0.8f;

    private int drawdepth = -1000;
    private float alpha = 1.0f;
    private float targetAlpha = 0f;
    private int fadeDir = -1;

    void Start()
    {
        //DontDestroyOnLoad(gameObject);
    }

    void OnGUI()
    {
        alpha = Mathf.MoveTowards(alpha, targetAlpha, Time.deltaTime);
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = drawdepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
    }

    public void beginFade()
    {
        if (alpha == 1)
            targetAlpha = 0;
        else
            targetAlpha = 1;
    }

    public void FadeOut()
    {
        alpha = 0;
        targetAlpha = 1;
    }

    public void FadeIn()
    {
        alpha = 1;
        targetAlpha = 0;
    }

    void OnLevelWasLoaded()
    {
        beginFade();
    }
}
