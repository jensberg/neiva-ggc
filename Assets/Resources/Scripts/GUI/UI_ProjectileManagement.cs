﻿using UnityEngine;
using UnityEngine.UI;

public class UI_ProjectileManagement : MonoBehaviour {
    [HideInInspector]
    public struct UIProjectiles
    {
        public GameObject gameObject;
        public Transform transform;
        public Animator animator;
        public Image image;
        public bool active;
    }

    private NeivaController.SoulType currentProjectile;
    private UIProjectiles[] uiProjectiles = new UIProjectiles[3];
    public float TransitionTimeUp = 8;
    public float TransitionTimeDown = 10f;
    public float MaxAlpha = 1f;
    public float MinAlpha = 0.25f;
    private int numOfProjectiles = 3;
    // Use this for initialization
    void Start () {
        for (int i = 0; i < transform.childCount; i++)
        {
            uiProjectiles[i] = new UIProjectiles();
            uiProjectiles[i].active = true;
            uiProjectiles[i].gameObject = transform.GetChild(i).gameObject;
            uiProjectiles[i].transform = uiProjectiles[i].gameObject.transform;
            uiProjectiles[i].image = uiProjectiles[i].gameObject.GetComponent<Image>();
            uiProjectiles[i].animator = uiProjectiles[i].gameObject.GetComponent<Animator>();
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        currentProjectile = GameManager.Singleton.Neiva.selectedSoulType;
        for (int i = 0; i < transform.childCount; i++)
            if (i == (int)currentProjectile)
            {
                uiProjectiles[i].image.color = Color.Lerp(uiProjectiles[i].image.color, new Color(1, 1, 1, MaxAlpha), Time.deltaTime * TransitionTimeUp);
                uiProjectiles[i].animator.SetBool("Active", true);
            }
            else
            {
                uiProjectiles[i].image.color = Color.Lerp(uiProjectiles[i].image.color, new Color(1, 1, 1, MinAlpha), Time.deltaTime * TransitionTimeDown);
                uiProjectiles[i].animator.SetBool("Active", false);

            }
    }
}
