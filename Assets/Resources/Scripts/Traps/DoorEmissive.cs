﻿using UnityEngine;
using System.Collections;

public class DoorEmissive : MonoBehaviour {

    TriggerDoor m_triggerDoor;
    Material mat;
    float emissionMultiplier = 1f;
    Color basecolor;
    [SerializeField]
    // Use this for initialization
    void Start () {
        m_triggerDoor = GetComponent<TriggerDoor>();
        mat = GetComponentInChildren<MeshRenderer>().material;
        basecolor = mat.GetColor("_EmissionColor");
    }
	
	// Update is called once per frame
	void Update () {

        if(m_triggerDoor.m_open)
        EmissionMoveTo(1f);
        else
        EmissionMoveTo(0f);
    }

    void EmissionMoveTo(float value)
    {
        emissionMultiplier = Mathf.MoveTowards(emissionMultiplier, value, Time.deltaTime);
        mat.SetColor("_EmissionColor", basecolor * emissionMultiplier);
    }

}
