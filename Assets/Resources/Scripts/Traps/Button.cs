﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(EventTrigger))]
public class Button : MonoBehaviour {
    Vector3 m_startPos;
    Vector3 m_destPos;
    BoxCollider m_collider;
    public Vector2 offset = new Vector2(0.0f, 0.225f);
    EventTrigger m_eventTrigger;
    bool atBottom = false;

    //Emissive
    Material mat;
    float emissionMultiplier = 1f;
    Color basecolor;
	// Use this for initialization
	void Start () {
        m_startPos = transform.position;
        m_destPos = new Vector3(transform.position.x - offset.x , transform.position.y - offset.y, transform.position.z);
        m_eventTrigger = GetComponent<EventTrigger>();

        //Emissive
        mat = GetComponent<MeshRenderer>().material;
        basecolor = mat.GetColor("_EmissionColor");
	}
	
	// Update is called once per frame
	void Update ()
    {
        m_destPos = new Vector3(transform.parent.position.x - offset.x, transform.parent.position.y - offset.y, transform.position.z);
        if (m_eventTrigger.isTriggered)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_destPos, Time.deltaTime);
        }
        else if(!m_eventTrigger.isTriggered)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_startPos, Time.deltaTime);
        }

        if (transform.position == m_destPos)
            atBottom = true;
        else
            atBottom = false;

        //Emissive
        if(atBottom)
        {
            emissionMultiplier = Mathf.MoveTowards(emissionMultiplier, 0, Time.deltaTime);
            mat.SetColor("_EmissionColor", basecolor * emissionMultiplier);
            m_startPos = new Vector3(transform.position.x + offset.x, transform.position.y + offset.y, transform.position.z);

        }
        else
        {
            emissionMultiplier = Mathf.MoveTowards(emissionMultiplier, 1, Time.deltaTime);
            mat.SetColor("_EmissionColor", basecolor * emissionMultiplier);
        }

    }
}
