﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class MovingPlatformRockPlacer : MonoBehaviour {
    private FollowPath m_followScript;

    private PlattformPath path;

    [SerializeField]
    public GameObject[] m_objectToSpawn = new GameObject[2];
    // Use this for initialization
    void Start () {
        m_followScript = GetComponent<FollowPath>();
        path = m_followScript.path;
         if (path == null)
        {
            Debug.LogError("path cannot be null", gameObject);
            return;
        }
        Spawn();
	}
	

    void Spawn()
    {    
            Instantiate(m_objectToSpawn[0], m_followScript.path.Points[0].position, Quaternion.Euler(0, 0, 0));
            Instantiate(m_objectToSpawn[1], m_followScript.path.Points[m_followScript.path.Points.Length-1].position, Quaternion.Euler(0, 0, 0));
    }
}
