﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof(SphereCollider))]
public class Magnet : MonoBehaviour
{
	// types
	public enum Type
	{
		Push, Pull
	}

	// variables
	public LayerMask affectingLayer = 1;
	public Type magnetType = Type.Pull;
	public float power = 1f;
	public AnimationCurve forceCurve;

	// accessors
	public SphereCollider SphereCollider { get; private set; }

	// events
	public UnityAction<Rigidbody> onRigidbodyInMagnet = null;

	// methods
	void Awake()
	{
		SphereCollider = GetComponent<SphereCollider>();
	}

	void OnTriggerStay(Collider col)
	{
		if (col.gameObject != this.gameObject)
			if (affectingLayer == (affectingLayer | (1 << col.gameObject.layer)))
				if (col.attachedRigidbody != null)
				{
					Vector3 dv = SphereCollider.bounds.center - col.attachedRigidbody.position;
					float t = Mathf.Clamp01(1f - dv.magnitude / SphereCollider.radius);
					col.attachedRigidbody.AddForceAtPosition(
						dv.normalized * forceCurve.Evaluate(t) * (magnetType == Type.Push ? -power : power) / 100f,
						SphereCollider.bounds.center,
						ForceMode.Force
						);

					if (onRigidbodyInMagnet != null)
						onRigidbodyInMagnet(col.attachedRigidbody);
				}
	}

	// unity editor
	void OnValidate()
	{
		power = Mathf.Clamp(power, 0f, float.MaxValue);
	}
}
