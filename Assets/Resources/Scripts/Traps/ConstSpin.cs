﻿using UnityEngine;
using System.Collections;

public class ConstSpin : MonoBehaviour {
        
    public enum Axis
    {
        X,
        Y,
        Z
    }
    public Axis axis;
    public float speed = 1.0f;
	// Update is called once per frame
	void Update () {
	    switch(axis)
        {
            case Axis.X:
                transform.Rotate(speed * Time.deltaTime, transform.rotation.y, transform.rotation.z);
                break;
            case Axis.Y:
                transform.Rotate(transform.rotation.x, speed * Time.deltaTime, transform.rotation.z);
                break;
            case Axis.Z:
                transform.Rotate(transform.rotation.x, transform.rotation.y, speed * Time.deltaTime);
                break;
        }


	}
}
