﻿using UnityEngine;
using System.Collections;

public class FallTrap : MonoBehaviour {

    public float m_waitForDropTime = 0.4f;

    private bool m_fall = false;
    private float m_timer;
    private Animator m_animator;
    private AudioSource m_audioSource;
    [HideInInspector]
    public bool m_triggered;

    public EventTrigger m_trigger;

    public AudioClip m_soundEffect;

    void Awake()
    {
        m_animator = GetComponentInParent<Animator>();
        m_audioSource = GetComponent<AudioSource>();
    }
    void Update()
    {
        if(m_triggered && m_trigger == null)
        {
            m_timer += Time.deltaTime;
            if(m_timer > m_waitForDropTime)
            {
                m_animator.SetBool("Fall", true);
                m_animator.SetBool("Tremor", false);
                if(!m_audioSource.isPlaying)
                    m_audioSource.PlayOneShot(m_soundEffect);
            }
            if (m_timer > m_waitForDropTime + 0.5f)
            {
                m_animator.SetBool("Fall", false);
                m_animator.SetBool("Tremor", false);
                m_triggered = false;
                m_timer = 0;
            }
        }
        else if(m_triggered && m_trigger != null)
        {
            m_timer += Time.deltaTime;
            if (m_timer > m_waitForDropTime)
            {
                m_animator.SetBool("Fall", true);
                m_animator.SetBool("Tremor", false);
            }
            if (m_trigger.isTriggered)
            {
                m_animator.SetBool("Fall", false);
                m_animator.SetBool("Tremor", false);
                m_timer = 0;
                m_triggered = false;
            }
        }
    }

    void Fall()
    {
        m_triggered = true;
        m_timer = 0;
        
        m_animator.SetBool("Tremor", true);
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Player" || col.gameObject.tag == "TestForSwap")
        {
            foreach (ContactPoint c in col)
            {
                if (c.normal.y < 0)
                {
                    Fall();
                    return;
                }
            }
        }
    }
}
