﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(EventTrigger))]
public class SinkingPlatform : MonoBehaviour {
    Vector3 m_startPos;
    Vector3 m_destPos;
    BoxCollider m_collider;
    public float offset = 0.5f;
    public float speed = 1.5f;
    private bool onButton = false;
    EventTrigger m_eventTrigger;
    bool atBottom = false;

    // Use this for initialization
    void Start () {
        m_startPos = transform.position;
        m_destPos = new Vector3(transform.position.x, transform.position.y - offset, transform.position.z);
        m_eventTrigger = GetComponent<EventTrigger>();
    }
	
	// Update is called once per frame
	void Update () {

        if (m_eventTrigger.isTriggered)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_destPos, speed * Time.deltaTime);
        }
        else if (!m_eventTrigger.isTriggered)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_startPos, speed * Time.deltaTime);
        }

        if (transform.position == m_destPos)
            atBottom = true;
        else
            atBottom = false;
    }
}
