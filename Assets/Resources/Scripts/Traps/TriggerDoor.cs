﻿using UnityEngine; 
using System.Collections;
using UnityEngine.Events;

public class TriggerDoor : MonoBehaviour {
   
    public EventTrigger[] m_triggers;
    [Space]
    public GameObject m_door;
    [Space]
    [Range(0,20)]
    public float m_openingSpeed = 1.2f;
    [Range(0, 20)]  
    public float m_closingSpeed = 3f;
    [Space]
    public float[] m_heights;
    private float m_currentMaxHeight;

    Vector3 m_originalPos;
    [HideInInspector]
    public bool m_open;
    private float m_openingTimer;
    private float m_onCollisionTimer;
    private bool m_playedSound;
    private bool m_forceClose;
    private bool m_shaken;

    [System.Serializable]
    public struct CloseOnTrigger
    {
        public bool enable; 
        public enum Angle { Verticle, Sideways }
        public Angle angle;
    }
    public CloseOnTrigger closeOnTrigger;
    [Space]
    public UnityEvent onClosingImpact;
    public UnityEvent onClosing;

    private ScreenShake m_screenshake;
    public AudioSource m_audioSource;
    private AudioClip m_openingSound;

    public bool isHiss;

    void Awake()
    {
        m_originalPos = m_door.transform.position;
        m_screenshake = Camera.main.GetComponent<ScreenShake>();
        if(m_audioSource != null)
            m_openingSound = m_audioSource.clip;
        m_currentMaxHeight = m_heights[0];
        if (closeOnTrigger.enable)
        {
            m_open = true;
            if (closeOnTrigger.angle == CloseOnTrigger.Angle.Verticle)
                m_door.transform.position = new Vector3(m_door.transform.position.x, m_door.transform.position.y + m_heights[0], m_door.transform.position.z);
            else if (closeOnTrigger.angle == CloseOnTrigger.Angle.Sideways)
                m_door.transform.position = new Vector3(m_door.transform.position.x, m_door.transform.position.y, m_door.transform.position.z + m_heights[0]);
        }
        else
            closeOnTrigger.angle = CloseOnTrigger.Angle.Verticle;
    }
    void Update()
    {
        //Opening door.
        if (m_open && Vector3.Distance(m_door.transform.position, m_originalPos) < m_currentMaxHeight)
        {
            m_audioSource.clip = m_openingSound;
            if (m_openingTimer < m_openingSpeed + 10f)
                m_openingTimer += Time.deltaTime * 0.8f;
            if (!isHiss)
                m_door.transform.Translate(Vector3.up * Time.deltaTime * m_openingTimer); //Translation with acceleration
            else
                m_door.transform.Translate(Vector3.right * Time.deltaTime * m_openingTimer);

            m_onCollisionTimer += Time.deltaTime;   //Add to timer for allowing invoking of OnClosingImpact Event
            if (!m_audioSource.isPlaying && !m_playedSound)
            {
                m_audioSource.Play();
                m_playedSound = true;
            }
            m_forceClose = false;
            m_shaken = false;
        }
        //Closing door
        else if (!m_open && Vector3.Distance(m_door.transform.position, m_originalPos) > 0.09f && !m_forceClose)
        {
            if(!isHiss)
             m_door.transform.Translate(Vector3.down * Time.deltaTime * m_closingSpeed);
            else
                m_door.transform.Translate(Vector3.left * Time.deltaTime * m_closingSpeed);
            if (!m_audioSource.isPlaying || m_audioSource.clip == m_openingSound)
            {
                if(!m_shaken)
                {
                    Camera.main.GetComponent<ScreenShake>().ShakeTrigger(0.8f);
                    m_shaken = true;
                }
                if (onClosing != null)
                    onClosing.Invoke();
            }
            m_openingTimer = 0;
            m_playedSound = false; 

            //Force close if positions are too wrong
            if (closeOnTrigger.angle == CloseOnTrigger.Angle.Verticle && m_door.transform.position.y < m_originalPos.y && !isHiss)
                m_forceClose = true;
            else if (closeOnTrigger.angle == CloseOnTrigger.Angle.Sideways && m_door.transform.position.x < m_originalPos.x && !isHiss)
                m_forceClose = true;
            if(isHiss)
            {
                if(m_door.transform.position.y + 0.1f > m_originalPos.y)
                {
                    m_forceClose = true;
                }
            }
        }
        //If door closes completely
        else if(!m_open && Vector3.Distance(m_door.transform.position, m_originalPos) < 0.09f || m_forceClose)
        {
            m_door.transform.position = m_originalPos; //Set the original position of the door again

            if (m_onCollisionTimer > 2f) //How much the door needs to open before triggering events. Timer is increased while opening.
            {
                if(onClosingImpact != null)
                {
                    
                    onClosingImpact.Invoke();
                    m_onCollisionTimer = 0;
                }
            }
            m_openingTimer = 0;
            m_forceClose = false;
        }
        CheckTriggers();  
    }

    void CheckTriggers()
    {
        //Check if all connected triggers are activated
        if (m_heights.Length == 1)
        {
            for (int i = 0; i < m_triggers.Length; i++)
            {
                if (!m_triggers[i].isTriggered)
                {
                    m_open = false;
                    if (closeOnTrigger.enable)
                        m_open = !m_open;
                    return;
                }
                else
                {
                    m_open = true;
                    if (closeOnTrigger.enable)
                        m_open = !m_open;
                }
            }
        }
        else
        {
            for (int i = 0; i < m_triggers.Length; i++)
            {
                if (m_triggers[i].isTriggered)
                {
                    m_open = true;
                    if (closeOnTrigger.enable)
                        m_open = !m_open;
                    m_currentMaxHeight = m_heights[i];
                }
            }
        }
    }
}
