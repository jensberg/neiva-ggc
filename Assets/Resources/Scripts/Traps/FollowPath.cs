﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class FollowPath : MonoBehaviour {

    public enum FollowType
    {
        MoveTowards,
        Lerp
    }

    public FollowType Type = FollowType.MoveTowards;
    public PlattformPath path;
    public float Speed = 1;
    public float MaxDistanceToGoal = .1f;

    private IEnumerator<Transform> m_currentPoint;
    private Rigidbody m_rigidbody;

    public void Start()
    {
        if (path == null)
        {
            Debug.LogError("path cannot be null", gameObject);
            return;
        }
        m_currentPoint = path.GetPathsEnumeratos();
        m_currentPoint.MoveNext();

        if (m_currentPoint.Current == null)
            return;

        transform.position = m_currentPoint.Current.position;
        m_rigidbody = GetComponent<Rigidbody>();
    }

    public void FixedUpdate()
    {

        if (m_currentPoint == null || m_currentPoint.Current == null)
            return;

            if (Type == FollowType.MoveTowards)
                m_rigidbody.MovePosition(Vector3.MoveTowards(transform.position, m_currentPoint.Current.position, Time.fixedDeltaTime * Speed));
            else if (Type == FollowType.Lerp)
                m_rigidbody.MovePosition(Vector3.Lerp(transform.position, m_currentPoint.Current.position, Time.fixedDeltaTime * Speed));

        var distanceSquared = (transform.position - m_currentPoint.Current.position).sqrMagnitude;
        if (distanceSquared < MaxDistanceToGoal * MaxDistanceToGoal)
            m_currentPoint.MoveNext();
        

    }
}
