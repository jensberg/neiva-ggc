﻿using UnityEngine;
using System.Collections;
public class SpikeTrap : MonoBehaviour {
    public float KnockbackForce = 5.0f;
    public int Damage = 20;
    AudioSource audioSource;
    public AudioClip projDeathSound;
    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
	}

    void Update()
    {

    }
	
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        TakeDamage();
        if (other.tag == "Projectile")
		{
			Soul soul = other.gameObject.GetComponent<Soul>();
            soul.stateMachine.setState(new Soul.CooldownState());
            if (!audioSource.isPlaying)
                audioSource.PlayOneShot(projDeathSound);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
            TakeDamage();

    }
    void TakeDamage()
    {
        GameManager.Singleton.Neiva.health.TakeDamage(Health.DamageType.Spike);
        //GameManager.Singleton.Neiva.KnockBack(knockDirection.normalized * KnockbackForce);
        //GameManager.Singleton.Neiva.FixedKnockback();
    }
}
