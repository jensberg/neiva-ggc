﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class Water : MonoBehaviour {
    public int Damage = 20;
    public GameObject m_WaterSplash;
    // Use this for initialization

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if(m_WaterSplash)
                Instantiate(m_WaterSplash, other.transform.position, m_WaterSplash.transform.rotation);
            else
            {
                Debug.Log("WaterSplash could not be found");
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        { 
            TakeDamage();
        }
    }
    void TakeDamage()
    {
        GameManager.Singleton.Neiva.health.TakeDamage(Health.DamageType.Water);
    }
}
