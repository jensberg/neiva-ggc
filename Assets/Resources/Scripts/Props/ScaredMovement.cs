﻿using UnityEngine;
using System.Collections;

public class ScaredMovement : MonoBehaviour {
    private Vector3 direction;
    public bool active;
    public float speed = 1f;
    EventTrigger m_eventTrigger;
    LimitedLifetime m_lifeTime;
    private float angle;
    // Use this for initialization
    void Start () {
        m_eventTrigger = transform.parent.GetComponent<EventTrigger>();
        m_lifeTime = GetComponent<LimitedLifetime>();
        angle = Random.Range(45f, 135f);
        direction = DegreeToVector2(angle);
        direction.Normalize();
	}
	
	// Update is called once per frame
	void Update () {
        active = m_eventTrigger.isTriggered;

        if (active)
        {
            transform.Translate(direction * Time.deltaTime * speed, Space.Self);
            m_lifeTime.enabled = true;
        }
    }
    public static Vector2 DegreeToVector2(float degree)
    {
        return RadianToVector2(degree * Mathf.Deg2Rad);
    }
    public static Vector2 RadianToVector2(float radian)
    {
        return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
    }
}
