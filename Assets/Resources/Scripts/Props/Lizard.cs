﻿using UnityEngine;
using System.Collections;

public class Lizard : MonoBehaviour {
    public float speed = 1f;
    private float angle = 1f;
    private float flipTimer;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        flipTimer += Time.deltaTime;

        Move();
        //Rotate();
        Flip();        
    }

    void Move()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    void Flip()
    {
        if (flipTimer >= 0.2f)
        {
            transform.Rotate(new Vector3(0, 0, 180f));
            flipTimer = 0;
        }

    }
    void Rotate()
    {
        angle = Mathf.Sin(Time.time * Mathf.PI);
        transform.Rotate(new Vector3(0, angle, 0));
    }
}
