﻿using UnityEngine;
using System.Collections;

public class ParticleOnMesh : MonoBehaviour {
    //Stickysoul -> This -> particleSystem
    private MeshRenderer parent;
    private GameObject child;
	// Use this for initialization
	void Start () {
        parent = transform.parent.GetComponent<MeshRenderer>();
        child = transform.GetChild(0).gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        if (parent.enabled == false)
            child.SetActive(false);
        else
        {
            child.SetActive(true);
        }
	}
}
