﻿using UnityEngine;
using System.Collections;

public class NeivaBlink : MonoBehaviour {
    public Material[] materials = new Material[2];
    private Material[] mats = new Material[3];
    public float blinkTime = 1f;
    public float timeInBlink = 0.2f;
    private float multiplier;
    private float blinkTimer;
    private float timeToBlink;
    private Renderer meshRenderer;
    private int blinkCounter;
	// Use this for initialization
	void Start () {
        meshRenderer = transform.GetChild(0).GetComponent<Renderer>();
        mats = new Material[meshRenderer.materials.Length];
        mats = meshRenderer.materials;
        multiplier = Random.Range(-1f, 1f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        blinkTimer += Time.deltaTime;
	    if(blinkTimer > blinkTime + multiplier)
        {
            Blink();
        }

    }

    void Blink()
    {
        mats[2] = materials[1];
        meshRenderer.materials = mats;
        timeToBlink += Time.deltaTime;
        
        if(timeToBlink > timeInBlink)
        {
            if(blinkCounter >= 5)
            {
                blinkCounter = 0;
            }
            else
            {
                blinkTimer = 0;
            }
            timeToBlink = 0;
            mats[2] = materials[0];
            meshRenderer.materials = mats;
            multiplier = Random.Range(-1f, 1f);
            blinkCounter += 1;
        }
        
    }
}
