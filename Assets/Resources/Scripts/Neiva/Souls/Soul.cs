﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

[RequireComponent(typeof(SphereCollider), typeof(Rigidbody))]
public class Soul : MonoBehaviour
{
	// constants
	public const float hideSpeed = 3f;
	public const float aimSpeed = 12f;

	// types
	public enum State
	{
		Idle, Aim, Throw, Return, Teleport, Cooldown, Hide
	}

	// variables
	public Teleportable host = null;
	[Space]
	public Vector3 idlePosition;
	public Vector3 aimPosition;
	public Vector3 hidePosition;
	[Space]
	public LayerMask noTeleportationLayer = 0;
	[Space]
	public float idleSpeed = 8f;
	[Space]
	public float throwSpeed = 10f;
	public float throwDuration = 1f;
	[Range(0f, 1f)]
	public float throwMiddleDuration = 1f;
	public float throwEndBreak = 4f;
	public float throwCollisionDelay = 0.05f;
	[Space]
	public float returnStartSpeed = 7f;
	public float returnAcceleration = 0.1f;
	public float returnCatchRadius = 0.5f;
	[Space]
	public bool enableTeleport = true;
	public float enableTeleportDelay = 0.1f;
	public float teleportCooldownTime = 0.4f;
    [Space]
    public GameObject destroyedParticle;
	// accessors
	public Vector3 aimInput { get; private set; }
	public bool throwInput { get; private set; }
	public bool teleportInput { get; private set; }
	public bool returnInput { get; private set; }
	public bool hideInput { get; private set; }
	public TStateMachine<Soul> stateMachine { get; private set; }
	public State currentState { get; set; }

	public SphereCollider sphereCollider { get; private set; }
	public new Rigidbody rigidbody { get; private set; }

	public UnityAction onThrow = null;

	// methods
	void Awake()
	{
		// get required components
		sphereCollider = GetComponent<SphereCollider>();
		rigidbody = GetComponent<Rigidbody>();

		// create state machine
		stateMachine = new TStateMachine<Soul>(this, new IdleState());
	}

	void OnEnable()
	{
		if (host != null)
			rigidbody.position = host.transform.position;
	}

	void Start()
	{
		// ignore host colliders
		Collider[] hostColliders = host.GetComponents<Collider>();
		if (hostColliders != null)
			foreach (Collider col in hostColliders)
				Physics.IgnoreCollision(col, sphereCollider);
		OnEnable();
	}

	void FixedUpdate()
	{
		// update state tree
		stateMachine.update(Time.fixedDeltaTime);

		returnInput = false;
	}

	public class IdleState : TState<Soul>
	{
		public override void init()
		{
			obj.currentState = State.Idle;
			obj.rigidbody.isKinematic = true;
			obj.rigidbody.detectCollisions = false;
		}
		public override TState<Soul> run()
		{
			obj.rigidbody.position =
				Vector3.Lerp(obj.rigidbody.position, obj.idlePosition, Time.fixedDeltaTime * obj.idleSpeed);
			return
				obj.hideInput ? new HideState() :
				obj.aimInput.magnitude != 0f ? new AimState() : 
				this as TState<Soul>;
		}
	}
	public class AimState : TState<Soul>
	{
		public override void init()
		{
			obj.currentState = State.Aim;
		}
		public override TState<Soul> run()
		{
			obj.rigidbody.position = Vector3.Lerp(obj.rigidbody.position, obj.aimPosition + (obj.host.transform.right * 0.5f), Time.fixedDeltaTime * aimSpeed);
			return
				obj.aimInput.magnitude == 0f || obj.hideInput ? new IdleState() :
				obj.throwInput ? new ThrowState() :
				this as TState<Soul>;
		}
	}
	public class ThrowState : TState<Soul>
	{
		TStateMachine<ThrowState> stateMachine;
		public override void init()
		{
			if (obj.onThrow != null)
				obj.onThrow();
			obj.currentState = State.Throw;
			obj.rigidbody.position = obj.aimPosition;
			stateMachine = new TStateMachine<ThrowState>(this, new Start());
		}
		public override TState<Soul> run()
		{
			stateMachine.update(Time.fixedDeltaTime);

			if (obj.teleportInput && obj.stateMachine.stateTime >= obj.enableTeleportDelay && obj.enableTeleport)
				if (obj.CanTeleport())
					return new TeleportState();
			return
				obj.returnInput || obj.hideInput ? new ReturnState() :
				obj.stateMachine.stateTime >= obj.throwDuration ? new ReturnState() :
				this as TState<Soul>;
		}
		class Start : TState<ThrowState>
		{
			public override void init()
			{
				obj.obj.rigidbody.isKinematic = false;
				obj.obj.rigidbody.detectCollisions = false;
				obj.obj.rigidbody.velocity = obj.obj.aimInput * obj.obj.throwSpeed;
			}
			public override TState<ThrowState> run()
			{
				obj.obj.rigidbody.detectCollisions = obj.obj.stateMachine.stateTime >= obj.obj.throwCollisionDelay;
				return obj.obj.rigidbody.detectCollisions ? new Middle() : this as TState<ThrowState>;
			}
		}
		class Middle : TState<ThrowState>
		{
			public override TState<ThrowState> run()
			{
				return obj.obj.stateMachine.stateTime >= obj.obj.throwDuration * obj.obj.throwMiddleDuration ? new Ending() : this as TState<ThrowState>;
			}
		}
		class Ending : TState<ThrowState>
		{
			public override TState<ThrowState> run()
			{
				obj.obj.rigidbody.velocity =
					Vector3.Lerp(obj.obj.rigidbody.velocity, Vector3.zero, Time.fixedDeltaTime * obj.obj.throwEndBreak);
				return this;
			}
		}
	}
	public class ReturnState : TState<Soul>
	{
		float speed;

		public override void init()
		{
			obj.currentState = State.Return;
			obj.rigidbody.isKinematic = true;
			obj.rigidbody.detectCollisions = false;
			speed = obj.returnStartSpeed;
		}
		public override TState<Soul> run()
		{
			Vector3 movement = (obj.aimPosition - obj.rigidbody.position).normalized * speed * Time.fixedDeltaTime;
			speed += obj.returnAcceleration * Time.fixedDeltaTime;

			if (obj.returnCatchRadius >= Vector3.Distance(obj.rigidbody.position, obj.aimPosition))
				return new IdleState();
			else {
				obj.rigidbody.position += movement;
				return this;
			}
		}
	}
	public class TeleportState : TState<Soul>
	{
		public override void init()
		{
			obj.currentState = State.Teleport;
		}
		public override TState<Soul> run()
		{
			Vector3 toPos = obj.rigidbody.position;  //(fulfix)
			toPos.z = obj.host.Rigidbody.position.z; //.

			obj.host.TeleportTo(toPos);
			return new CooldownState();
		}
	}
	public class CooldownState : TState<Soul>
	{
		public override void init()
		{
			obj.currentState = State.Cooldown;
            obj.destroyedParticle.SetActive(true);
            Instantiate(obj.destroyedParticle, obj.transform.position, obj.transform.rotation);
		}
		public override TState<Soul> run()
		{
			obj.rigidbody.position = obj.host.transform.position;
			return obj.stateMachine.stateTime >= obj.teleportCooldownTime ? new IdleState() : this as TState<Soul>;
		}
	}
	public class HideState : TState<Soul>
	{
		TStateMachine<HideState> stateMachine;
		public override void init()
		{
			obj.currentState = State.Hide;
			stateMachine = new TStateMachine<HideState>(this, new Hide());
		}
		public override TState<Soul> run()
		{
			stateMachine.update(Time.fixedDeltaTime);
			return obj.hideInput == false ? new IdleState() : this as TState<Soul>;
		}
		class Hide : TState<HideState>
		{
			Vector3 initialPosition;
			public override void init()
			{
				initialPosition = obj.obj.rigidbody.position;
			}
			public override TState<HideState> run()
			{
				obj.obj.rigidbody.position =
					Vector3.Lerp(
						initialPosition,
						obj.obj.hidePosition,
						obj.stateMachine.stateTime / 0.15f
						);
				return obj.stateMachine.stateTime >= 0.15f ? new Hidden() : this as TState<HideState>;
			}
		}
		class Hidden : TState<HideState>
		{
			public override TState<HideState> run()
			{
				obj.obj.rigidbody.position = obj.obj.hidePosition;
				return this;
			}
		}
	}

	// public methods
	public void Control(Vector3 aim, bool shoot, bool teleport)
	{
		aimInput = aim.normalized;
		throwInput = shoot;
		teleportInput = teleport;
	}

	public void CancelThrow()
	{
		returnInput = true;
	}

	public void SetHiding(bool condition)
	{
		hideInput = condition;
	}

	public bool CanTeleport()
	{
		return Physics.OverlapSphere(
			sphereCollider.bounds.center,
			sphereCollider.bounds.extents.x,
			noTeleportationLayer,
			QueryTriggerInteraction.Collide).Length == 0;
	}

#if UNITY_EDITOR
	void OnValidate()
	{
		idleSpeed = clamp0(idleSpeed);
		throwSpeed = clamp0(throwSpeed);
		throwDuration = clamp0(throwDuration);

		returnStartSpeed = clamp0(returnStartSpeed);
		returnAcceleration = clamp0(returnAcceleration);
		returnCatchRadius = clamp0(returnCatchRadius);
	}
	static float clamp0(float a) { return a < 0f ? 0f : a; }
#endif
}
