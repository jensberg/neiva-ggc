﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[RequireComponent(typeof(Soul))]
public class BouncySoul : MonoBehaviour
{
	// variables
	public bool regainSpeedOnBounce = true;
	[Space]
	public LayerMask forcedBounceAngleLayer = 0;
	public float forcedBounceAngle = 20f;
	[Header("On bounce:")]
	public float addThrowDuration = 0.3f;
	public float subtractAdd = 0.1f;

	float originalThrowDuration;
	float acc = 0f;

    public UnityAction onBounce = null;

	// accessors
	public Soul soul { get; private set; }

	// methods
	void Awake()
	{
		// get required components
		soul = GetComponent<Soul>();

		originalThrowDuration = soul.throwDuration;
	}

	void Update()
	{
		if (soul.currentState != Soul.State.Throw)
		{
			acc = addThrowDuration;
			soul.throwDuration = originalThrowDuration;
		}
	}

	void OnCollisionEnter(Collision coll)
	{
		if (soul.currentState == Soul.State.Throw)
		{
			soul.throwDuration += acc;
			acc -=subtractAdd;
			acc = acc < 0f ? 0f : acc;

			if (regainSpeedOnBounce)
				soul.rigidbody.velocity = soul.rigidbody.velocity.normalized * soul.throwSpeed;

			if (Physics.OverlapSphere(
				soul.sphereCollider.bounds.center,
				soul.sphereCollider.bounds.extents.x,
				forcedBounceAngleLayer,
				QueryTriggerInteraction.Collide).Length > 0)
			{
				Vector2 normal = coll.contacts[0].normal;
				Vector3 cross = Vector3.Cross(normal, soul.rigidbody.velocity);
				soul.rigidbody.velocity = Quaternion.Euler(0f, 0f, (90f - forcedBounceAngle) * (cross.z < 0f ? -1f : 1f)) * normal * soul.rigidbody.velocity.magnitude;
			}
            if(onBounce != null)
                onBounce.Invoke();
		}
	}
#if UNITY_EDITOR
	void OnValidate()
	{
		addThrowDuration = Mathf.Clamp(addThrowDuration, 0f, float.MaxValue);
		subtractAdd = Mathf.Clamp(subtractAdd, 0f, float.MaxValue);
		if (soul == null) soul = GetComponent<Soul>();
		originalThrowDuration = soul.throwDuration;
	}
#endif
}
