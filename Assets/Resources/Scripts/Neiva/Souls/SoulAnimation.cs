﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Soul))]
public class SoulAnimation : MonoBehaviour
{
	[Tooltip("Booleans: 'Idle', 'Aim', 'Throw', 'Teleport', 'Return', 'Cooldown' \nFloats: 'Lifetime'\nTriggers: 'Active'")]
	public Animator animator = null;
	[Header("Optional:")]
	public SoulThrowerController soulThrower = null;

	public Soul soul { get; private set; }

	bool isIdle;
	bool isAim;
	bool isThrow;
	bool isTeleport;
	bool isReturn;
	bool isCooldown;

	// methods
	void Awake()
	{
		// get required components
		soul = GetComponent<Soul>();
	}

	void Start()
	{
		// listen to state change
		soul.stateMachine.onChangeState += OnChangeState;
		if (soulThrower != null)
			soulThrower.onActivateSoul += OnActivate;
	}

	void OnDestroy()
	{
		// stop listen to state change
		soul.stateMachine.onChangeState -= OnChangeState;
		if (soulThrower != null)
			soulThrower.onActivateSoul -= OnActivate;
	}

	void Update()
	{
		if (soul.currentState == Soul.State.Throw)
			animator.SetFloat("Lifetime", soul.throwDuration - soul.stateMachine.stateTime);
	}

	void OnChangeState()
	{
		animator.SetBool("Idle", soul.currentState == Soul.State.Idle);
		animator.SetBool("Aim", soul.currentState == Soul.State.Aim);
		animator.SetBool("Throw", soul.currentState == Soul.State.Throw);
		animator.SetBool("Teleport", soul.currentState == Soul.State.Teleport);
		animator.SetBool("Return", soul.currentState == Soul.State.Return);
		animator.SetBool("Cooldown", soul.currentState == Soul.State.Cooldown);
		animator.SetBool("Hide", soul.currentState == Soul.State.Hide);
	}

	void OnActivate()
	{
		if (soulThrower.activeSoul == this.soul)
			animator.SetTrigger("Activate");
	}
}
