﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Soul))]
public class SwappySoul : MonoBehaviour
{
	// variables
	public string tagForSwappable = "TestForSwap";

	// accessors
	public Soul soul { get; private set; }

	// methods
	void Awake()
	{
		soul = GetComponent<Soul>();
	}

	void OnCollisionEnter(Collision coll)
	{
		if (soul.currentState == Soul.State.Throw)
		{
			if (coll.gameObject.tag != tagForSwappable)
				soul.CancelThrow();
			else {
				Collider maybeCollider = soul.host.GetComponent<Collider>();
				Vector3 hostPos = maybeCollider != null ? maybeCollider.bounds.center : soul.host.transform.position;
				Vector3 swappablePos = coll.collider.bounds.center;
				swappablePos.z = hostPos.z;
				soul.host.TeleportTo(swappablePos);
				coll.gameObject.GetComponent<Teleportable>().TeleportTo(hostPos);
				soul.stateMachine.setState(new Soul.CooldownState());
			}
		}
	}
}
