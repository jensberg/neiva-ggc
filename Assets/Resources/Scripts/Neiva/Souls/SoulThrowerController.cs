﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof(Teleportable))]
public class SoulThrowerController : MonoBehaviour
{
	// variables
	public Soul[] souls = new Soul[0];
	[Space]
	public Vector3 soulIdlePosition = Vector3.up;
	public Vector3 soulAimPosition = Vector3.right;
	public Vector3 soulHidePosition = Vector3.zero;
	[Space]
	public float cooldownTime = 0.5f;
	public UnityAction onActivateSoul = null;
	public UnityAction onThrow = null;

	// accessors
	public int selectedSoulIndex { get; private set; }
	public Soul activeSoul { get; private set; }

	public Teleportable teleportable { get; private set; }

	public int selectInput { get; private set; }
	public Vector3 aimInput { get; private set; }
	public bool throwInput { get; private set; }
	public bool teleportInput { get; private set; }
	public bool returnInput { get; private set; }

	// methods
	void Awake()
	{
		// get required components
		teleportable = GetComponent<Teleportable>();

		// set inital selected soul
		if (souls.Length > 0)
			for (int i = 0; i < souls.Length; i++)
				if (souls[i].gameObject.activeInHierarchy)
				{
					selectedSoulIndex = i;
					activeSoul = souls[i];
					break;
				}

		// listen to soul throw events
		foreach (Soul soul in souls)
			soul.onThrow += OnThrow;
	}

	void Start()
	{
		// pre-update to set soul positions
		FixedUpdate();
	}

	void OnDestroy()
	{
		// stop listen to soul throw events
		foreach (Soul soul in souls)
			soul.onThrow -= OnThrow;
	}

	void FixedUpdate()
	{
		foreach (Soul soul in souls)
			if (soul.gameObject.activeInHierarchy)
			{
				soul.hidePosition = transform.position + soulHidePosition;
				soul.idlePosition = transform.position + soulIdlePosition;
				soul.aimPosition = transform.position + soulAimPosition;
				soul.SetHiding(soul != activeSoul);
			}

		if (souls[selectedSoulIndex] == activeSoul)
			activeSoul.Control(aimInput, throwInput, teleportInput);
		else if (activeSoul.currentState == Soul.State.Idle)
			if (souls[selectedSoulIndex] != activeSoul)
			{
				activeSoul = souls[selectedSoulIndex];
				if (onActivateSoul != null)
					onActivateSoul();
			}

		// return soul
		if (returnInput || (souls[selectedSoulIndex] != activeSoul))
			activeSoul.CancelThrow();
	}

	public void Control(int selectSoul, Vector2 aim, bool throwSoul, bool teleportToSoul, bool returnSoul)
	{
		selectInput = selectSoul;
		aimInput = aim;
		throwInput = throwSoul;
		teleportInput = teleportToSoul;
		returnInput = returnSoul;

		if (selectSoul != selectedSoulIndex)
			SelectSoul(selectSoul);
	}

	public void CancelThrow()
	{
		activeSoul.CancelThrow();
	}

	public bool SelectSoul(int index)
	{
		try {
			// return false if the suggested soul is inactive in the scene
			if (!souls[index].gameObject.activeInHierarchy || activeSoul.currentState == Soul.State.Aim)
				return false;
			else {
				selectedSoulIndex = index;
				if (souls[index] != activeSoul)
					activeSoul.CancelThrow();
				return true;
			}
		}
		catch { Debug.LogError("SoulThrowerController.SelectSoul(index) is out of range! "); return false; }
	}

	void OnThrow()
	{
		if (onThrow != null)
			onThrow();
	}

#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(1f, 0f, 1f, 0.4f);
		Gizmos.DrawSphere(transform.position + soulAimPosition, 0.2f);
		Gizmos.color = new Color(1f, 0f, 1f, 0.6f);
		Gizmos.DrawWireSphere(transform.position + soulIdlePosition, 0.2f);
	}
#endif
}
