﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Soul))]
public class StickySoul : MonoBehaviour
{
	// variables
	public float stickTime = 3f;

	Transform objectStuckTo;
	Vector3 objectLastPosition;
	float originalThrowDuration;

	// accessors
	public bool isStuck { get; private set; }
	public Soul soul { get; private set; }

	// methods
	void Awake()
	{
		// get required components
		soul = GetComponent<Soul>();
		originalThrowDuration = soul.throwDuration;
	}

	void OnEnable()
	{
		isStuck = false;
	}

	void Update()
	{
		if (soul.currentState != Soul.State.Throw)
			soul.throwDuration = originalThrowDuration;

		if (isStuck)
		{
			if (soul.currentState != Soul.State.Throw)
				isStuck = false;
			else {
				soul.rigidbody.position += objectStuckTo.position - objectLastPosition;
				objectLastPosition = objectStuckTo.position;
			}
		}
	}

	void OnCollisionEnter(Collision coll)
	{
		if (soul.currentState == Soul.State.Throw)
		{
			soul.rigidbody.isKinematic = true;
			soul.rigidbody.detectCollisions = false;
			soul.rigidbody.position = coll.contacts[0].point + coll.contacts[0].normal * soul.sphereCollider.bounds.extents.x;
			objectStuckTo = coll.transform;
			objectLastPosition = objectStuckTo.position;
			soul.throwDuration = stickTime + soul.stateMachine.stateTime;
			isStuck = true;
		}
	}
#if UNITY_EDITOR
	void OnValidate()
	{
		stickTime = Mathf.Clamp(stickTime, 0f, float.MaxValue);
		if (soul == null) soul = GetComponent<Soul>();
		originalThrowDuration = soul.throwDuration;
	}
#endif
}
