﻿using UnityEngine;
using System.Collections;

public class BouncyFX : MonoBehaviour {

    AudioSource audioSource;
    BouncySoul soul;

    public GameObject particles;
    public AudioClip bounceClip;
	
    // Use this for initialization
	void Start ()
    {
       audioSource = GetComponent<AudioSource>();
       soul = GetComponent<BouncySoul>();

       soul.onBounce += OnBounce;
        if (particles == null)
            Debug.Log("Nope");
	}

    void OnDestroy()
    {
        soul.onBounce -= OnBounce;
    }
    void OnBounce()
    {
        particles.SetActive(false);
        audioSource.PlayOneShot(bounceClip);
        particles.SetActive(true);
    }
}
