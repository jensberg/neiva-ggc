﻿using UnityEngine;
using System.Collections;

public class NeivaParticleManager : MonoBehaviour {
    public GameObject ChargeParticles;
    public GameObject TeleportParticles;
    public GameObject ShrineParticles;
    public GameObject AltarParticles;
    [Space]
    private float chargeTimer;
    private float chargeTime = 0.2f;
    private bool isBack = true;

    private float shrineTimer;
    private float shrineTime = 15f;
    private bool onShrine;
	// Use this for initialization
	void Start () {
        ShrineParticles.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.Singleton.Neiva.selectedSoul.currentState == Soul.State.Throw)
        {

        }
        if (GameManager.Singleton.Neiva.selectedSoul.currentState == Soul.State.Aim)
        {
            isBack = true;
            ChargeParticles.SetActive(true);
        }
        else
        {
            chargeTimer += Time.deltaTime;
            if (chargeTimer > chargeTime)
            {
                chargeTimer = 0f;
                ChargeParticles.SetActive(false);
            }
        }

        if (GameManager.Singleton.Neiva.selectedSoul.currentState == Soul.State.Cooldown)
        {
            if(isBack)
            {
                Instantiate(TeleportParticles, GameManager.Singleton.Neiva.transform.position, Quaternion.identity);
                isBack = false;
            }
        }
        RunShrine();
    }

    private void RunShrine()
    {
        shrineTimer += Time.deltaTime;
        if (shrineTimer >= shrineTime)
        {
            ShrineParticles.SetActive(false);
            shrineTimer = 0;
        }
    }

    public void OnShrine()
    {
        ShrineParticles.SetActive(true);
        onShrine = true;
    }

    public void OnAltar()
    {
        AltarParticles.SetActive(true);
    }
}
