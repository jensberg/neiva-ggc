﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Health : MonoBehaviour {
    [HideInInspector]
    public enum DamageType { Undefined, Spike, Saw, Water, Projectile, FlyingEnemy };

    public int maxHealth = 3;
    private int currentHealth;
    public float Cooldown = 1f;
    private bool Invulnerable = false;
    private bool IsCouroutineRunning = false;
    
    public UnityEvent OnTakeDamage = null;
	public UnityEvent OnDeath = null;
    public UnityEvent OnRestore = null;

    public DamageType damageType { get; private set; }
    // debug
    public bool canPlayerDie = false;
    public int guiFontSize = 50;
    private bool isNoclip = false;
    public int Damage = 1;
    // Unity methods
	void Start () {
        Restore();
	}
    void Update()
    {
        if (Invulnerable && !IsCouroutineRunning)
        {
            StartCoroutine("GotHurt");
        }
        if (currentHealth <= 0)
        {
            if (OnDeath != null)
                OnDeath.Invoke();
        }
    }
    
    // class methods
    public void TakeDamage(DamageType type)
    {
        if(canPlayerDie)
        {
            if (!Invulnerable && currentHealth > 0 && !isNoclip)
            {
                currentHealth -= Damage;
                Invulnerable = true;
                damageType = type;
            }
        }
    }
    private IEnumerator GotHurt()
    {
        IsCouroutineRunning = true;
        if (OnTakeDamage != null)
        {
            OnTakeDamage.Invoke();
        }
        yield return new WaitForSeconds(Cooldown);
        Invulnerable = false;
        IsCouroutineRunning = false;
    }

    public void Restore()
    {
        currentHealth = maxHealth;
        if(OnRestore != null)
        {
            OnRestore.Invoke();
        }
    }
    public int GetHealth()
    {
        return currentHealth;
    }
    public int GetMaxHealth()
    {
        return maxHealth;
    }

    //PLACEHOLDER FOR NOCLIP
    public void SetNoclip(bool state)
    {
        isNoclip = state;
    }

}
