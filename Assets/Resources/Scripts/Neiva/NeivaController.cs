﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

[RequireComponent(typeof(Collider), typeof(Rigidbody), typeof(GroundChecker))]
[RequireComponent(typeof(MoveController), typeof(JumpController), typeof(Teleportable))]
[RequireComponent(typeof(Health), typeof(SoulThrowerController))]
public class NeivaController : MonoBehaviour
{
	// types
	public enum SoulType
	{
		Sticky,
		Bouncy,
		Swappy,
	}

	// variables
	public Transform throwFrom = null;
	[Space]
	public StickySoul stickySoul = null;
	public BouncySoul bouncySoul = null;
	public SwappySoul swappySoul = null;
	[Space]
	public bool canMoveWhileAiming = false;

    private int numSouls;
	private float throwCooldownTimer;
    private Vector3 immobilePosition;

    // accessors
    public bool isImmobilized { get; set; }
    public bool isDead { get { return health.GetHealth() <= 0; } }
    public float moveInput { get; private set; }
	public Vector2 aimInput { get; private set; }
    public bool levitateInput { get; private set; }
	public SoulType selectedSoulType { get; private set; }
	public Soul selectedSoul { get
		{
			return
				selectedSoulType == SoulType.Sticky ? soulThrowerController.souls[0] :
				selectedSoulType == SoulType.Bouncy ? soulThrowerController.souls[1] :
				selectedSoulType == SoulType.Swappy ? soulThrowerController.souls[2] :
				null;
		} }
	public bool soulIsHome { get
		{
            return
                selectedSoul.currentState == Soul.State.Idle ||
                selectedSoul.currentState == Soul.State.Aim;
		} }

	public new Collider collider { get; private set; }
	public new Rigidbody rigidbody { get; private set; }
	public GroundChecker groundChecker { get; private set; }
	public MoveController moveController { get; private set; }
	public JumpController jumpController { get; private set; }
	public Teleportable teleportable { get; private set; }
	public Health health { get; private set; }
	public SoulThrowerController soulThrowerController { get; private set; }
	public LedgeClimber ledgeClimber { get; private set; }

	// methods
	void Awake()
	{
		// get required components
		collider = GetComponent<Collider>();
		rigidbody = GetComponent<Rigidbody>();
		groundChecker = GetComponent<GroundChecker>();
		moveController = GetComponent<MoveController>();
		jumpController = GetComponent<JumpController>();
		teleportable = GetComponent<Teleportable>();
		health = GetComponent<Health>();
		soulThrowerController = GetComponent<SoulThrowerController>();
		ledgeClimber = GetComponent<LedgeClimber>();

		// listen to events
		ledgeClimber.onClimbEnter += OnEnterLedgeClimb;
	}

	void OnDestroy()
	{
		// stop listen to events
		ledgeClimber.onClimbEnter -= OnEnterLedgeClimb;
	}

	void Update()
	{
        if (!Noclip.active)
        {
            // resetting after grappled
            jumpController.enabled = true;
            moveController.enabled = true;
            rigidbody.useGravity = true;
            rigidbody.isKinematic = false;
        }
    }

	public void Control(float move, bool jump, SoulType selectSoul, Vector2 aim, bool throwSoul, bool teleport, bool cancelThrow, bool levitate)
	{
		moveInput = Mathf.Clamp(move, -1f, 1f);
		aimInput = aim.normalized;

		// if not dead
		if (health.GetHealth() > 0 && !teleportable.isTeleporting)
		{
			// move
			if (aimInput.magnitude == 0f || (aimInput.magnitude > 0f && canMoveWhileAiming) || !groundChecker.isGrounded)
				moveController.Move(Vector3.right * move);
			else
				moveController.Move(Vector3.zero);

			// jump
			if (jump && !ledgeClimber.isClimbing) {
				if (aimInput.magnitude == 0f || (aimInput.magnitude > 0f && canMoveWhileAiming))
					jumpController.JumpOneShot();
			}
			else
				jumpController.StopJump();

			// throw
			int soulIndex =
				selectSoul == SoulType.Sticky ? 0 :
				selectSoul == SoulType.Bouncy ? 1 :
				selectSoul == SoulType.Swappy ? 2 :
				0;
			soulThrowerController.Control(soulIndex, aimInput, throwSoul, teleport, cancelThrow);
			soulThrowerController.soulAimPosition = throwFrom.position - transform.position;

			selectedSoulType =
				soulThrowerController.selectedSoulIndex == 0 ? SoulType.Sticky :
				soulThrowerController.selectedSoulIndex == 1 ? SoulType.Bouncy :
				soulThrowerController.selectedSoulIndex == 2 ? SoulType.Swappy :
				selectedSoulType;

			// cancel throw
			if (cancelThrow)
				soulThrowerController.activeSoul.CancelThrow();

            if(jumpController.currentState == JumpController.State.Falling)
				levitateInput = levitate;

			// extra nicenysess
			if (moveInput < 0f || aimInput.x < 0f)
				soulThrowerController.soulIdlePosition.x = Mathf.Abs(soulThrowerController.soulIdlePosition.x);
			else if (moveInput > 0f || aimInput.x > 0f)
				soulThrowerController.soulIdlePosition.x = -Mathf.Abs(soulThrowerController.soulIdlePosition.x);

			// climb ledges
			ledgeClimber.Control(ledgeClimber.isClimbing || (jump && jumpController.stateTime >= 0.1f && (rigidbody.velocity.y >= 0f || jumpController.isJumping)));
		}
		else
		{
			moveController.Move(Vector3.zero);
			jumpController.StopJump();
		}
	}

    public void AddSouls(int value)
    {
        numSouls += value;
    }

    public int GetSouls()
    {
        return numSouls;
    }

    public void RemoveSouls()
    {
       
        StartCoroutine(RemoveSoul());
    }

    IEnumerator RemoveSoul()
    {
        while(numSouls > 0)
        {
                numSouls--;
            yield return new WaitForSeconds(1f / numSouls);

        }
    }

	public void OnEnterLedgeClimb()
	{
		jumpController.StopJump();
	}
}

