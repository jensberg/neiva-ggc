﻿using UnityEngine;
using System;
using System.Linq;

[RequireComponent(typeof(NeivaController))]
public class NeivaUserControl : MonoBehaviour
{
	// types
	public enum InputType
	{
		MouseKeyboard, XboxController
	}
	public enum XboxButton
	{
		None,
		A, B, X, Y,
		LB, RB,
		BACK, START,
		LS, RS,
		LT, RT,
		DPAD_UP, DPAD_DOWN, DPAD_LEFT, DPAD_RIGHT,
	}
	public enum XboxAxis
	{
		LS, RS,
		LT_RT,
		DPAD,
	}
	public enum MouseButton
	{
		Left, Middle, Right
	}
	[System.Serializable]
	public struct XboxButtonDuple
	{
		public XboxButton primary;
		public XboxButton secondary;
	}

	// variables
	public InputType m_inputType = InputType.MouseKeyboard;
	[Header("Mouse & Keyboard")]
	public KeyCode m_keyboardMoveLeft = KeyCode.A;
	public KeyCode m_keyboardMoveRight = KeyCode.D;
	public KeyCode m_keyboardJump = KeyCode.Space;
	public KeyCode m_keyboardSelectsticky = KeyCode.Alpha1;
	public KeyCode m_keyboardSelectbouncy = KeyCode.Alpha2;
	public KeyCode m_keyboardSelectswap = KeyCode.Alpha3;
	public MouseButton m_mouseShoot = MouseButton.Left;
	public MouseButton m_mouseTeleport = MouseButton.Right;
	public MouseButton m_mouseCancelThrow = MouseButton.Middle;
	[Header("Xbox controller")]
	public XboxAxis xboxMove = XboxAxis.LS;
	public XboxAxis xboxAim = XboxAxis.RS;
	public XboxButtonDuple xboxJump = new XboxButtonDuple { primary = XboxButton.A, secondary = XboxButton.LB };
	public XboxButtonDuple xboxThrow = new XboxButtonDuple { primary = XboxButton.RB, secondary = XboxButton.RT };
	public XboxButtonDuple xboxTeleport = new XboxButtonDuple { primary = XboxButton.RB, secondary = XboxButton.RT };
	public XboxButtonDuple xboxCancelThrow = new XboxButtonDuple { primary = XboxButton.RS, secondary = XboxButton.None };
	public XboxButtonDuple xboxSelectSticky = new XboxButtonDuple { primary = XboxButton.X, secondary = XboxButton.DPAD_LEFT };
	public XboxButtonDuple xboxSelectBouncy = new XboxButtonDuple { primary = XboxButton.B, secondary = XboxButton.DPAD_RIGHT };
	public XboxButtonDuple xboxSelectSwappy = new XboxButtonDuple { primary = XboxButton.Y, secondary = XboxButton.DPAD_UP };
	[Space]
	[Range(0f, 1f)] public float xboxLeftStickDeadzone = 0.3f;
	[Range(0f, 1f)] public float xboxRightStickDeadzone = 0.3f;

	public static float leftStickDeadzone = 0.3f;
	public static float rightStickDeadzone = 0.3f;

	// accessors

	public NeivaController NeivaController { get; private set; }
	public float MoveInput { get; private set; }
	public bool JumpInput { get; private set; }
	public NeivaController.SoulType SelectSoulInput { get; private set; }
	public Vector2 AimInput { get; private set; }
	public bool ShootInput { get; private set; }
	public bool TeleportInput { get; private set; }
	public bool CancelThrowInput { get; private set; }
    public bool LevitateInput { get; private set; }
	// methods

	void Awake()
	{
		NeivaController = GetComponent<NeivaController>();
	}

	void Update()
	{
		if (m_inputType == InputType.MouseKeyboard)
		{
			MouseKeyboard();
			if (anyXboxInput())
				m_inputType = InputType.XboxController;
		}
		else if (m_inputType == InputType.XboxController)
		{
			XboxController();
			if ((Input.anyKey && !anyXboxInput()) || (Input.GetAxis("Mouse X") != 0f || Input.GetAxis("Mouse Y") != 0f))
				m_inputType = InputType.MouseKeyboard;
		}

		NeivaController.Control(
			MoveInput, 
			JumpInput,
			SelectSoulInput,
			AimInput,
			ShootInput, 
			TeleportInput,
			CancelThrowInput,
            LevitateInput
			);
	}

	void MouseKeyboard()
	{
		// get input
		MoveInput = Input.GetKey(m_keyboardMoveLeft) ? -1f : Input.GetKey(m_keyboardMoveRight) ? 1f : 0f;
		JumpInput = NeivaController.jumpController.isJumping || NeivaController.ledgeClimber.isClimbing ?
			Input.GetKey(m_keyboardJump) :
			Input.GetKeyDown(m_keyboardJump);
		SelectSoulInput =
			Input.GetKeyDown(m_keyboardSelectsticky) ? NeivaController.SoulType.Sticky :
			Input.GetKeyDown(m_keyboardSelectbouncy) ? NeivaController.SoulType.Bouncy :
			Input.GetKeyDown(m_keyboardSelectswap) ? NeivaController.SoulType.Swappy :
			SelectSoulInput;
		AimInput = Input.GetMouseButton(0) || Input.GetMouseButtonUp(0) ? MouseAim() : Vector2.zero;
		ShootInput = Input.GetMouseButtonUp(0);
		TeleportInput = Input.GetMouseButtonDown(0);
		CancelThrowInput = Input.GetMouseButtonDown(1);
	}

	void XboxController()
	{
		// get input
		MoveInput = GetXboxAxis(xboxMove).x;
		JumpInput = NeivaController.jumpController.isJumping || NeivaController.ledgeClimber.isClimbing ?
			GetXboxButtonDuple(xboxJump) :
			GetXboxButtonDupleDown(xboxJump);
		SelectSoulInput =
			GetXboxButtonDupleDown(xboxSelectSticky) ? NeivaController.SoulType.Sticky :
			GetXboxButtonDupleDown(xboxSelectBouncy) ? NeivaController.SoulType.Bouncy :
			GetXboxButtonDupleDown(xboxSelectSwappy) ? NeivaController.SoulType.Swappy :
			SelectSoulInput;
		AimInput = GetXboxAxis(xboxAim);
		ShootInput = GetXboxButtonDupleDown(xboxThrow);
		TeleportInput = GetXboxButtonDupleDown(xboxTeleport);
		CancelThrowInput = GetXboxButtonDupleDown(xboxCancelThrow);
        LevitateInput = GetXboxButtonDuple(xboxJump);
	}

	Vector2 MouseAim()
	{
		Ray rey = Camera.main.ScreenPointToRay(Input.mousePosition);
		float t = -Camera.main.transform.position.z / rey.direction.z;
		
		float x = Camera.main.transform.position.x + rey.direction.x * t;
		float y = Camera.main.transform.position.y + rey.direction.y * t;

		return new Vector2(x, y) - (Vector2)NeivaController.transform.position;
	}

#if UNITY_EDITOR
	void OnValidate()
	{
		leftStickDeadzone = xboxLeftStickDeadzone;
		rightStickDeadzone = xboxRightStickDeadzone;
	}
#endif

	// static

	public static bool GetXboxButtonMultiple(XboxButton[] buttons)
	{
		foreach (XboxButton button in buttons)
			if (GetXboxButton(button))
				return true;
		return false;
	}

	public static bool GetXboxButtonDownMultiple(XboxButton[] buttons)
	{
		foreach (XboxButton button in buttons)
			if (GetXboxButtonDown(button))
				return true;
		return false;
	}

	public static bool GetXboxButton(XboxButton button)
	{
		switch (button)
		{
			case XboxButton.A: return Input.GetKey(KeyCode.JoystickButton0);
			case XboxButton.B: return Input.GetKey(KeyCode.JoystickButton1);
			case XboxButton.X: return Input.GetKey(KeyCode.JoystickButton2);
			case XboxButton.Y: return Input.GetKey(KeyCode.JoystickButton3);
			case XboxButton.LB: return Input.GetKey(KeyCode.JoystickButton4);
			case XboxButton.RB: return Input.GetKey(KeyCode.JoystickButton5);
			case XboxButton.BACK: return Input.GetKey(KeyCode.JoystickButton6);
			case XboxButton.START: return Input.GetKey(KeyCode.JoystickButton7);
			case XboxButton.LS: return Input.GetKey(KeyCode.JoystickButton8);
			case XboxButton.RS: return Input.GetKey(KeyCode.JoystickButton9);
			case XboxButton.LT: return Input.GetAxis("LT RT") < -0.8f;
			case XboxButton.RT: return Input.GetAxis("LT RT") > 0.8f;
			case XboxButton.DPAD_UP: return Input.GetAxis("DPAD Y") > +0.8f;
			case XboxButton.DPAD_DOWN: return Input.GetAxis("DPAD Y") < -0.8f;
			case XboxButton.DPAD_LEFT: return Input.GetAxis("DPAD X") < -0.8f;
			case XboxButton.DPAD_RIGHT: return Input.GetAxis("DPAD X") > +0.8f;
			default: return false;
		}
	}

	public static bool GetXboxButtonDown(XboxButton button)
	{
		switch (button)
		{
			case XboxButton.A: return Input.GetKeyDown(KeyCode.JoystickButton0);
			case XboxButton.B: return Input.GetKeyDown(KeyCode.JoystickButton1);
			case XboxButton.X: return Input.GetKeyDown(KeyCode.JoystickButton2);
			case XboxButton.Y: return Input.GetKeyDown(KeyCode.JoystickButton3);
			case XboxButton.LB: return Input.GetKeyDown(KeyCode.JoystickButton4);
			case XboxButton.RB: return Input.GetKeyDown(KeyCode.JoystickButton5);
			case XboxButton.BACK: return Input.GetKeyDown(KeyCode.JoystickButton6);
			case XboxButton.START: return Input.GetKeyDown(KeyCode.JoystickButton7);
			case XboxButton.LS: return Input.GetKeyDown(KeyCode.JoystickButton8);
			case XboxButton.RS: return Input.GetKeyDown(KeyCode.JoystickButton9);
			case XboxButton.LT: return Input.GetAxis("LT RT") < -0.8f;
			case XboxButton.RT: return Input.GetAxis("LT RT") > 0.8f;
			case XboxButton.DPAD_UP: return Input.GetAxis("DPAD Y") > +0.8f; // TODO: axis click
			case XboxButton.DPAD_DOWN: return Input.GetAxis("DPAD Y") < -0.8f;
			case XboxButton.DPAD_LEFT: return Input.GetAxis("DPAD X") < -0.8f;
			case XboxButton.DPAD_RIGHT: return Input.GetAxis("DPAD X") > +0.8f;
			default: return false;
		}
	}

	public static bool GetXboxButtonDuple(XboxButtonDuple bb)
	{
		return GetXboxButton(bb.primary) || GetXboxButton(bb.secondary);
	}

	public static bool GetXboxButtonDupleDown(XboxButtonDuple bb)
	{
		return GetXboxButtonDown(bb.primary) || GetXboxButtonDown(bb.secondary);
	}

	public static Vector2 GetXboxAxis(XboxAxis axis)
	{
		switch (axis)
		{
			case XboxAxis.LS:
				Vector2 ls = new Vector2(Input.GetAxis("LS X"), Input.GetAxis("LS Y"));
				return ls.magnitude > leftStickDeadzone ? ls : Vector2.zero;
			case XboxAxis.RS:
				Vector2 rs = new Vector2(Input.GetAxis("RS X"), Input.GetAxis("RS Y"));
				return rs.magnitude > rightStickDeadzone ? rs : Vector2.zero;
			case XboxAxis.LT_RT: return new Vector2(Input.GetAxis("LT RT"), 0f);
			case XboxAxis.DPAD: return new Vector2(Input.GetAxis("DPAD X"), Input.GetAxis("DPAD Y"));
			default: return Vector2.zero;
		}
	}

	public static KeyCode GetCurrentKeyDown()
	{
		foreach (KeyCode k in Enum.GetValues(typeof(KeyCode)))
			if (Input.GetKeyDown(k)) return k;
		return KeyCode.None;
	}

	public static bool anyXboxInput()
	{
		foreach (XboxButton b in Enum.GetValues(typeof(XboxButton)))
			if (GetXboxButton(b)) return true;
		foreach (XboxAxis a in Enum.GetValues(typeof(XboxAxis)))
			if (GetXboxAxis(a).magnitude > 0.5f) return true;
		return false;
	}
}
