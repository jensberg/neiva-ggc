﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System.Collections.Generic;

/*
	Script:		NeivaAudio
	Syfte:		Triggar och hanterar ljudeffekter som kommer från Neiva (NeivaController)
*/

[RequireComponent(typeof(NeivaController), typeof(AudioSource))]
public class NeivaAudio : MonoBehaviour
{
    [System.Serializable]
    public struct MoveEffects
    {
        public string name;
        public AudioClip clip;
    }

    // inspector variables
    [Header("Damage FX:")]
    public AudioClip[] sharpClip;
    public AudioClip[] liquidClip;
    public AudioClip[] projectileClip;

    [Header("Movement FX:")]
    public MoveEffects[] jumpClip;
    public MoveEffects[] landClip;
    public MoveEffects[] footstepsClip;

    [Header("Teleportation FX:")]
    public AudioClip teleportationClip;
    public AudioClip flingClip1;
    public AudioClip flingClip2;
    [Space]
    public AudioSource noPitchSource;

    // private variables
    AudioSource m_audioSource;
    NeivaController m_neiva;
    float m_levitateTimer;
    int lastPlayed;

	void Awake()
	{
		m_neiva = GetComponent<NeivaController>();
		m_audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        // listen to events from NeivaController
        m_neiva.jumpController.onJump += OnJump;
        m_neiva.groundChecker.onGroundEnter += OnGroundEnter;
        m_neiva.health.OnTakeDamage.AddListener(TakeDamage); //INKONSEKVENT
        m_neiva.teleportable.onTeleport += OnNeivaTeleport;
        m_neiva.soulThrowerController.activeSoul.stateMachine.onChangeState += OnThrow;
    }

    void OnDestroy()
    {
        m_neiva.jumpController.onJump -= OnJump;
        m_neiva.groundChecker.onGroundEnter -= OnGroundEnter;
        m_neiva.health.OnTakeDamage.RemoveListener(TakeDamage);
        m_neiva.teleportable.onTeleport -= OnNeivaTeleport;
    }
    void OnThrow()
    {
        if (m_neiva.soulThrowerController.activeSoul.currentState == Soul.State.Throw)
        {
            int i = Random.Range(0, 1);
            AudioClip c = i == 1 ? flingClip1 : flingClip2;
            noPitchSource.PlayOneShot(c);
        }
            
    }
    void OnJump()
    {
        if (jumpClip.Length != 0)
        {
            m_audioSource.pitch = Random.Range(0.7f, 1.1f);
            m_audioSource.PlayOneShot(jumpClip[0].clip, Random.Range(0.3f, 0.4f));
        }
        else
            Debug.Log("Jump audio clip missing in NeivaAudio.");
    }
    
    void OnGroundEnter()
    {
        if (landClip.Length != 0)
        {
            m_audioSource.pitch = Random.Range(0.7f, 1.1f);
            foreach (MoveEffects m in landClip)
            {
                if (GetGroundMaterial() == landClip[0].name)
                    m_audioSource.PlayOneShot(landClip[0].clip,0.2f);
                else if (GetGroundMaterial() == landClip[1].name)
                    m_audioSource.PlayOneShot(landClip[1].clip, 0.2f);
                else
                    m_audioSource.PlayOneShot(landClip[0].clip, 0.2f);
            }
        }
        else
            Debug.Log("Land audio clip missing in NeivaAudio.");
    }
    
    void TakeDamage()
    {
        Health.DamageType type = m_neiva.health.damageType;
        AudioClip clip = null;
        
        if (type == Health.DamageType.Saw || type == Health.DamageType.Spike)
        {
            m_audioSource.pitch = Random.Range(0.85f, 1f);
            int i = Random.Range(0, sharpClip.Length);

            if (sharpClip.Length != 0)
                clip = sharpClip[i];
            else
                Debug.Log("Damage Sharp Audio Clip missing in inspector.");
        }

        else if(type == Health.DamageType.Water)
        {
            m_audioSource.pitch = 1f;
            if (liquidClip.Length != 0)
                clip = liquidClip[0];
            else
                Debug.Log("Damage Liquid Audio Clip missing in inspector.");
        }

        else if(type == Health.DamageType.Projectile)
        {
            m_audioSource.pitch = 1f;
            if (projectileClip.Length != 0)
                clip = projectileClip[0];
            else
                Debug.Log("Damage Projectile Audio Clip missing in inspector.");
        }
        
        switch (type)
        {
            case Health.DamageType.Saw:
                m_audioSource.PlayOneShot(clip);
                break;

            case Health.DamageType.Spike:
                m_audioSource.PlayOneShot(clip);
                break;

            case Health.DamageType.Water:
                m_audioSource.PlayOneShot(clip, 0.5f);
                break;

            case Health.DamageType.Projectile:
                m_audioSource.PlayOneShot(clip);
                break;
            case Health.DamageType.FlyingEnemy:
                break;
            case Health.DamageType.Undefined:
                break;
            default:
                break;

        }
    }
     
    public void PlayFootstep()
    {
        if(m_neiva.groundChecker.isGrounded && footstepsClip.Length != 0)
        {
            List<AudioClip> list = new List<AudioClip>();
            foreach (MoveEffects m in footstepsClip)
            {
                if (m.name == GetGroundMaterial())
                    list.Add(m.clip);
                else if(GetGroundMaterial() == null && m.name == "Rock")
                    list.Add(m.clip);
            }

            if(list.Count != 0)
            {
                int i = Random.Range(0, list.Count - 1);
                if(lastPlayed == i)
                    i = Random.Range(0, list.Count - 1);
                m_audioSource.pitch = 1;
                m_audioSource.PlayOneShot(list[i], 0.3f);
                lastPlayed = i;
            }
        }
    }

    void OnNeivaTeleport(Vector3 position, Vector3 arg2)
    {
        if (noPitchSource != null)
        {
            noPitchSource.pitch = Random.Range(0.97f, 1.03f);
            noPitchSource.PlayOneShot(teleportationClip);
            
        }
    }
    
    string GetGroundMaterial()
    {
        if (m_neiva.groundChecker.ground.sharedMaterial != null)
            return m_neiva.groundChecker.ground.sharedMaterial.name;
        else
            return null;
    } 
}
