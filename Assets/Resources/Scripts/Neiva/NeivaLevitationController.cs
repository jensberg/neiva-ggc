﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof(NeivaController))]
public class NeivaLevitationController : MonoBehaviour
{
	public float duration = 2f;
	public int maxUseCount = 5;
	public float firstDescendSpeed = 1f;
	public float lastDescendSpeed = 5f;
	public float horizontalSpeed = 0.5f;
	[Space]
	public bool disableAfterTeleport = false;
	public bool mustResetAimEnabled = true;

	float timer = 0f;
	int numTimesUsed = 0;
	Vector3 velocity = Vector3.zero;
	Vector3 initialVelocity = Vector3.zero;
	bool mustResetAim = false;
    [SerializeField]
    bool HoldToLevitate = false;

	public bool isLevitating { get; set; }
	public bool canLevitate { get { return timer < duration && !NeivaController.groundChecker.isGrounded && numTimesUsed < maxUseCount && !mustResetAim; } }
	public NeivaController NeivaController { get; private set; }

	public UnityAction onLevitateStart = null;
	public UnityAction onLevitateUpdate = null;
	public UnityAction onLevitateEnd = null;

	void Awake()
	{
		NeivaController = GetComponent<NeivaController>();
	}
	
	void Start()
	{
		NeivaController.teleportable.onTeleport += OnTeleport;
		NeivaController.groundChecker.onGroundExit += OnGroundExit;
		onLevitateStart += OnLevitateStart;
		onLevitateUpdate += OnLevitateUpdate;
		onLevitateEnd += OnLevitateEnd;
	}

	void OnDestroy()
	{
		NeivaController.teleportable.onTeleport -= OnTeleport;
		NeivaController.groundChecker.onGroundExit -= OnGroundExit;
		onLevitateStart -= OnLevitateStart;
		onLevitateUpdate -= OnLevitateUpdate;
		onLevitateEnd -= OnLevitateEnd;
	}

	void OnTeleport(Vector3 from, Vector3 to)
	{
		if (disableAfterTeleport)
			timer = duration;
	}

	void OnGroundExit()
	{
		timer = 0f;
		numTimesUsed = 0;
		mustResetAim = false;

		// player must reset the aim input if player is aiming while falling off a ledge
		if (mustResetAimEnabled)
			if (NeivaController.aimInput.magnitude > 0.9f)
				mustResetAim = true;
	}
	
	void OnLevitateStart()
	{
		isLevitating = true;
		numTimesUsed += 1;
		initialVelocity = NeivaController.rigidbody.velocity;
		velocity = Vector3.down * Mathf.Lerp(firstDescendSpeed, lastDescendSpeed, numTimesUsed == 1 ? 0f : (float)numTimesUsed / (float)maxUseCount) + Vector3.right * initialVelocity.x * horizontalSpeed;
	}

	void OnLevitateUpdate()
	{
		timer += Time.fixedDeltaTime;

		NeivaController.rigidbody.velocity = velocity;
	}

	void OnLevitateEnd()
	{
		isLevitating = false;
		initialVelocity.y = Mathf.Clamp(initialVelocity.y, 0f, float.MaxValue);
		NeivaController.rigidbody.velocity = initialVelocity;
	}

	void FixedUpdate()
	{
        
		if (mustResetAim)
			mustResetAim = NeivaController.aimInput.magnitude < 0.9f ? false : true;
        
		if (canLevitate)
		{
            bool levitate;
            if (!HoldToLevitate)
                levitate =
                    NeivaController.aimInput.magnitude != 0f ? true : false;
            else
                levitate = NeivaController.levitateInput;

			if (levitate && !isLevitating)
                onLevitateStart.Invoke();
            else if (levitate && isLevitating)
				onLevitateUpdate.Invoke();
			else if (!levitate && isLevitating)
				onLevitateEnd.Invoke();
			else
				isLevitating = false;
		}
		else isLevitating = false;
	}
}
