﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NeivaController), typeof(Animator))]
public class NeivaAnimation : MonoBehaviour
{
	public float randomIdleTimeInterval = 5f;
	public string[] randomIdleNames = new string[] { "Idle1", "Idle2", "Idle3" };

	private float idleTimer = 0f;
	private bool isIdling = false;

	public NeivaController Neiva { get; private set; }
	public Animator Animator { get; private set; }

	void Awake()
	{
		Neiva = GetComponent<NeivaController>();
		Animator = GetComponent<Animator>();
    }
    void Start()
    {
		Neiva.soulThrowerController.activeSoul.onThrow += OnThrow;
		Neiva.teleportable.onTeleport += OnTeleport;
		Neiva.jumpController.onJump += OnJump;
		Neiva.health.OnTakeDamage.AddListener(OnDamage);
		Neiva.health.OnDeath.AddListener(OnDeath);
		Neiva.ledgeClimber.onClimbEnter += OnClimb;
	}

	void OnDestroy()
	{
		Neiva.jumpController.onJump -= OnJump;
		Neiva.soulThrowerController.activeSoul.onThrow -= OnThrow;
		Neiva.teleportable.onTeleport -= OnTeleport;
		Neiva.health.OnTakeDamage.RemoveListener(OnDamage);
		Neiva.health.OnDeath.RemoveListener(OnDeath);
		Neiva.ledgeClimber.onClimbEnter -= OnClimb;
	}

	void Update()
	{
		Animator.SetBool("Grounded", Neiva.groundChecker.isGrounded);
		Animator.SetFloat("VelocityX", Neiva.moveInput);
		Animator.SetFloat("VelocityY", Neiva.rigidbody.velocity.y);
		Animator.SetFloat("AimX", Neiva.aimInput.x);
		Animator.SetFloat("AimY", Neiva.aimInput.y);
        Animator.SetBool("SoulIsHome", Neiva.soulIsHome);
        Animator.SetBool("Immobilized", Neiva.isImmobilized);
        Animator.SetBool("Death", Neiva.isDead);
        Animator.SetFloat("MoveSpeed", Neiva.rigidbody.velocity.x);

		// handle random idle animations
		bool wasIdling = isIdling;
		isIdling = Neiva.moveInput == 0f && Neiva.groundChecker.isGrounded && !Neiva.isDead && Neiva.aimInput.magnitude == 0f;
		if (!wasIdling)
			idleTimer = 0f;
		if (isIdling) {
			idleTimer += Time.deltaTime;
			if (idleTimer >= randomIdleTimeInterval) {
				idleTimer = 0f;
				Animator.SetTrigger(randomIdleNames[Random.Range(0, randomIdleNames.Length)]);
			}
		}

		// flip transform
		Flip();
	}

	void OnJump()
	{
		Animator.SetTrigger("Jump");
	}

	void OnThrow()
	{
		Animator.SetTrigger("Throw");
	}

	void OnTeleport(Vector3 from, Vector3 to)
	{
		Animator.SetTrigger("Teleport");
	}

	void OnDamage()
	{
		Animator.SetTrigger("Damage");
	}

	void OnDeath()
	{
		Animator.SetTrigger("Death");
	}

	void OnClimb()
	{
		Animator.SetTrigger("Climb");
	}

    void Flip()
    {
        if (Neiva.aimInput.x < 0)
        {
            Flip(-180f);
        }
        else if(Neiva.aimInput.x > 0)
        {
            Flip(0f);
        }
        else if (Neiva.moveInput < 0)
        {
            Flip(-180f);
        }
        else if (Neiva.moveInput > 0)
        {
            Flip(0f);
        }
    }

    private void Flip(float angle)
    {
        Neiva.transform.rotation = Quaternion.Euler(Neiva.transform.rotation.x, angle, Neiva.transform.rotation.z);
    }

#if UNITY_EDITOR
	void OnValidate()
	{
		randomIdleTimeInterval = randomIdleTimeInterval < 0f ? 0f : randomIdleTimeInterval;
	}
#endif
}
