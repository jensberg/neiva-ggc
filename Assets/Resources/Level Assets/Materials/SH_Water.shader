// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2865,x:32719,y:32712,varname:node_2865,prsc:2|diff-5720-OUT,spec-7468-OUT,gloss-6449-OUT,normal-4763-OUT,transm-1822-OUT,lwrap-1822-OUT,alpha-5675-OUT,refract-5295-OUT,voffset-4271-OUT;n:type:ShaderForge.SFN_Color,id:6665,x:30823,y:32586,ptovrint:False,ptlb:Color A,ptin:_ColorA,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5019608,c2:0.5019608,c3:0.5019608,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5964,x:31207,y:32067,ptovrint:True,ptlb:Normal Map A,ptin:_BumpMap,varname:_BumpMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True|UVIN-305-UVOUT;n:type:ShaderForge.SFN_DepthBlend,id:5675,x:31082,y:33869,varname:node_5675,prsc:2|DIST-4472-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4472,x:30859,y:33903,ptovrint:False,ptlb:Water Depth,ptin:_WaterDepth,varname:node_4472,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:50;n:type:ShaderForge.SFN_Color,id:9478,x:30823,y:32772,ptovrint:False,ptlb:Color B,ptin:_ColorB,varname:node_9478,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:8425,x:31077,y:32666,varname:node_8425,prsc:2|A-6665-RGB,B-9478-RGB,T-5675-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:874,x:30553,y:33591,varname:node_874,prsc:2|IN-5675-OUT,IMIN-1889-OUT,IMAX-7046-OUT,OMIN-7319-OUT,OMAX-5282-OUT;n:type:ShaderForge.SFN_Slider,id:1889,x:30180,y:33602,ptovrint:False,ptlb:FoamInMin,ptin:_FoamInMin,varname:node_1889,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:7046,x:30180,y:33706,ptovrint:False,ptlb:FoamInMax,ptin:_FoamInMax,varname:node_7046,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Vector1,id:7319,x:30371,y:33831,varname:node_7319,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:5282,x:30433,y:33910,varname:node_5282,prsc:2,v1:1;n:type:ShaderForge.SFN_Tex2d,id:8156,x:30521,y:33423,ptovrint:False,ptlb:Foam Map,ptin:_FoamMap,varname:node_8156,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-8726-UVOUT;n:type:ShaderForge.SFN_Add,id:5895,x:30941,y:33241,varname:node_5895,prsc:2|A-8425-OUT,B-2811-OUT;n:type:ShaderForge.SFN_Clamp01,id:5720,x:31203,y:33247,varname:node_5720,prsc:2|IN-5895-OUT;n:type:ShaderForge.SFN_OneMinus,id:7977,x:30706,y:33591,varname:node_7977,prsc:2|IN-874-OUT;n:type:ShaderForge.SFN_Multiply,id:1447,x:30744,y:33423,varname:node_1447,prsc:2|A-8156-RGB,B-7977-OUT;n:type:ShaderForge.SFN_Clamp01,id:2811,x:30914,y:33423,varname:node_2811,prsc:2|IN-1447-OUT;n:type:ShaderForge.SFN_Power,id:3039,x:31047,y:33602,varname:node_3039,prsc:2|VAL-7977-OUT,EXP-8457-OUT;n:type:ShaderForge.SFN_Vector1,id:8457,x:30890,y:33715,varname:node_8457,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Clamp01,id:7542,x:31185,y:33471,varname:node_7542,prsc:2|IN-3039-OUT;n:type:ShaderForge.SFN_Panner,id:8726,x:30132,y:33311,varname:node_8726,prsc:2,spu:1.5,spv:0.6|UVIN-857-UVOUT,DIST-8027-TSL;n:type:ShaderForge.SFN_TexCoord,id:857,x:29944,y:33205,varname:node_857,prsc:2,uv:0;n:type:ShaderForge.SFN_Time,id:8027,x:29939,y:33486,varname:node_8027,prsc:2;n:type:ShaderForge.SFN_FragmentPosition,id:4291,x:30398,y:34330,varname:node_4291,prsc:2;n:type:ShaderForge.SFN_Add,id:9768,x:30630,y:34330,varname:node_9768,prsc:2|A-4291-X,B-8027-T;n:type:ShaderForge.SFN_Sin,id:6603,x:31023,y:34385,varname:node_6603,prsc:2|IN-1209-OUT;n:type:ShaderForge.SFN_Multiply,id:5016,x:31232,y:34385,varname:node_5016,prsc:2|A-6603-OUT,B-8326-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8326,x:31150,y:34582,ptovrint:False,ptlb:Height,ptin:_Height,varname:node_8326,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_ValueProperty,id:4347,x:31359,y:34582,ptovrint:False,ptlb:Wave Widht,ptin:_WaveWidht,varname:node_4347,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:4879,x:31516,y:34385,varname:node_4879,prsc:2|A-5016-OUT,B-4347-OUT;n:type:ShaderForge.SFN_Multiply,id:1209,x:30846,y:34385,varname:node_1209,prsc:2|A-9768-OUT,B-393-OUT;n:type:ShaderForge.SFN_ValueProperty,id:393,x:30568,y:34702,ptovrint:False,ptlb:Count,ptin:_Count,varname:node_393,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:10;n:type:ShaderForge.SFN_RemapRange,id:4271,x:31700,y:34385,varname:node_4271,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-4879-OUT;n:type:ShaderForge.SFN_Panner,id:305,x:31006,y:32067,varname:node_305,prsc:2,spu:1,spv:0.5|UVIN-857-UVOUT,DIST-8027-TSL;n:type:ShaderForge.SFN_Tex2d,id:9670,x:31392,y:34146,ptovrint:False,ptlb:Noise,ptin:_Noise,varname:node_9670,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1fdd755d71958804192368871bdeb21d,ntxv:0,isnm:False|UVIN-143-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:3377,x:31207,y:32269,ptovrint:False,ptlb:Normal Map B,ptin:_NormalMapB,varname:node_3377,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True|UVIN-9531-UVOUT;n:type:ShaderForge.SFN_Panner,id:9531,x:31006,y:32246,varname:node_9531,prsc:2,spu:0.5,spv:1|UVIN-857-UVOUT,DIST-8027-TSL;n:type:ShaderForge.SFN_Lerp,id:5123,x:31665,y:32446,varname:node_5123,prsc:2|A-5964-RGB,B-3377-RGB,T-7939-OUT;n:type:ShaderForge.SFN_Sin,id:7324,x:31077,y:32472,varname:node_7324,prsc:2|IN-8027-T;n:type:ShaderForge.SFN_RemapRange,id:3300,x:31264,y:32509,varname:node_3300,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-7324-OUT;n:type:ShaderForge.SFN_OneMinus,id:8738,x:31413,y:32721,varname:node_8738,prsc:2|IN-9670-G;n:type:ShaderForge.SFN_Lerp,id:7939,x:31709,y:32682,varname:node_7939,prsc:2|A-8738-OUT,B-9670-G,T-3300-OUT;n:type:ShaderForge.SFN_Panner,id:143,x:31213,y:34146,varname:node_143,prsc:2,spu:3,spv:0|UVIN-857-UVOUT,DIST-8027-TSL;n:type:ShaderForge.SFN_ComponentMask,id:9240,x:31844,y:32896,varname:node_9240,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-1181-RGB;n:type:ShaderForge.SFN_Multiply,id:8683,x:31954,y:33076,varname:node_8683,prsc:2|A-6915-OUT,B-5023-OUT;n:type:ShaderForge.SFN_Slider,id:6915,x:31360,y:33108,ptovrint:False,ptlb:Refraction,ptin:_Refraction,varname:node_6915,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Vector1,id:5023,x:31759,y:33233,varname:node_5023,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Multiply,id:5295,x:32110,y:32950,varname:node_5295,prsc:2|A-9240-OUT,B-8683-OUT;n:type:ShaderForge.SFN_Lerp,id:4763,x:32038,y:32643,varname:node_4763,prsc:2|A-5123-OUT,B-1181-RGB,T-6915-OUT;n:type:ShaderForge.SFN_Vector1,id:1822,x:32502,y:32898,varname:node_1822,prsc:2,v1:1;n:type:ShaderForge.SFN_Tex2d,id:1181,x:31626,y:32878,ptovrint:False,ptlb:Refraction Normal,ptin:_RefractionNormal,varname:node_1181,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:502d5cd7b096dee488c83ba7b559347a,ntxv:3,isnm:True|UVIN-305-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:6449,x:32369,y:32870,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_6449,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Vector1,id:7468,x:32385,y:32779,varname:node_7468,prsc:2,v1:0;proporder:5964-6665-9478-4472-1889-7046-8156-8326-4347-393-3377-9670-6915-1181-6449;pass:END;sub:END;*/

Shader "Shader Forge/Water" {
    Properties {
        _BumpMap ("Normal Map A", 2D) = "bump" {}
        _ColorA ("Color A", Color) = (0.5019608,0.5019608,0.5019608,1)
        _ColorB ("Color B", Color) = (0.5,0.5,0.5,1)
        _WaterDepth ("Water Depth", Float ) = 50
        _FoamInMin ("FoamInMin", Range(0, 1)) = 0
        _FoamInMax ("FoamInMax", Range(0, 1)) = 0
        _FoamMap ("Foam Map", 2D) = "white" {}
        _Height ("Height", Float ) = 5
        _WaveWidht ("Wave Widht", Float ) = 0
        _Count ("Count", Float ) = 10
        _NormalMapB ("Normal Map B", 2D) = "bump" {}
        _Noise ("Noise", 2D) = "white" {}
        _Refraction ("Refraction", Range(0, 1)) = 0
        _RefractionNormal ("Refraction Normal", 2D) = "bump" {}
        _Gloss ("Gloss", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _ColorA;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _WaterDepth;
            uniform float4 _ColorB;
            uniform float _FoamInMin;
            uniform float _FoamInMax;
            uniform sampler2D _FoamMap; uniform float4 _FoamMap_ST;
            uniform float _Height;
            uniform float _WaveWidht;
            uniform float _Count;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform sampler2D _NormalMapB; uniform float4 _NormalMapB_ST;
            uniform float _Refraction;
            uniform sampler2D _RefractionNormal; uniform float4 _RefractionNormal_ST;
            uniform float _Gloss;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 screenPos : TEXCOORD7;
                float4 projPos : TEXCOORD8;
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float4 node_8027 = _Time + _TimeEditor;
                float node_4271 = (((sin(((mul(_Object2World, v.vertex).r+node_8027.g)*_Count))*_Height)+_WaveWidht)*0.5+0.5);
                v.vertex.xyz += float3(node_4271,node_4271,node_4271);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_8027 = _Time + _TimeEditor;
                float2 node_305 = (i.uv0+node_8027.r*float2(1,0.5));
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(node_305, _BumpMap)));
                float2 node_9531 = (i.uv0+node_8027.r*float2(0.5,1));
                float3 _NormalMapB_var = UnpackNormal(tex2D(_NormalMapB,TRANSFORM_TEX(node_9531, _NormalMapB)));
                float2 node_143 = (i.uv0+node_8027.r*float2(3,0));
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(node_143, _Noise));
                float3 _RefractionNormal_var = UnpackNormal(tex2D(_RefractionNormal,TRANSFORM_TEX(node_305, _RefractionNormal)));
                float3 normalLocal = lerp(lerp(_BumpMap_var.rgb,_NormalMapB_var.rgb,lerp((1.0 - _Noise_var.g),_Noise_var.g,(sin(node_8027.g)*0.5+0.5))),_RefractionNormal_var.rgb,_Refraction);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (_RefractionNormal_var.rgb.rg*(_Refraction*0.2));
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                d.boxMax[0] = unity_SpecCube0_BoxMax;
                d.boxMin[0] = unity_SpecCube0_BoxMin;
                d.probePosition[0] = unity_SpecCube0_ProbePosition;
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.boxMax[1] = unity_SpecCube1_BoxMax;
                d.boxMin[1] = unity_SpecCube1_BoxMin;
                d.probePosition[1] = unity_SpecCube1_ProbePosition;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float node_5675 = saturate((sceneZ-partZ)/_WaterDepth);
                float2 node_8726 = (i.uv0+node_8027.r*float2(1.5,0.6));
                float4 _FoamMap_var = tex2D(_FoamMap,TRANSFORM_TEX(node_8726, _FoamMap));
                float node_7319 = 0.0;
                float node_7977 = (1.0 - (node_7319 + ( (node_5675 - _FoamInMin) * (1.0 - node_7319) ) / (_FoamInMax - _FoamInMin)));
                float3 diffuseColor = saturate((lerp(_ColorA.rgb,_ColorB.rgb,node_5675)+saturate((_FoamMap_var.rgb*node_7977)))); // Need this for specular when using metallic
                float specularMonochrome;
                float3 specularColor;
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, 0.0, specularColor, specularMonochrome );
                specularMonochrome = 1-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * (UNITY_PI / 4) );
                float3 directSpecular = 1 * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = dot( normalDirection, lightDirection );
                float node_1822 = 1.0;
                float3 w = float3(node_1822,node_1822,node_1822)*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(node_1822,node_1822,node_1822);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                NdotLWrap = max(float3(0,0,0), NdotLWrap);
                float3 directDiffuse = ((forwardLight+backLight) + ((1 +(fd90 - 1)*pow((1.00001-NdotLWrap), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL))*(0.5-max(w.r,max(w.g,w.b))*0.5) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,node_5675),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _ColorA;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _WaterDepth;
            uniform float4 _ColorB;
            uniform float _FoamInMin;
            uniform float _FoamInMax;
            uniform sampler2D _FoamMap; uniform float4 _FoamMap_ST;
            uniform float _Height;
            uniform float _WaveWidht;
            uniform float _Count;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform sampler2D _NormalMapB; uniform float4 _NormalMapB_ST;
            uniform float _Refraction;
            uniform sampler2D _RefractionNormal; uniform float4 _RefractionNormal_ST;
            uniform float _Gloss;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 screenPos : TEXCOORD7;
                float4 projPos : TEXCOORD8;
                LIGHTING_COORDS(9,10)
                UNITY_FOG_COORDS(11)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float4 node_8027 = _Time + _TimeEditor;
                float node_4271 = (((sin(((mul(_Object2World, v.vertex).r+node_8027.g)*_Count))*_Height)+_WaveWidht)*0.5+0.5);
                v.vertex.xyz += float3(node_4271,node_4271,node_4271);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_8027 = _Time + _TimeEditor;
                float2 node_305 = (i.uv0+node_8027.r*float2(1,0.5));
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(node_305, _BumpMap)));
                float2 node_9531 = (i.uv0+node_8027.r*float2(0.5,1));
                float3 _NormalMapB_var = UnpackNormal(tex2D(_NormalMapB,TRANSFORM_TEX(node_9531, _NormalMapB)));
                float2 node_143 = (i.uv0+node_8027.r*float2(3,0));
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(node_143, _Noise));
                float3 _RefractionNormal_var = UnpackNormal(tex2D(_RefractionNormal,TRANSFORM_TEX(node_305, _RefractionNormal)));
                float3 normalLocal = lerp(lerp(_BumpMap_var.rgb,_NormalMapB_var.rgb,lerp((1.0 - _Noise_var.g),_Noise_var.g,(sin(node_8027.g)*0.5+0.5))),_RefractionNormal_var.rgb,_Refraction);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (_RefractionNormal_var.rgb.rg*(_Refraction*0.2));
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float node_5675 = saturate((sceneZ-partZ)/_WaterDepth);
                float2 node_8726 = (i.uv0+node_8027.r*float2(1.5,0.6));
                float4 _FoamMap_var = tex2D(_FoamMap,TRANSFORM_TEX(node_8726, _FoamMap));
                float node_7319 = 0.0;
                float node_7977 = (1.0 - (node_7319 + ( (node_5675 - _FoamInMin) * (1.0 - node_7319) ) / (_FoamInMax - _FoamInMin)));
                float3 diffuseColor = saturate((lerp(_ColorA.rgb,_ColorB.rgb,node_5675)+saturate((_FoamMap_var.rgb*node_7977)))); // Need this for specular when using metallic
                float specularMonochrome;
                float3 specularColor;
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, 0.0, specularColor, specularMonochrome );
                specularMonochrome = 1-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * (UNITY_PI / 4) );
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = dot( normalDirection, lightDirection );
                float node_1822 = 1.0;
                float3 w = float3(node_1822,node_1822,node_1822)*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(node_1822,node_1822,node_1822);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                NdotLWrap = max(float3(0,0,0), NdotLWrap);
                float3 directDiffuse = ((forwardLight+backLight) + ((1 +(fd90 - 1)*pow((1.00001-NdotLWrap), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL))*(0.5-max(w.r,max(w.g,w.b))*0.5) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * node_5675,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _Height;
            uniform float _WaveWidht;
            uniform float _Count;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                float4 node_8027 = _Time + _TimeEditor;
                float node_4271 = (((sin(((mul(_Object2World, v.vertex).r+node_8027.g)*_Count))*_Height)+_WaveWidht)*0.5+0.5);
                v.vertex.xyz += float3(node_4271,node_4271,node_4271);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _ColorA;
            uniform float _WaterDepth;
            uniform float4 _ColorB;
            uniform float _FoamInMin;
            uniform float _FoamInMax;
            uniform sampler2D _FoamMap; uniform float4 _FoamMap_ST;
            uniform float _Height;
            uniform float _WaveWidht;
            uniform float _Count;
            uniform float _Gloss;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float4 projPos : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                float4 node_8027 = _Time + _TimeEditor;
                float node_4271 = (((sin(((mul(_Object2World, v.vertex).r+node_8027.g)*_Count))*_Height)+_WaveWidht)*0.5+0.5);
                v.vertex.xyz += float3(node_4271,node_4271,node_4271);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float node_5675 = saturate((sceneZ-partZ)/_WaterDepth);
                float4 node_8027 = _Time + _TimeEditor;
                float2 node_8726 = (i.uv0+node_8027.r*float2(1.5,0.6));
                float4 _FoamMap_var = tex2D(_FoamMap,TRANSFORM_TEX(node_8726, _FoamMap));
                float node_7319 = 0.0;
                float node_7977 = (1.0 - (node_7319 + ( (node_5675 - _FoamInMin) * (1.0 - node_7319) ) / (_FoamInMax - _FoamInMin)));
                float3 diffColor = saturate((lerp(_ColorA.rgb,_ColorB.rgb,node_5675)+saturate((_FoamMap_var.rgb*node_7977))));
                float specularMonochrome;
                float3 specColor;
                diffColor = DiffuseAndSpecularFromMetallic( diffColor, 0.0, specColor, specularMonochrome );
                float roughness = 1.0 - _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
