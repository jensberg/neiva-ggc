﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class Load : MonoBehaviour {
    public GameObject background;

    private int loadProgress = 0;
    private bool isCoroutineDone = false;

	// Use this for initialization
	void Start () {
        background.SetActive(false);
        StartCoroutine(DisplayLoadingScreen());

    }
	
	// Update is called once per frame
	void Update () {
        if (isCoroutineDone)
        {
            gameObject.SetActive(false);
        }
    }

    IEnumerator DisplayLoadingScreen()
    {
        background.SetActive(true);

        AsyncOperation async = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);

        yield return async;
        isCoroutineDone = true;

    }
}
