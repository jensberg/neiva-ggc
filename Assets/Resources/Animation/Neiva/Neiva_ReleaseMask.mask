%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Neiva_ReleaseMask
  m_Mask: 00000000000000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Neiva
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:ChestEndEffector
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:ChestOriginEffector
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:HeadEffector
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:LeftUpLeg
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:LeftUpLeg/Neiva_Character_Ctrl:LeftLeg
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:LeftUpLeg/Neiva_Character_Ctrl:LeftLeg/Neiva_Character_Ctrl:LeftFoot
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:LeftUpLeg/Neiva_Character_Ctrl:LeftLeg/Neiva_Character_Ctrl:LeftFoot/Neiva_Character_Ctrl:LeftInFootIndex
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:LeftUpLeg/Neiva_Character_Ctrl:LeftLeg/Neiva_Character_Ctrl:LeftFoot/Neiva_Character_Ctrl:LeftInFootMiddle
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:LeftUpLeg/Neiva_Character_Ctrl:LeftLeg/Neiva_Character_Ctrl:LeftFoot/Neiva_Character_Ctrl:LeftInFootThumb
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:RightUpLeg
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:RightUpLeg/Neiva_Character_Ctrl:RightLeg
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:RightUpLeg/Neiva_Character_Ctrl:RightLeg/Neiva_Character_Ctrl:RightFoot
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:RightUpLeg/Neiva_Character_Ctrl:RightLeg/Neiva_Character_Ctrl:RightFoot/Neiva_Character_Ctrl:RightInFootIndex
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:RightUpLeg/Neiva_Character_Ctrl:RightLeg/Neiva_Character_Ctrl:RightFoot/Neiva_Character_Ctrl:RightInFootMiddle
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:RightUpLeg/Neiva_Character_Ctrl:RightLeg/Neiva_Character_Ctrl:RightFoot/Neiva_Character_Ctrl:RightInFootThumb
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm/Neiva_Character_Ctrl:LeftForeArm
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm/Neiva_Character_Ctrl:LeftForeArm/Neiva_Character_Ctrl:LeftHand
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm/Neiva_Character_Ctrl:LeftForeArm/Neiva_Character_Ctrl:LeftHand/Neiva_Character_Ctrl:LeftHandIndex1
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm/Neiva_Character_Ctrl:LeftForeArm/Neiva_Character_Ctrl:LeftHand/Neiva_Character_Ctrl:LeftHandIndex1/Neiva_Character_Ctrl:LeftHandIndex2
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm/Neiva_Character_Ctrl:LeftForeArm/Neiva_Character_Ctrl:LeftHand/Neiva_Character_Ctrl:LeftHandMiddle1
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm/Neiva_Character_Ctrl:LeftForeArm/Neiva_Character_Ctrl:LeftHand/Neiva_Character_Ctrl:LeftHandMiddle1/Neiva_Character_Ctrl:LeftHandMiddle2
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm/Neiva_Character_Ctrl:LeftForeArm/Neiva_Character_Ctrl:LeftHand/Neiva_Character_Ctrl:LeftHandRing1
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm/Neiva_Character_Ctrl:LeftForeArm/Neiva_Character_Ctrl:LeftHand/Neiva_Character_Ctrl:LeftHandRing1/Neiva_Character_Ctrl:LeftHandRing2
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm/Neiva_Character_Ctrl:LeftForeArm/Neiva_Character_Ctrl:LeftHand/Neiva_Character_Ctrl:LeftHandThumb1
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:LeftShoulder/Neiva_Character_Ctrl:LeftArm/Neiva_Character_Ctrl:LeftForeArm/Neiva_Character_Ctrl:LeftHand/Neiva_Character_Ctrl:LeftHandThumb1/Neiva_Character_Ctrl:LeftHandThumb2
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:Neck
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:Neck/Neiva_Character_Ctrl:Neck1
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:Neck/Neiva_Character_Ctrl:Neck1/Neiva_Character_Ctrl:Head
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm/Neiva_Character_Ctrl:RightForeArm
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm/Neiva_Character_Ctrl:RightForeArm/Neiva_Character_Ctrl:RightHand
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm/Neiva_Character_Ctrl:RightForeArm/Neiva_Character_Ctrl:RightHand/Neiva_Character_Ctrl:RightHandIndex1
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm/Neiva_Character_Ctrl:RightForeArm/Neiva_Character_Ctrl:RightHand/Neiva_Character_Ctrl:RightHandIndex1/Neiva_Character_Ctrl:RightHandIndex2
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm/Neiva_Character_Ctrl:RightForeArm/Neiva_Character_Ctrl:RightHand/Neiva_Character_Ctrl:RightHandMiddle1
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm/Neiva_Character_Ctrl:RightForeArm/Neiva_Character_Ctrl:RightHand/Neiva_Character_Ctrl:RightHandMiddle1/Neiva_Character_Ctrl:RightHandMiddle2
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm/Neiva_Character_Ctrl:RightForeArm/Neiva_Character_Ctrl:RightHand/Neiva_Character_Ctrl:RightHandRing1
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm/Neiva_Character_Ctrl:RightForeArm/Neiva_Character_Ctrl:RightHand/Neiva_Character_Ctrl:RightHandRing1/Neiva_Character_Ctrl:RightHandRing2
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm/Neiva_Character_Ctrl:RightForeArm/Neiva_Character_Ctrl:RightHand/Neiva_Character_Ctrl:RightHandThumb1
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:Hips/Neiva_Character_Ctrl:Spine/Neiva_Character_Ctrl:Spine1/Neiva_Character_Ctrl:Spine2/Neiva_Character_Ctrl:Spine3/Neiva_Character_Ctrl:Spine4/Neiva_Character_Ctrl:RightShoulder/Neiva_Character_Ctrl:RightArm/Neiva_Character_Ctrl:RightForeArm/Neiva_Character_Ctrl:RightHand/Neiva_Character_Ctrl:RightHandThumb1/Neiva_Character_Ctrl:RightHandThumb2
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:HipsEffector
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:LeftAnkleEffector
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:LeftElbowEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:LeftHandIndexEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:LeftHandMiddleEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:LeftHandRingEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:LeftHandThumbEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:LeftHipEffector
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:LeftKneeEffector
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:LeftShoulderEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:LeftWristEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:RightAnkleEffector
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:RightElbowEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:RightHandIndexEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:RightHandMiddleEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:RightHandRingEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:RightHandThumbEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:RightHipEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:RightKneeEffector
    m_Weight: 0
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:RightShoulderEffector
    m_Weight: 1
  - m_Path: Neiva_Character_Ctrl:Reference/Neiva_Character_Ctrl:RightWristEffector
    m_Weight: 1
  - m_Path: Neiva_Mesh
    m_Weight: 1
  - m_Path: NeivaLLeg_Platform
    m_Weight: 0
  - m_Path: NeivaPelvis_
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaBackSkirt_
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaBackSkirt_/NeivaLSkirt_1
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaBackSkirt_/NeivaRSkirt_1
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaBackTabard_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaBackTabard_1/NeivaBackTabard_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaBackTabard_1/NeivaLBackTabard_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaBackTabard_1/NeivaRBackTabard_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaBackTabard_1/NeivaRBackTabard_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaFrontTabard_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaFrontTabard_1/NeivaFrontTabard_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaLLeg_1
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaLLeg_1/NeivaLLeg_2
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaLLeg_1/NeivaLLeg_2/NeivaLLeg_Ankle_
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaLLeg_1/NeivaLLeg_2/NeivaLLeg_Ankle_/NeivaLLeg_LToeBig1
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaLLeg_1/NeivaLLeg_2/NeivaLLeg_Ankle_/NeivaLLeg_LToeMiddle1
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaLLeg_1/NeivaLLeg_2/NeivaLLeg_Ankle_/NeivaLLeg_LToeSmall1
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaRLeg_1
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaRLeg_1/NeivaRLeg_2
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaRLeg_1/NeivaRLeg_2/NeivaRLeg_Ankle_
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaRLeg_1/NeivaRLeg_2/NeivaRLeg_Ankle_/NeivaRLeg_RToeBig1
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaRLeg_1/NeivaRLeg_2/NeivaRLeg_Ankle_/NeivaRLeg_RToeMiddle1
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaRLeg_1/NeivaRLeg_2/NeivaRLeg_Ankle_/NeivaRLeg_RToeSmall1
    m_Weight: 0
  - m_Path: NeivaPelvis_/NeivaSpine1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaFrontPearl
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1/NeivaLArm_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1/NeivaLArm_2/NeivaLArm_Palm
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1/NeivaLArm_2/NeivaLArm_Palm/NeivaLArm_LIndex_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1/NeivaLArm_2/NeivaLArm_Palm/NeivaLArm_LIndex_1/NeivaLArm_LIndex_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1/NeivaLArm_2/NeivaLArm_Palm/NeivaLArm_LMiddle_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1/NeivaLArm_2/NeivaLArm_Palm/NeivaLArm_LMiddle_1/NeivaLArm_LMiddle_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1/NeivaLArm_2/NeivaLArm_Palm/NeivaLArm_LPinky_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1/NeivaLArm_2/NeivaLArm_Palm/NeivaLArm_LPinky_1/NeivaLArm_LPinky_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1/NeivaLArm_2/NeivaLArm_Palm/NeivaLArm_LThumb_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaLArm_Collarbone/NeivaLArm_1/NeivaLArm_2/NeivaLArm_Palm/NeivaLArm_LThumb_1/NeivaLArm_LThumb_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaNeck_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaNeck_1/NeivaNeck_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaNeck_1/NeivaNeck_2/NeivaNeck_3
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaNeck_1/NeivaNeck_2/NeivaNeck_3/NeivaHead
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaNeck_1/NeivaNeck_2/NeivaNeck_3/NeivaHead/NeivaLEar
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaNeck_1/NeivaNeck_2/NeivaNeck_3/NeivaHead/NeivaREar
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaPearlBack
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1/NeivaRArm_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1/NeivaRArm_2/NeivaRArm_Palm
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1/NeivaRArm_2/NeivaRArm_Palm/NeivaRArm_RIndex_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1/NeivaRArm_2/NeivaRArm_Palm/NeivaRArm_RIndex_1/NeivaRArm_RIndex_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1/NeivaRArm_2/NeivaRArm_Palm/NeivaRArm_RMiddle_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1/NeivaRArm_2/NeivaRArm_Palm/NeivaRArm_RMiddle_1/NeivaRArm_RMiddle_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1/NeivaRArm_2/NeivaRArm_Palm/NeivaRArm_RPinky_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1/NeivaRArm_2/NeivaRArm_Palm/NeivaRArm_RPinky_1/NeivaRArm_RPinky_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1/NeivaRArm_2/NeivaRArm_Palm/NeivaRArm_RThumb_1
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRArm_Collarbone/NeivaRArm_1/NeivaRArm_2/NeivaRArm_Palm/NeivaRArm_RThumb_1/NeivaRArm_RThumb_2
    m_Weight: 1
  - m_Path: NeivaPelvis_/NeivaSpine1/NeivaSpine2/NeivaSpine3/NeivaSpine4/NeivaChest_/NeivaRopeBack
    m_Weight: 1
  - m_Path: NeivaRLeg_Platform
    m_Weight: 0
